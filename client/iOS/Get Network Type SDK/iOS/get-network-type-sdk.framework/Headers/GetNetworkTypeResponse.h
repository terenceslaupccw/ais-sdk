//
//  GetNetworkTypeResponse.h
//  GetNetworkTypeSDK
//
//  Created by Promptnow on 3/22/17.
//  Copyright © 2017 AIS. All rights reserved.
//

#import <basic-authen-sdk/ResponseStatus.h>

@interface GetNetworkTypeResponse : ResponseStatus

@property (nonatomic, strong)NSString *subNetworkType, *mobileStatus;

- (id)initWithResponseData:(NSDictionary *)responseData;

@end
