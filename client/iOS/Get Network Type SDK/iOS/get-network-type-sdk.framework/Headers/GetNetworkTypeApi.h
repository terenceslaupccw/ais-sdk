//
//  GetNetworkTypeApi.h
//  GetNetworkTypeSDK
//
//  Created by Promptnow on 3/22/17.
//  Copyright © 2017 AIS. All rights reserved.
//

#import "GetNetworkTypeParameters.h"
#import "GetNetworkTypeResponse.h"

@interface GetNetworkTypeApi : NSObject

+ (GetNetworkTypeApi *)sharedService;

//Pre SubScription
- (void)getNetworkTypeWithParameters:(GetNetworkTypeParameters *)params
                      callbackSuccess:(void (^)(GetNetworkTypeResponse *response))success
                        callbackError:(void (^)(ResponseStatus *response))error;

@end
