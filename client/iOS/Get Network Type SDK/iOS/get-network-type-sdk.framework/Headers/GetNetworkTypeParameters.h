//
//  GetNetworkTypeParameters.h
//  GetNetworkTypeSDK
//
//  Created by Promptnow on 3/22/17.
//  Copyright © 2017 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetNetworkTypeParameters : NSObject

@property (nonatomic, strong)NSString *accessToken, *privateId;

- (instancetype)initWithAccessToken:(NSString *)accessToken privateId:(NSString *)privateId;

@end
