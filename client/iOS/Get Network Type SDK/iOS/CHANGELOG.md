# iOS - Get Network Type SDK ChangeLog
___
#### Version 1.0.1
___
##### 14 December 2017
* Migrate new service endpoint.

___
#### Version 1.0.0
___
##### 18 September 2017
* Release Service Get Network Type.
___
