# iOS - Purchase SDK ChangeLog
___
#### Version 1.0.1
___
##### 14 December 2017
* Migrate new service endpoint.

___
#### Version 1.0.0
___
##### 31 January 2017
* Release Service Presubscription Package.
___
