//
//  PurchaseApi.h
//  PurchaseSDK
//
//  Created by PromptNow on 7/11/16.
//  Copyright © 2016 AIS. All rights reserved.
//

#import "PreSubscriptionParameters.h"
#import "PreSubscriptionResponse.h"

@interface PurchaseApi : NSObject

+ (PurchaseApi *)sharedService;

//Pre SubScription
- (void)preSubScriptionWithParameters:(PreSubscriptionParameters *)params
               callbackSuccess:(void (^)(PreSubscriptionResponse *response))success
                 callbackError:(void (^)(ResponseStatus *response))error;

@end
