//
//  PreSubscriptionResponse.h
//  PurchaseSDK
//
//  Created by PromptNow on 7/11/16.
//  Copyright © 2016 AIS. All rights reserved.
//

#import <basic-authen-sdk/ResponseStatus.h>

@interface PreSubscriptionResponse : ResponseStatus

- (id)initWithResponseData:(NSDictionary *)responseData;

@end
