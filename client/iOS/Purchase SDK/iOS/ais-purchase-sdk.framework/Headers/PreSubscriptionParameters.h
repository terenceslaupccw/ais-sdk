//
//  PreSubscriptionParameters.h
//  PurchaseSDK
//
//  Created by PromptNow on 7/11/16.
//  Copyright © 2016 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PreSubscriptionParameters : NSObject


@property (nonatomic, strong)NSString *accessToken, *urlTrigCallBack;

- (instancetype)initWithAccessToken:(NSString *)accessToken urlTrigCallBack:(NSString *)urlTrigCallBack;

@end
