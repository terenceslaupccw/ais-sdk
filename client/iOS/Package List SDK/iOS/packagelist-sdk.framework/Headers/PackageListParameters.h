//
//  PackageListParameters.h
//  PackageListSDK
//
//  Created by PromptNow on 7/11/16.
//  Copyright © 2016 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PackageListParameters : NSObject


@property (nonatomic, strong)NSString *accessToken,*privateId,*command,*uid,*callBackUrl;

- (instancetype)initWithAccessToken:(NSString *)accessToken privateId:(NSString *)privateId command:(NSString *)command;

@end
