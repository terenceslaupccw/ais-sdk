//
//  PackageListApi.h
//  PackageListSDK
//
//  Created by PromptNow on 7/11/16.
//  Copyright © 2016 AIS. All rights reserved.
//

#import "PackageListParameters.h"
#import "PackageListResponse.h"

@interface PackageListApi : NSObject

+ (PackageListApi *)sharedService;

- (void)packageListWithParameters:(PackageListParameters *)params
                     callbackSuccess:(void (^)(PackageListResponse *response))success
                       callbackError:(void (^)(ResponseStatus *response))error;

@end
