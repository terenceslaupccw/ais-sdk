//
//  PackageListResponse.h
//  PackageListSDK
//
//  Created by PromptNow on 7/11/16.
//  Copyright © 2016 AIS. All rights reserved.
//

#import <basic-authen-sdk/ResponseStatus.h>
#import "PackageModel.h"

@interface PackageListResponse : ResponseStatus

@property (nonatomic, assign)NSString *fbbId,*msisdn,*privateId;
@property (nonatomic, strong)NSArray *listOfPackage;


- (id)initWithResponseData:(NSDictionary *)responseData;

@end
