//
//  PackageModel.h
//  PackageListSDK
//
//  Created by Promptnow on 11/15/17.
//  Copyright © 2017 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PackageModel : NSObject

@property (nonatomic, strong)NSString *packageNo, *packageName,*packageStartTime,*packageExpiryTime;

- (id)initWithResponseData:(NSDictionary *)responseData;

@end
