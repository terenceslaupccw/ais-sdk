# iOS - Basic Authentication SDK ChangeLog
___
####
___
#### Version 1.2.9
___
##### 16 March 2018
* ###### Check Network
* Change url endpoint for service Check Network.
___
#### Version 1.2.8
___
##### 24 October 2017
* ###### APIGW Core Sevice
* Modify validation logic. Add more flexibility of configuration and validation result handling.
___
#### Version 1.2.7
___
##### 24 July 2017
* ###### Webview Login
    * Fixed login via Wifi error cause white space in parameter.
___
#### Version 1.2.6
___
##### 15 June 2017
* ###### Check Network
    * Change url endpoint for service Check Network.
___
#### Version 1.2.5
___
##### 26 August 2016
* ###### ServiceAppAuthen
    * Modify response header from backend services.
___
#### Version 1.2.4
___
##### 3 August 2016
* ###### Check Network
    * Modify the code to support IPv6.
* ###### Connect Server
    * Remove bypass SSL certificate.
* ###### Log Error Exception
    * Set "moreInfo" in case error 90005.
* ###### Authentication
    * Modify the code to support User and Password Authentication.
