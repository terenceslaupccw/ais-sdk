//
//  AppAuthenResponse.h
//  FungusFramework
//
//  Created by Pluem Limrattanakan on 10/17/2557 BE.
//  Copyright (c) 2557 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResponseStatus.h"

@interface AppAuthenResponse : ResponseStatus

@property (nonatomic, strong)NSString *privateId, *accessToken, *expireIn, *idType, *idValue;
@property (nonatomic)BOOL newPrivateIdFlag;


@end
