//
//  FungusUtilities.h
//  FungusFramework
//
//  Created by Vasin Charoensuk on 1/8/56 BE.
//  Copyright (c) 2556 AIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

typedef enum {
    ConnectionTypeUnknown,
    ConnectionTypeNone,
    ConnectionType3G,
    ConnectionTypeWiFi
} ConnectionType;

@interface FungusUtilities : NSObject


+(NSString *) getRandomText:(int) len;

//+(NSString *) getCertificate;

+ (NSDictionary *)getSDKConfig;

+ (NSString *)getFungusLiveKey;
+ (NSString *)getExampleJson;

+(NSString *) getFingerPrint;

+(NSString *) getFungusTimeFormat:(double) time;

+(void) displayDialog:(NSString *) message;
+ (BOOL)isValidEmail:(NSString *)checkString;

//+ (BOOL)checkInternetConnection;
+ (ConnectionType)connectionType;
+(NSString*)base64forData:(NSData*)theData;
+ (NSMutableData *)base64DataFromString: (NSString *)string;
+(NSString *)bytesToHex:(const unsigned char *)bytes length:(size_t)len;
+(NSData*)mergeNSData:(NSData*)first mergeWith:(NSData*)second;
+(NSData*)divideData:(NSData*) fullPiece;
+ (NSString *)getHash512:(NSData *)data;


+(NSData *)getJSONDataWithRequest:(NSDictionary *)requestDict;
+(NSDictionary *)getDictionaryWithJSONData:(NSData *)jsonData;

+ (NSString *)getCurrentDeviceLanguage;

+ (NSString *)getUserAgent;

// Loading
+ (void)showLoading;
+ (void)hideLoading;

+ (void)showLoadingView:(UIViewController *)presentView;
+ (void)hideLoadingView;

+ (BOOL)checkNetworkIsAIS;

+ (void)setBeforeNetworkInROM:(ConnectionType)connectionType;
+ (ConnectionType)getBeforeNetworkInROM;

+ (NSString *)getIPAddress;


@end
