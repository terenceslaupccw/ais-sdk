//
//  DeviceAuthenService.h
//  FungusFramework
//
//  Created by Pluem Limrattanakan on 5/13/2558 BE.
//  Copyright (c) 2558 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AuthenParameter.h"
#import "AppAuthenResponse.h"
#import "ResponseStatus.h"
#import "LogoutResponse.h"

@interface ClientAuthenService : NSObject

+ (AppAuthenResponse *)initialSdk;

+ (ClientAuthenService *)sharedService;

- (void)loginAndCallbackSuccess:(void (^)(AppAuthenResponse *response))success
                  callbackError:(void (^)(ResponseStatus* response))error;

- (void)keepAliveWithParameter:(AuthenParameter *)authenParameter
               callbackSuccess:(void (^)(AppAuthenResponse *response))success
                 callbackError:(void (^)(ResponseStatus* response))error;

- (void)logOutWithParameter:(AuthenParameter *)authenParameter
            callbackSuccess:(void (^)(LogoutResponse *response))success
              callbackError:(void (^)(ResponseStatus* response))error;



@end
