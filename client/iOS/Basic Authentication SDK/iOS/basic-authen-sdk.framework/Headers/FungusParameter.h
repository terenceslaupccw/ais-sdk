//
//  FungusParameter.h
//  FungusFramework
//
//  Created by Vasin Charoensuk on 1/8/56 BE.
//  Copyright (c) 2556 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>

#define AUTHEN_TYPE_OTP_SMS @"otpsms"
#define AUTHEN_TYPE_MSISDN  @"msisdn"
#define TEST @"4"

@interface FungusParameter : NSObject

+ (void)setPrimitiveRoot:(NSString *)primitiveRoot;
+ (NSString *)getPrimitiveRoot;

+ (void)setPrime:(NSString *)prime;
+ (NSString *)getPrime;

+ (void)setAuthCode:(NSString *)authCode;
+ (NSString *)getAuthCode;

//+ (void)setAuthCodeKeepAlive:(NSString *)authCode;
//+ (NSString *)getAuthCodeKeepAlive;

+ (void)setApplicationID:(NSString *) appID;
+ (NSString *)getApplicationID;

+ (void)setAppName:(NSString *)appName;
+ (NSString *)getAppName;

+ (void)setPlatform:(NSString *)platform;
+ (NSString *)getPlatform;

+ (NSString *)getRequestSender;

+ (void)setApplicationVersion:(NSString *)appVersion;
+ (NSString *)getApplicationVersion;

+ (void)setClientIp:(NSString *)clientIp;
+ (NSString *)getClientIp;

+ (void)setClientId:(NSString *)clientId;
+ (NSString *)getClientId;

+ (void)setEmail:(NSString *)email;
+ (NSString *)getEmail;

+ (void)setUrlRegister:(NSString *)urlRegister;
+ (NSString *)getUrlRegister;

+ (void)setIsMigrationSubscriber:(NSString *)migrationFlag;
+ (BOOL)getIsMigrationSubscriber;

+ (void)setUrlBackend:(NSString *)urlBackend;
+ (NSString *)getUrlBackend;

+ (void)setPartnerId:(NSString *)partnerId;
+ (NSString *)getPartnerId;

+ (void)setClientLiveKey;
+ (NSString *)getClientLiveKey;

+ (void)setPtsAppEnvironmentType:(NSString *)ptsAppEnvironmentType;
+ (NSString *)getPtsAppEnvironmentType;

+ (void)clearSessionID;

+ (void)setSessionID:(NSString *)sessionID;
+ (NSString *)getSessionID;

+ (void)setSrfpToken:(NSString *)srfpToken;
+ (NSString *)getSrfpToken;

+ (void)setCookie:(NSString *)cookie;
+ (NSString *)getCookie;

+ (void)setUserID:(NSString *)userID;
+ (NSString *)getUserID;

+ (void)setApp:(NSString *)app;
+ (NSString *)getApp;

+ (void)setOrderRef;
+ (NSString *)getOrderRef;

+ (void)clearMSISDN;
+ (void)setMSISDN:(NSString *)number;
+ (NSString *)getMSISDN;

+ (void)setOrderID:(NSString *)transID;
+ (NSString *)getOrderID;

+ (void)setPermissionList:(NSArray *) permissions;

+ (NSArray *)getPermissionList;

+ (void) setAppStatus:(NSString *)status;
+ (NSString *)getAppStatus;

+ (void)setGssoPassword:(NSString *)password;
+ (NSString *)getGssoPassword;

+ (void)setGssoTransactionId:(NSString *)gssoTransactionId;
+ (NSString *)getGssoTransactionId;

+ (void)setAccessToken:(NSString *)accessToken;
+ (NSString *)getAccessToken;

+ (NSString *)getRequester;

+ (void)setAuthenLevel:(NSString *)authenLevel;
+ (NSString *)getAuthenLevel;

+ (void)setExpireIn:(NSString *)time;
+ (NSString *)getExpireIn;

#define ID_TYPE_MSISDN @"0"
#define ID_TYPE_FBB    @"3"
+ (void)setIDType:(NSString *)idType;
+ (NSString *)getIDType;

+ (void)setIDValue:(NSString *)idValue;
+ (NSString *)getIDValue;

+ (void)setOpenIDFlag:(NSString *)openIDFlag;
+ (NSString *)getOpenIDFlag;

+ (void)setRegistrationLevel:(NSString *)registrationLevel;
+ (NSString *)getRegistrationLevel;

+ (void)setSecurityLevel:(NSInteger)level;
+ (NSInteger)getSecurityLevel;

+ (void)setPrivateId:(NSString *)privateId;
+ (NSString *)getPrivateId;

+ (void)setPublicId:(NSString *)publicId;
+ (NSString *)getPublicId;

+ (void)setCredential:(NSString *)credential;
+ (NSString *)getCredential;

+ (void)setLang:(NSString *)lang;
+ (NSString *)getLang;

#pragma mark - EnvironmentType
#define ENVIRONMENTTYPE_PRODUCTION  @"1"
#define ENVIRONMENTTYPE_IOT         @"2"
#define ENVIRONMENTTYPE_TEST        @"4"

+ (void)setEnvironment:(NSString *)environment;
+ (NSString *)getEnvironment;

+ (void)setApiAppName:(NSString *)apiAppName;
+ (NSString *)getApiAppName;

+ (void)setApiPlatform:(NSString *)apiPlatform;
+ (NSString *)getApiPlatform;

+ (NSString *)getSubmissionTime;
+ (NSString *)getCommandId;
+ (NSString *)getChannel;

#pragma mark - AuthenType
#define AUTHENTYPE_MSISDN              @"msisdn"
#define AUTHENTYPE_MSISDN_WIFI         @"msisdn_wifi"
#define AUTHENTYPE_MSISDNAUTO          @"msisdnauto"
#define AUTHENTYPE_MSISDNMANUAL        @"msisdnmanual"
#define AUTHENTYPE_MSISDNAUTO_TRUST    @"msisdnauto_trust"
#define AUTHENTYPE_MSISDNMANUAL_TRUST  @"msisdnmanual_trust"

+ (void)setAuthenType:(NSString *)authenType;
+ (NSString *)getAuthenType;

+ (void)setAuthenByMSISDN:(NSString *)authenbyMSISDN;
+ (BOOL)getAuthenByMSISDN;

+ (void)setDefaultTemplate:(NSString *)defaultTemplate;
+ (NSString *)getDefaultTemplate;

+ (void)setTemplateName:(NSString *)templateName;
+ (NSString *)getTemplateName;

+ (NSString *)getType;

+ (BOOL)isAuthenticated;

+ (void)setPrivateIdList:(NSString *)privateId andAuthenType:(NSString *)authenType;
+ (NSArray *)getPrivateIdList;
+ (void)clearPrivateIdList;

+ (void)clearParameter;


//PLAYBOX
+ (void)setAuthenTypeInROM:(NSString *)authenType;
+ (NSString *)getAuthenTypeInROM;

+ (NSString *)getPrivateIdInROM;
+ (void)setPackageName:(NSString *)packageName;
+ (NSString *)getPackageName;

+ (void)setOTP:(NSString *)otp;
+ (NSString *)getotp;

+ (void)setTransactionID:(NSString *)transactionID;
+ (NSString *)getTransactionID;

+ (void)setURLTrigCallBack:(NSString *)url;
+ (NSString *)getURLTrigCallback;

+ (NSArray *)getRule;

+ (void)setRule:(NSArray *)rule;
#pragma mark - UserDefaults
#define KEY_UD_RULE             @"Rule"
#define KEY_UD_FIRST_LOGIN      @"FirstLogin"
#define KEY_UD_HEADER           @"Header"
#define KEY_UD_DATA             @"Data"
#define KEY_UD_ACCESSTOKEN      @"AccessToken"
#define KEY_UD_DECRYPT_KEY      @"DecryptKey"
#define KEY_UD_ENCRYPT_KEY      @"EncryptKey"
#define KEY_UD_INTIALVECTOR_KEY @"IntialVectorKey"
#define KEY_UD_NEW_PRIVATEID_FLAG_KEY @"NewPrivateIdFlag"
#define KEY_UD_PERMISSION_LIST  @"PermissionList"

#define KEY_UD_XPRIVATE_ID       @"xPrivateId"
#define KEY_UD_XPUBLIC_ID        @"xPublicId"
#define KEY_UD_XCREDENTIAL       @"xCredential"

#define KEY_UD_PRIVATE_IP        @"PrivateIp"

#pragma mark - UserDefaults - Rule
+ (NSArray *)getRuleInROM;
+ (void)setRuleInROM:(NSArray *)rule;

#pragma mark - UserDefaults - FirstLogin
+ (BOOL)getFirstLoginInROM;
+ (void)setFirstLoginInROM;
+ (void)clearFirstLoginInROM;

#pragma mark - UserDefaults - Header
+ (void)storeHeaderInROM:(NSDictionary *)header;
+ (NSDictionary *)getHeaderInROM;
+ (void)clearHeaderInRom;

#pragma mark - UserDefaults - Data
+ (void)storeDataInROM:(NSDictionary *)data;
+ (NSDictionary *)getDataInROM;
+ (void)clearDataInRom;

#pragma mark - UserDefaults - SecurityKey
+ (void)setEncryptKeyInROM:(NSData *)key;
+ (NSData *)getEncryptKeyInROM;
+ (void)setDecryptKeyInROM:(NSData *)key;
+ (NSData *)getDecryptInROM;
+ (void)setInitalVectorInROM:(NSData *)key;
+ (NSData *)getInitalVectorInROM;
+ (void)clearSecurityKeyInROM;

#pragma mark - UserDefaults - AccessToken
+ (void)setAccessTokenInROM:(NSString *)accessToken;
+ (NSString *)getAccessTokenInROM;
+ (void)clearAccessTokenInROM;

#pragma mark - UserDefaults - xPrivateId
+ (void)setXPrivateIdInROM:(NSString *)xPrivateId;
+ (NSString *)getXPrivateIdInROM;

#pragma mark - UserDefaults - xPublicId
+ (void)setXPublicIdInROM:(NSString *)xPublicId;
+ (NSString *)getXPublicIdInROM;

#pragma mark - UserDefaults - xCredential
+ (void)setXCredentialInROM:(NSString *)xCredential;
+ (NSString *)getXCredentialInROM;

+ (void)clearXHeaderInROM;


+ (void)setXSessionId:(NSString *)xSessionId;
+ (NSString *)getXSessionId;

+ (void)setXPrivateId:(NSString *)xPrivateId;
+ (NSString *)getXPrivateId;

+ (void)setXPublicId:(NSString *)xPublicId;
+ (NSString *)getXPublicId;

+ (void)setXCredential:(NSString *)xCredential;
+ (NSString *)getXCredential;


+ (void)setPrivateIpInROM:(NSString *)privateIp;
+ (NSString *)getPrivateIpInROM;
+ (void)clearPrivateIpInROM;

+ (NSArray *)getPermissionListInROM;
+ (void)setPermissionListInROM:(NSArray *)permissionList;

+ (void)setNewPrivateIdFlag:(BOOL)flag;
+ (BOOL)getNewPrivateIdFlag;


#define NETWORK_TYPE_OTHERMOBILE @"OTHERMOBILE"
#define NETWORK_TYPE_OTHERWISE   @"OTHERWISE"
+ (void)setNetworkType:(NSString *)networkType;
+ (NSString *)getNetworkType;


//+ (void)storeAutoLoginData:(NSDictionary *)data;
//+ (NSDictionary *)getAutoLoginData;
//
//+ (void)clearAutoLoginData;

typedef NS_ENUM(NSUInteger, RequestService) {
    RequestServiceLoginByB2C,
    RequestServiceKeepAlive
};
+ (void)setObtainAuthorityRequestFrom:(RequestService)service;
+ (RequestService)getObtainAuthorityRequestFrom;

#pragma mark - EVENT
#define EVENT_NEW               @"new"
#define EVENT_EXISTED           @"existed"
#define EVENT_TIMEOUT           @"timeout"
#define EVENT_LOGOUT            @"logout"
#define EVENT_FORCE_LOGOUT      @"forcelogout"
#define EVENT_DEFAULT           @"default"
#define EVENT_MISSING           @"privateidmissing"
#define EVENT_MISSMATCH         @"privateidmismatch"
#define EVENT_REGIS_COMPLETE    @"registercomplete"

+ (NSString *)getEvent;
+ (void)setEvent:(NSString *)event;

+ (NSString *)getCurrentNetwork;
+ (void)setCurrentNetwork:(NSString *)currentNetwork;

+(void)openOnData:(BOOL)flag;
+(BOOL)getOnData;

+ (void)setTestURL:(NSString *)url;

+ (NSString *)getTestURL;

+ (void)setOl4:(NSString *)ol4;
+ (NSString *)getOl4;

+ (void)setXApp:(NSString *)xApp;
+ (NSString *)getXApp;

+ (void)setPwrtcAddress:(NSString *)pwrtcAddress;
+ (NSString *)getPwrtcAddress;

+ (void)setStunServer:(NSString *)stunServer;
+ (NSString *)getStunServer;

@end
