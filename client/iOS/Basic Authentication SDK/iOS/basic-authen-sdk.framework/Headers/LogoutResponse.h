//
//  LogoutResponse.h
//  FungusFramework
//
//  Created by Pluem Limrattanakan on 10/28/2558 BE.
//  Copyright © 2558 AIS. All rights reserved.
//

#import "ResponseStatus.h"

@interface LogoutResponse : ResponseStatus

@end
