//
//  AuthenParameter.h
//  FungusFramework
//
//  Created by Pluem Limrattanakan on 4/9/2558 BE.
//  Copyright (c) 2558 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuthenParameter : NSObject

@property (nonatomic, strong)NSString *accessToken;

@end
