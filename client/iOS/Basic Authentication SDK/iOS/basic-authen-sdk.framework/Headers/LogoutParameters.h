//
//  LogoutParameter.h
//  FungusFramework
//
//  Created by Pluem Limrattanakan on 11/13/2558 BE.
//  Copyright © 2558 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LogoutParameters : NSObject

@property (nonatomic, strong)NSString *accessToken;

- (instancetype)initWithAccessToken:(NSString *)accessToken;

@end
