//
//  ResponseStatus.h
//  FungusFramework
//
//  Created by Vasin Charoensuk on 1/18/56 BE.
//  Copyright (c) 2556 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>
#define RELEASE

@interface ResponseStatus : NSObject

@property(nonatomic, strong)NSString *developerMessage, *userMessage, *moreInfo, *resultCode;

@end
