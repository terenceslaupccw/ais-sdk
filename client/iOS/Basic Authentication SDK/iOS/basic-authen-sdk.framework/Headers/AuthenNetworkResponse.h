//
//  AuthenNetworkResponse.h
//  FungusFramework
//
//  Created by Pluem Limrattanakan on 10/27/2558 BE.
//  Copyright © 2558 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ResponseStatus.h"

@interface AuthenNetworkResponse : ResponseStatus

@property (nonatomic, strong)NSString *privateId, *accessToken, *expireIn, *idType, *idValue;
@property (nonatomic)BOOL newPrivateIdFlag;

@end
