//
//  ConnectResponse.h
//  FungusApp
//
//  Created by Vasin Charoensuk on 11/19/55 BE.
//  Copyright (c) 2555 AIS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ConnectRequest.h"

@interface ConnectResponse : NSObject
{
    ConnectRequest *request;
    NSDictionary *responseHeader;
    NSURLConnection *connection;
}

@property (nonatomic, strong) ConnectRequest *request;
@property (nonatomic, strong) NSData *responseData;
@property (nonatomic, strong) NSDictionary *responseHeader;
@property (nonatomic, strong) NSDictionary *responseDict;
@property (nonatomic) NSInteger statusCode;

@end
