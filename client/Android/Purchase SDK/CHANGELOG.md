# Android - Purchase SDK ChangeLog
___
#### Version 1.1.0
___
##### 18 January 2017
* Release Service Presubscription Package.
* Migrate URL from Data Power to SACF.
___
#### Version 1.0.0
___
##### 28 December 2016
* Release Service Purchase Package.
___

