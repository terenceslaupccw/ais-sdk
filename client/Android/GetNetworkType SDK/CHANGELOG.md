# Android - GetNetworkType SDK ChangeLog
___
#### Version 1.1.0
___
##### 1 December 2017
* * Add GetNetworkTypeResponse interface parameter 'mobileStatus'.
* * Migrate URL from Data Power to SACF.
___
#### Version 1.0.0
___
##### 7 April 2017
* * Create getNetworkType function.
