# Android - Basic Authentication SDK ChangeLog
___
#### Version 1.2.11
___
##### 31 October 2017
* Configuration change.
___
#### Version 1.2.10
___
##### 22 September 2017
* Minor fix bug.
___
#### Version 1.2.9
___
##### 14 July 2017
* Minor fix bug.
___
#### Version 1.2.8
___
##### 7 June 2017
* Update playground environment
___
#### Version 1.2.7
___
##### 19 April 2017
* Configuration change.
___
#### Version 1.2.6
___
##### 30 November 2016
* Remove read device function.
___
#### Version 1.2.5
___
##### 26 August 2016
* Minor fix bug.
___
#### Version 1.2.4
___
##### 20 July 2016
* Https connection security improvment.
___
#### Version 1.2.3
___
##### 4 July 2016
* Add moreInfo description for error code 90005.
___
#### Version 1.2.2
___
##### 29 April 2016
* Minor fix bug.
___
#### Version 1.2.1
___
##### 8 April 2016
* Enhance Code. 