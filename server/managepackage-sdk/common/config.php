<?php
namespace _manage_package_sdk\common
{

    class Configuration
    {

        private $initConfig = "ew0KICAgICJwdHNBcHBFbnZpcm9ubWVudFR5cGUiOnsNCiAgICAgICAgIjEiOiJodHRwczovL2FwaWNtLmFpcy5jby50aDoxNTQxMSIsDQoJIjIiOiJodHRwczovL2FwaS5haXMuY28udGg6MTQ1MTEiLA0KICAgICAgICAiMyI6Imh0dHBzOi8vYXBpcGcuYWlzLmNvLnRoOjE1NDExIiwNCiAgICAgICAgIjQiOiJodHRwczovLzEwLjEzNy4xNi41NzoxNTQxMSIsDQogICAgICAgICI1IjoiaHR0cDovLzEwLjEwNC4yNDAuMTcyOjYxMDMiDQogICAgfSwNCiAgICAibWFuYWdlcGFja2FnZSI6ew0KICAgICAgICAiYXBwbHlQYWNrYWdlX3VybCI6Ii9hcGkvdjEvc3NiL3BhY2thZ2UvYXBwbHlQYWNrYWdlLmpzb24iDQogICAgICAgIA0KICAgIH0NCn0=";
        private $methodType;

        private $ptsAppEnvironmentType;

        public function __construct($environment = '', $methodType)
        {
            $this->setPtsAppEnvironmentType($environment);
            $this->setMethodType($methodType);
        }

        public function initConfiguration()
        {
            $config = $this->__decodeBase64($this->initConfig);
            return $this->__parseURL($config, $this->getPtsAppEnvironmentType(), $this->getMethodType());
        }

        private function __decodeBase64($param)
        {
            return base64_decode($param);
        }

        private function __jsonDecode($param)
        {
            return json_decode($param, true);
        }

        private function __parseURL($config, $environment, $methodType)
        {
            $url = array();
            $parseURL = $this->__jsonDecode($config);
            
            $url = array(
                "host" => $parseURL['ptsAppEnvironmentType'][$environment]
            );
            if ($environment == "development")
                $url["path"] = "";
            
            return $url;
        }

        private function setPtsAppEnvironmentType($param)
        {
            $this->ptsAppEnvironmentType = $param;
        }

        private function getPtsAppEnvironmentType()
        {
            return $this->ptsAppEnvironmentType;
        }

        private function setMethodType($param)
        {
            $this->methodType = $param;
        }

        private function getMethodType()
        {
            return $this->methodType;
        }
    }
}
?>