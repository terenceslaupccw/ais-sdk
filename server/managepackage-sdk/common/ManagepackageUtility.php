<?php
namespace _manage_package_sdk\common
{

    include_once (dirname(__DIR__) . '/../basic-authen-sdk/common/ServerAuthenUtility.php');
    use _server_sdk\common\ServerAuthenUtility;

    class ManagepackageUtility
    {

        public static function returnError($errorCode, $errorMessage, $errormoreInfo)
        {
            $resData = array(
                'resultCode' => $errorCode,
                'resultDescription' => $errorMessage,
                'isSuccess' => 'false',
                'transactionID' => '',
                'operName' => '',
                'moreInfo' => $errormoreInfo
            );
            return $resData;
        }

        public static function returnErrorVerify($errorCode, $errorMessage, $errormoreInfo)
        {
            $resData = array(
                'resultCode' => $errorCode,
                'resultDescription' => $errorMessage,
                'isSuccess' => 'false',
                'transactionID' => '',
                'operName' => '',
                'passKey' => '',
                'moreInfo' => $errormoreInfo
            );
            return $resData;
        }

        public static function returnErrorReserve($errorCode, $errorMessage, $errormoreInfo)
        {
            $resData = array(
                'aocResponseParameters' => 'null',
                'reserveVolumeResponsesParameters' => array(
                    'resultCode' => $errorCode,
                    'resultDescription' => $errorMessage,
                    'isSuccess' => 'false',
                    'purchaseID' => '',
                    'moreInfo' => $errormoreInfo
                )
            );
            return $resData;
        }

        public static function returnErrorBusiness($errorCode, $errorMessage, $errormoreInfo)
        {
            $resData = array(
                'resultCode' => $errorCode,
                'resultDescription' => $errorMessage,
                'isSuccess' => 'false',
                'moreInfo' => $errormoreInfo
            );
            return $resData;
        }
    }
}
?>