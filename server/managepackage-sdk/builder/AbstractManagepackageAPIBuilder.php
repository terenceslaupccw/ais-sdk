<?php
namespace _manage_package_sdk\builder
{

    abstract class AbstractManagepackageAPIBuilder
    {

        abstract public function buildManagepackage($serviceConfigParameters);
    }
}
?>