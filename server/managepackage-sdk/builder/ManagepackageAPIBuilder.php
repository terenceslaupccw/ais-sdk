<?php
namespace _manage_package_sdk\builder
{

    include_once __DIR__ . '/../service/mockup/ManagepackageAPIMockupManager.php';
    include_once __DIR__ . '/../service/ManagepackageManagerlmpl.php';
    include_once __DIR__ . '/AbstractManagepackageAPIBuilder.php';
    
    use _manage_package_sdk\service\mockup\ManagepackageAPIMockupManager;
    use _manage_package_sdk\builder\AbstractManagepackageAPIBuilder;
    use _manage_package_sdk\service\ManagepackageAPIManagerImpl;
    
    class ManagepackageAPIBuilder extends AbstractManagepackageAPIBuilder
    {

        private $managePackageApiManager;

        public static $serviceConfig;

        public function __construct()
        {}

        public function buildManagepackage($serviceConfigParameters)
        {
            self::$serviceConfig = $serviceConfigParameters;
            
            if (self::$serviceConfig->getLiveKeyPath() != '') {
                $this->managePackageApiManager = new ManagepackageAPIManagerImpl();
            } else {
                $this->managePackageApiManager = new ManagepackageAPIMockupManager();
            }
        }

        public function getResult()
        {
            return $this->managePackageApiManager;
        }
    }
}
?>