<?php
namespace _manage_package_sdk\model
{

    class ApplyPackageResponse
    {

        public function __construct($param)
        {
            $this->resultCode = $param->{'resultCode'};
            $this->developerMessage = (! property_exists((object) $param, 'developerMessage')) ? null : $param->{'developerMessage'};
            $this->userMessage = (! property_exists((object) $param, 'userMessage')) ? null : $param->{'userMessage'};
            $this->moreInfo = (! property_exists((object) $param, 'moreInfo')) ? null : $param->{'moreInfo'};
            $this->message = (! property_exists((object) $param, 'message')) ? null : $param->{'message'};
            return $this;
        }
    }
}
?>