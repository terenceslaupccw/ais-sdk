<?php
namespace _manage_package_sdk\validation
{

    include_once (dirname(__DIR__) . '/../basic-authen-sdk/exception/ServerAuthenException.php');
    
    use _manage_package_sdk\exception\ManagepackageException;
    use _server_sdk\exception\ServerAuthenException;

    class ValidationManager
    {

        public static function validateRequestBody($req, $data)
        {
            if (count((array) $req) > 0) {
                foreach ($data as $key) {
                    if (! property_exists($req, $key) || $req->{$key} == "" || $req->{$key} == " " || $req->{$key} == null) {
                        throw new ManagepackageException('422', '90003', '');
                    }
                }
            } else {
                throw new ManagepackageException('422', '90003', '');
            }
        }

        public static function validateRequestBodyArray($req, $data)
        {
            $i = 0;
            if (count($req) > 0) {
                while ($i < count($req)) {
                    foreach ($data as $key) {
                        if (! property_exists($req[$i], $key) || $req[$i]->{$key} == "" || $req[$i]->{$key} == " " || $req[$i]->{$key} == null) {
                            throw new ManagepackageException('422', '90003', '');
                        }
                    }
                    $i ++;
                }
            } else {
                throw new ManagepackageException('422', '90003', '');
            }
        }

        // check validate body response array
        public static function validationResponseArray($data, $body, $fieldlistArray)
        {
            if (! is_array($data)) {
                throw new ManagepackageException('422', '90005', $body);
            }
        }

        // check validate response Boolean
        public static function validationResponseBoolean($data, $body, $fieldlistBoolean)
        {
            if (! is_bool($data)) {
                if (strtolower($data) != "true" && strtolower($data) != "false") {
                    throw new ManagepackageException('422', '90005', $body);
                }
            }
        }

        // Check validation of body response from API backend.
        public static function validationResponseBody($data, $fieldlistString, $fieldlistArray = '', $fieldlistBoolean = '')
        {
            if (count($data) > 0) {
                foreach ($fieldlistString as $key) {
                    if (! array_key_exists($key, $data) || $data[$key] == ' ' || $data[$key] == '') {
                        throw new ManagepackageException('422', '90005', $data);
                    }
                }
                if (! empty($fieldlistArray)) {
                    foreach ($fieldlistArray as $key) {
                        self::validationResponseArray($data[$key], $data, $key);
                    }
                }
                if (! empty($fieldlistBoolean)) {
                    foreach ($fieldlistBoolean as $key) {
                        self::validationResponseBoolean($data[$key], $data, $key);
                    }
                }
            } else {
                throw new ManagepackageException('422', '90005', $data);
            }
        }

        // Check mandatory value clientId, email and secret concatString.
        public static function validationConfigB2B($inputData, $fieldlistString)
        {
            $keyReturn = '';
            $configB2B = array(
                'email' => $inputData->getEmail(),
                'clientId' => $inputData->getClientId(),
                'secret' => $inputData->getSecret(),
                'environment' => $inputData->getEnvironment()
            );
            $encodeConfig = json_encode($configB2B);
            $data = json_decode($encodeConfig);
            if (count((array) $data) > 0) {
                foreach ($fieldlistString as $key) {
                    if (! property_exists($data, $key) || $data->{$key} == ' ' || $data->{$key} == '') {
                        $keyReturn = $keyReturn . $key . ', ';
                    }
                }
            } else {
                throw new ManagepackageException('422', '90005', $data);
            }
            // CREATE BY LEK
            if ($keyReturn != '') {
                $count = strlen($keyReturn);
                $return = substr($keyReturn, 0, $count - 2);
                throw new ManagepackageException('422', '90001', '[Hummus] (' . $return . ')' . ' Missing');
            } else if (! filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
                throw new ManagepackageException('422', '90002', '[Hummus] E-Mail Format Incorrect');
            } else {
                ValidationManager::checkValidateEnvironment($data->environment);
            }
        }

        // Check value environment from partner.
        public static function checkValidateEnvironment($environment)
        {
            if ($environment == '') {
                throw new ManagepackageException('422', '90003', '[Hummus] Environment Missing');
            } elseif (is_numeric($environment)) {
                if ($environment < 1 || $environment > 5) {
                    throw new ManagepackageException('422', '90004', '[Hummus] Environment Invalid (' . $environment . ')');
                }
            } else {
                throw new ManagepackageException('422', '90004', '[Hummus] Environment Invalid (' . $environment . ')');
                // throw new ManagepackageException('422', '90004','[Hummus]Environment Invalid Number');
            }
        }

        public static function validateRequestMissing($data, $fieldlist)
        {
            $keyReturn = '';
            if (count((array) $data) > 0) {
                foreach ($fieldlist as $key) {
                    if ($data->{$key} == null || $data->{$key} == '') {
                        $keyReturn = $keyReturn . $key . ', ';
                    }
                }
            }
            // CREATE BY LEK
            if ($keyReturn != '') {
                $count = strlen($keyReturn);
                $return = substr($keyReturn, 0, $count - 2);
                throw new ManagepackageException('422', '90003', '[Hummus] (' . $return . ')' . ' Missing');
            }
        }

        // Check validation of body response from API backend.
        public static function validationResponseAPIBody($data, $fieldlistString, $orderRef, $subfix)
        {
            if (count((array) $data) > 0) {
                $isError = 0;
                $errorParam = '';
                foreach ($fieldlistString as $key) {
                    if (! property_exists($data, $key) || $data->{$key} == "") {
                        $isError = 1;
                        $errorParam .= ', ' . $key;
                    }
                }
                if ($isError) {
                    $errorParam = substr($errorParam, 2);
                    if ($orderRef == "" || $orderRef == null) {
                        throw new ManagepackageException('422', '90005', "(" . $errorParam . ") " . $subfix);
                    } else {
                        throw new ManagepackageException('422', '90005', "(" . $orderRef . ") " . "(" . $errorParam . ") " . $subfix);
                    }
                }
            } else {
                if ($orderRef == "" || $orderRef == null) {
                    throw new ManagepackageException('422', '90005', "(resultCode,developerMessage) " . $subfix);
                } else {
                    throw new ManagepackageException('422', '90005', "(" . $orderRef . ") " . "(resultCode, developerMessage) " . $subfix);
                }
            }
        }
    }
}
?>