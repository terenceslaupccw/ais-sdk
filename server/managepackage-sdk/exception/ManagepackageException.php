<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ServerAuthenException.php
 *
 */
namespace _manage_package_sdk\exception
{

    class ManagepackageException extends \Exception
    {

        private $status = null;

        private $developerMessage = null;

        private $userMessage = null;

        private $errorCode = null;

        private $moreInfo = null;

        public function __construct($status, $errorCode, $param = null)
        {
            $this->selectMessage($status, $errorCode, $param);
        }

        public function selectMessage($status, $errorCode, $param = null)
        {
            switch ($errorCode) {
                case 20000:
                    $message = 'Success';
                    break;
                case 90001:
                    $message = 'Missing Key Identify';
                    break;
                case 90002:
                    $message = 'Incorrect Key Identify';
                    break;
                case 90003:
                    $message = 'Missing Parameter';
                    break;
                case 90004:
                    $message = 'Incorrect Parameter';
                    break;
                case 90005:
                    $message = 'System Error';
                    break;
                case 90006:
                    $message = 'Permission deny';
                    break;
                case 90007:
                    $message = 'Connection Error';
                    break;
                case 90008:
                    $message = 'Connection Timeout';
                    break;
                case 90009:
                    $message = 'Unknown encoding';
                    break;
                case 90010:
                    $message = 'Decryption Error';
                    break;
                case 90011:
                    $message = 'Unknown Format';
                    break;
                case 90099:
                    $message = 'Unknown Error';
                    break;
            }
            $this->setDeveloperMessage($param);
            $this->setUserMessage($message);
            $this->setErrorCode($errorCode);
            $this->setMoreInfo(null);
            $this->setStatus($status);
        }

        public function getStatus()
        {
            return $this->status;
        }

        public function setStatus($status)
        {
            $this->status = $status;
            return $this;
        }

        public function getDeveloperMessage()
        {
            return $this->developerMessage;
        }

        public function setDeveloperMessage($developerMessage)
        {
            $this->developerMessage = $developerMessage;
            return $this;
        }

        public function getUserMessage()
        {
            return $this->userMessage;
        }

        public function setUserMessage($userMessage)
        {
            $this->userMessage = $userMessage;
            return $this;
        }

        public function getErrorCode()
        {
            return $this->errorCode;
        }

        public function setErrorCode($errorCode)
        {
            $this->errorCode = $errorCode;
            return $this;
        }

        public function getMoreInfo()
        {
            return $this->moreInfo;
        }

        public function setMoreInfo($moreInfo)
        {
            $this->moreInfo = $moreInfo;
            return $this;
        }
    }
}
?>