<?php
namespace _manage_package_sdk\service\api
{

    include_once (dirname(__DIR__) . '/../../basic-authen-sdk/common/ServiceAuthenAPIRequestParameters.php');
    use _server_sdk\common\ServiceAuthenAPIRequestParameters;

    interface ManagepackageAPIBackendManager
    {

        public function applyPackage($serviceAuthenAPIRequestParameters);
    }
}
?>