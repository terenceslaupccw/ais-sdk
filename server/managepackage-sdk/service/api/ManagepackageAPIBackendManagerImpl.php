<?php
namespace _manage_package_sdk\service\api
{

    include_once (dirname(__DIR__) . '/../../basic-authen-sdk/connection/ServiceAuthenAPIConnection.php');
    include_once (dirname(__DIR__) . '/../../basic-authen-sdk/utils/ServerAuthenUtility.php');
    include_once (dirname(__DIR__) . '/../../basic-authen-sdk/exception/ServerAuthenException.php');
    include_once (dirname(__DIR__) . '/../../basic-authen-sdk/common/ServiceAuthenAPIRequestParameters.php');
    include_once (dirname(__DIR__) . '/../../basic-authen-sdk/security/ServiceAuthenSecurity.php');
    include_once (dirname(__DIR__) . '/../../basic-authen-sdk/validation/ServiceAuthenValidation.php');
    
    include_once __DIR__ . '/ManagepackageAPIBackendManager.php';
    include_once __DIR__ . '/../../builder/ManagepackageAPIBuilder.php';
    include_once __DIR__ . '/../../common/config.php';
    include_once __DIR__ . '/../../model/ApplyPackageResponse.php';
    include_once __DIR__ . '/../../utils/Utility.php';
    
    use _server_sdk\utils\UtilityManage;
    use _server_sdk\connection\ServiceAuthenAPIConnection;
    use _server_sdk\utils\ServerAuthenUtility;
    use _server_sdk\exception\ServerAuthenException;
    use _server_sdk\common\ServiceAuthenAPIRequestParameters;
    use _server_sdk\security\ServiceAuthenSecurity;
    use _server_sdk\validation\ServiceAuthenValidation;
    use _manage_package_sdk\exception\ManagepackageException;
    use _manage_package_sdk\common\MapErrorCodeManagepackage;
    use _manage_package_sdk\common\Configuration;
    use _manage_package_sdk\service\api\ManagepackageAPIBackendManager;
    use _manage_package_sdk\builder\ManagepackageAPIBuilder;
    use _manage_package_sdk\validation\ValidationManager;
    use _manage_package_sdk\model\ApplyPackageResponse;
    
    class ManagepackageAPIBackendManagerImpl implements ManagepackageAPIBackendManager
    {

        private $apiResponse;

        public function applyPackage($serviceAuthenAPIRequestParameters)
        {
            $header = ServiceAuthenSecurity::decAccesstoken(ManagepackageAPIBuilder::$serviceConfig->getClientId(), $serviceAuthenAPIRequestParameters->{'accessToken'});
            $configuration = new Configuration(ManagepackageAPIBuilder::$serviceConfig->getEnvironment(), 'chargeReservation_url');
            $iniConfig = $configuration->initConfiguration();
            preg_match_all('/(?<param>.+)=(?<value>.+)/', ServerAuthenUtility::removeLN($header['x-app']), $appName);
            $appNameRecieved = explode('|', $appName['value'][0])[0];
            $hummusgen = date('YmdHis') . str_pad(rand(0, 9999), 4, '0', STR_PAD_LEFT);
            
            if ($header['apiURL'] != '') {
                $iniConfig["host"] = $header['apiURL'];
                $authenURL = $header['authenURL'];
                $apiURL = $header['apiURL'];
            }
            $iniConfig['path'] = "/api/v1/ssb/package/applyPackage.json";
            
           
            
            $initHeader = array(
                // Content-Type,Accept P'Kung
                'Content-Type: application/json; charset=utf-8',
                'Accept: application/json',
                'Cookie: ' . UtilityManage::removeLN($header['Cookie']),
                'x-session-id: ' . UtilityManage::removeLN($header['x-session-id']),
                'x-app: ' . UtilityManage::removeLN($header['x-app']),
                'x-user-id: ' . UtilityManage::removeLN($header['x-user-id']),
                'x-orderRef: ' . $hummusgen,
                'x-access-token: ' . UtilityManage::removeLN($header['x-access-token']),
                'x-sdk-category: Hummus|php',
                'x-sdk-name: Manage Package|3.0.0',
                
                'x-ssb-origin: '.$appNameRecieved,
                'x-ssb-service-origin: HUMMUS',
                'x-ssb-transaction-id: '.$hummusgen,
                'x-ssb-order-channel: '.$appNameRecieved,
                'x-ssb-version: V1',
                'x-ssb-command-name: applyPackage',
            );
            
            if (strlen($serviceAuthenAPIRequestParameters->{'msisdn'}) >= 15) {
                $msisdn = $serviceAuthenAPIRequestParameters->{'msisdn'};
                $initHeader[] = 'x-convert-pid: '.$serviceAuthenAPIRequestParameters->{'msisdn'};
            } else {
                if (substr($serviceAuthenAPIRequestParameters->{'msisdn'}, 0, 1) === "0") {
                    $msisdn = $serviceAuthenAPIRequestParameters->{'msisdn'};
                    $msisdn = substr($msisdn, 1, strlen($msisdn));
                    $msisdn = "66" . $msisdn;
                } else {
                    $msisdn = $serviceAuthenAPIRequestParameters->{'msisdn'};
                }
            }
           
            //Set Body to SACF
            $requestBody = array(
                'msisdn' => $msisdn,
                'accessNum' => $serviceAuthenAPIRequestParameters->{'accessNum'},
            );
            
            $initBody = json_encode($requestBody);
            $initTimeout = 20000;
            
            $serviceAuthenAPIRequestParameters = new ServiceAuthenAPIRequestParameters();
            
            $serviceAuthenAPIRequestParameters->setInitConfig($iniConfig['host'] . $iniConfig['path']);
            $serviceAuthenAPIRequestParameters->setInitMethod('POST');
            $serviceAuthenAPIRequestParameters->setInitTimeout($initTimeout);
            $serviceAuthenAPIRequestParameters->setInitHeader($initHeader);
            $serviceAuthenAPIRequestParameters->setInitBody($initBody);
            
            
            $hummusCurl = new ServiceAuthenAPIConnection();
            $responseCurl = $hummusCurl->curl($serviceAuthenAPIRequestParameters->getInitBody(), $serviceAuthenAPIRequestParameters->getInitConfig(), $serviceAuthenAPIRequestParameters->getInitMethod(), $serviceAuthenAPIRequestParameters->getInitHeader(), $serviceAuthenAPIRequestParameters->getInitTimeout());
            
            // Connection Error 90007
            if ($responseCurl['errno'] == '52' || $responseCurl['errno'] == '56' || $responseCurl['errno'] == '7' || $responseCurl['errno'] == '6') {
                throw new ManagepackageException('422', '90007', "[Hummus] Can't Connect Backend");
            }
            if ($responseCurl['errno'] == '28') {
                throw new ManagepackageException('422', '90008', "[Hummus] Timeout SDK { 20 Second }");
            }
            
            $this->apiBodyResponse = json_decode($responseCurl['body']);
            
            if(!isset ($this->apiBodyResponse->{'resultCode'})){$this->apiBodyResponse->{'resultCode'} = null;}
            if(!isset ($this->apiBodyResponse->{'userMessage'})){$this->apiBodyResponse->{'userMessage'} = null;}
            if(!isset ($this->apiBodyResponse->{'developerMessage'})){$this->apiBodyResponse->{'developerMessage'} = null;}
            if(!isset ($this->apiBodyResponse->{'moreInfo'})){$this->apiBodyResponse->{'moreInfo'} = null;}
            if(!isset ($this->apiBodyResponse->{'responseMessage'})){$this->apiBodyResponse->{'responseMessage'} = null;}
            if(!isset ($this->apiBodyResponse->{'ussdMessage'})){$this->apiBodyResponse->{'ussdMessage'} = null;}
            
            
            $this->headerResponseBk = UtilityManage::createHeaderB2B($responseCurl['header']);
            if (json_last_error() != JSON_ERROR_NONE || $responseCurl['body'] == null || is_array($this->apiBodyResponse) == 1) {
                if ($this->headerResponseBk['x-orderRef'] == "" ||$this->headerResponseBk['x-orderRef'] === null) {
                    throw new ManagepackageException('422', '90011', 'Response Not JSON format');
                } else {
                    throw new ManagepackageException('422', '90011', '(' . $this->headerResponseBk['x-orderRef'] . ')' . ' Response Not JSON format');
                }
                throw new ManagepackageException('422', '90011', '[Hummus] Response Not JSON format');
            }
            
            
            if ($responseCurl['http_code'] == '200' || $responseCurl['http_code'] == '201') {
                // http Status is 200 || 201
                $fieldlist = array(
                    'resultCode',
                    'developerMessage'
                );
                ValidationManager::validationResponseAPIBody($this->apiBodyResponse, $fieldlist, $this->headerResponseBk['x-orderRef'], 'Missing');
                
                if ($this->apiBodyResponse->{'resultCode'} == 20000) {
                    // ResultCode is 20000
                          $requestChargeReservation = (object)array();
                        $requestChargeReservation->{'resultCode'} = $this->apiBodyResponse->{'resultCode'};
                        $requestChargeReservation->{'userMessage'} = $this->apiBodyResponse->{'userMessage'};
                        $requestChargeReservation->{'developerMessage'} = $this->apiBodyResponse->{'developerMessage'};
                        $requestChargeReservation->{'moreInfo'} = $this->apiBodyResponse->{'moreInfo'};
                        if(null == $this->apiBodyResponse->{'responseMessage'} || $this->apiBodyResponse->{'responseMessage'} == ''){
                            $requestChargeReservation->{'message'} = $this->apiBodyResponse->{'ussdMessage'};
                        }else{
                            $requestChargeReservation->{'message'} = $this->apiBodyResponse->{'responseMessage'};
                        }
                        
                } else {
                    // ResultCode is not 20000
                    $requestChargeReservation = (object)array();
                    $requestChargeReservation->{'resultCode'} = $this->apiBodyResponse->{'resultCode'};
                    $requestChargeReservation->{'userMessage'} = $this->apiBodyResponse->{'userMessage'};
                    if ($this->headerResponseBk['x-orderRef'] == "" || $this->headerResponseBk['x-orderRef'] == null) {
                        $requestChargeReservation->{'developerMessage'} = $this->apiBodyResponse->{'developerMessage'};
                    } else {
                        $requestChargeReservation->{'developerMessage'} = '(' . $this->headerResponseBk['x-orderRef'] . ') ' . $this->apiBodyResponse->{'developerMessage'};
                    }
                    $requestChargeReservation->{'moreInfo'} = $this->apiBodyResponse->{'moreInfo'};
                    if(null == $this->apiBodyResponse->{'responseMessage'} || $this->apiBodyResponse->{'responseMessage'} == ''){
                        $requestChargeReservation->{'message'} = $this->apiBodyResponse->{'ussdMessage'};
                    }else{
                        $requestChargeReservation->{'message'} = $this->apiBodyResponse->{'responseMessage'};
                    }
                }
                
                $applyPackageResponse = new ApplyPackageResponse($requestChargeReservation);
            } else {
                // http Status is not 200 || 201
                $fieldlist = array(
                    'resultCode',
                    'developerMessage'
                );
                ValidationManager::validationResponseAPIBody($this->apiBodyResponse, $fieldlist, $this->headerResponseBk['x-orderRef'], 'Missing and HTTP status code was not 200 and 201');
                $requestChargeReservation = (object)array();
                $requestChargeReservation->{'resultCode'} = $this->apiBodyResponse->{'resultCode'};
                $requestChargeReservation->{'userMessage'} = $this->apiBodyResponse->{'userMessage'};
                if ($this->headerResponseBk['x-orderRef'] == "" || $this->headerResponseBk['x-orderRef'] == null) {
                    $requestChargeReservation->{'developerMessage'} = $this->apiBodyResponse->{'developerMessage'} . " and HTTP status code was not 200 and 201";
                } else {
                    $requestChargeReservation->{'developerMessage'} = '(' . $this->headerResponseBk['x-orderRef'] . ') ' . $this->apiBodyResponse->{'developerMessage'} . " and HTTP status code was not 200 and 201";
                }
                $requestChargeReservation->{'moreInfo'} = $this->apiBodyResponse->{'moreInfo'};
                if(null == $this->apiBodyResponse->{'responseMessage'} || $this->apiBodyResponse->{'responseMessage'} == ''){
                    $requestChargeReservation->{'message'} = $this->apiBodyResponse->{'ussdMessage'};
                }else{
                    $requestChargeReservation->{'message'} = $this->apiBodyResponse->{'responseMessage'};
                }
                
                $applyPackageResponse = new ApplyPackageResponse($requestChargeReservation);
            }
            return $applyPackageResponse;
        }
    }
}
?>