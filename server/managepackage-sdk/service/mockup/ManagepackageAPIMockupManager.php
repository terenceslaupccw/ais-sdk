<?php
namespace _manage_package_sdk\service\mockup
{

    include_once (dirname(__DIR__) . '/../../basic-authen-sdk/common/ServerAuthenUtility.php');
    include_once (dirname(__DIR__) . '/../../basic-authen-sdk/exception/ServerAuthenException.php');
    include_once (dirname(__DIR__) . '/../../basic-authen-sdk/common/ServiceAuthenAPIRequestParameters.php');
    
    include_once __DIR__ . '/../ManagepackageManager.php';
    include_once __DIR__ . '/../../common/ManagepackageUtility.php';
    include_once __DIR__ . '/../../builder/ManagepackageAPIBuilder.php';
    include_once __DIR__ . '/../../validation/ValidationManager.php';
    include_once __DIR__ . '/../../model/ApplyPackageResponse.php';
    
    use _manage_package_sdk\exception\ManagepackageException;
    use _manage_package_sdk\validation\ValidationManager;
    use _server_sdk\common\ServerAuthenUtility;
    use _server_sdk\exception\ServerAuthenException;
    use _server_sdk\common\ServiceAuthenAPIRequestParameters;
    use _manage_package_sdk\service\ManagepackageManager;
    use _manage_package_sdk\common\ManagepackageUtility;
    use _manage_package_sdk\builder\ManagepackageAPIBuilder;
    use _manage_package_sdk\model\ApplyPackageResponse;
    
    class ManagepackageAPIMockupManager implements ManagepackageManager
    {

        private $managePackageParameters;

        private $apiResponse = array();

        public function applyPackage($paramResult)
        {
            $logHeaders = apache_request_headers();
            
            try {
                // Send parameter cliendId, Email and environment to function checkConfigB2B for check validation.
                $fieldlist = array(
                    'email',
                    'clientId',
                    'environment'
                );
                ValidationManager::validationConfigB2B(ManagepackageAPIBuilder::$serviceConfig, $fieldlist);
                
                $fieldlist = array(
                    'accessToken',
                    'msisdn',
                    'accessNum'
                );
                if(!isset ($paramResult->{'accessToken'})){$paramResult->{'accessToken'} = null;}
                if(!isset ($paramResult->{'msisdn'})){$paramResult->{'msisdn'} = null;}
                if(!isset ($paramResult->{'accessNum'})){$paramResult->{'accessNum'} = null;}
                ValidationManager::validateRequestMissing($paramResult, $fieldlist);
                
                $this->applyPackageParameters = (object) [];
                $this->applyPackageParameters->{'resultCode'} = '20000';
                $this->applyPackageParameters->{'developerMessage'} = 'Success (Mockup)';
                $this->applyPackageParameters->{'userMessage'} = 'Success (Mockup)';
                $this->applyPackageParameters->{'moreInfo'} = null;
                $this->applyPackageParameters->{'message'} = 'ระบบกำลังตรวจสอบข้อมูล กรุณารอรับข้อความแจ้งสิทธิ์ของท่านค่ะ';
                
                return $this->applyPackageParameters;
            } catch (ServerAuthenException $e) {
                $this->errorResponse = (object) [];
                $this->errorResponse->{'resultCode'} = $e->getErrorCode();
                $this->errorResponse->{'developerMessage'} = $e->getDeveloperMessage();
                $this->errorResponse->{'userMessage'} = $e->getUserMessage();
                $this->errorResponse->{'moreInfo'} = $e->getMoreInfo();
                
                $applyPackageParameters = new ApplyPackageResponse($this->errorResponse);
                return $applyPackageParameters;
            } catch (ManagepackageException $e) {
                $this->errorResponse = (object) [];
                $this->errorResponse->{'resultCode'} = $e->getErrorCode();
                $this->errorResponse->{'developerMessage'} = $e->getDeveloperMessage();
                $this->errorResponse->{'userMessage'} = $e->getUserMessage();
                $this->errorResponse->{'moreInfo'} = $e->getMoreInfo();
                
                $applyPackageParameters = new ApplyPackageResponse($this->errorResponse);
                return $applyPackageParameters;
            }
        }
    }
}
?>