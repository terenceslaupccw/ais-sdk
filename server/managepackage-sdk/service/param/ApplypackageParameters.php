<?php
namespace _manage_package_sdk\service\param{

      class ApplypackageParameters {
        private $msisdn = "";
        private $accessToken= "";
        private $accessNum= "";
        
        /**
         * @return mixed
         */
        public function getMsisdn()
        {
            return $this->msisdn;
        }
    
        /**
         * @return mixed
         */
        public function getAccessToken()
        {
            return $this->accessToken;
        }
    
        /**
         * @return mixed
         */
        public function getAccessNum()
        {
            return $this->accessNum;
        }
    
        /**
         * @param mixed $msisdn
         */
        public function setMsisdn($msisdn)
        {
            $this->msisdn = $msisdn;
        }
    
        /**
         * @param mixed $accessToken
         */
        public function setAccessToken($accessToken)
        {
            $this->accessToken = $accessToken;
        }
    
        /**
         * @param mixed $accessNum
         */
        public function setAccessNum($accessNum)
        {
            $this->accessNum = $accessNum;
        }
    
    }
}
?>