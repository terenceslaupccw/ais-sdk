<?php
namespace _manage_package_sdk\service
{

    include_once (dirname(__DIR__) . '/../basic-authen-sdk/utils/ServerAuthenUtility.php');
    include_once (dirname(__DIR__) . '/../basic-authen-sdk/exception/ServerAuthenException.php');
    include_once (dirname(__DIR__) . '/../basic-authen-sdk/validation/ServiceAuthenValidation.php');
    include_once (dirname(__DIR__) . '/../basic-authen-sdk/security/ServiceAuthenSecurity.php');
    
    
    include_once __DIR__ . '/api/ManagepackageAPIBackendManagerImpl.php';
    include_once __DIR__ . '/../model/ApplyPackageResponse.php';
    include_once __DIR__ . '/../validation/ValidationManager.php';
    include_once __DIR__ . '/../exception/ManagepackageException.php';
    
    use _manage_package_sdk\exception\ManagepackageException;
    use _manage_package_sdk\validation\ValidationManager;
    use _server_sdk\utils\ServerAuthenUtility;
    use _server_sdk\exception\ServerAuthenException;
    use _server_sdk\validation\ServiceAuthenValidation;
    use _server_sdk\security\ServiceAuthenSecurity;
    use _manage_package_sdk\service\ManagepackageManager;
    use _manage_package_sdk\builder\ManagepackageAPIBuilder;
    use _manage_package_sdk\service\api\ManagepackageAPIBackendManagerImpl;
    use _manage_package_sdk\model\ApplyPackageResponse;
    
    include_once __DIR__ . '/param/ApplypackageParameters.php';
    use _manage_package_sdk\service\param\ApplypackageParameters;
    class ManagepackageAPIManagerImpl implements ManagepackageManager
    {
        
        private $managePackageParameters;

        private static $managePackageAPIManagerImpl;

        public static function getManagepackageAPIInstance()
        {
            if (! isset(self::$managePackageAPIManagerImpl)) {
                self::$managePackageAPIManagerImpl = new ManagepackageAPIBackendManagerImpl();
            }
            return self::$managePackageAPIManagerImpl;
        }

        public function applyPackage($paramResult)
        {
            $applyPackageParameters = new ApplypackageParameters();
            $applyPackageParameters->setMsisdn(!property_exists((object) $paramResult, 'msisdn') ? '' : $paramResult->{'msisdn'});
            $applyPackageParameters->setAccessNum(!property_exists((object) $paramResult, 'accessNum') ? '' : $paramResult->{'accessNum'});
            $applyPackageParameters->setAccessToken(!property_exists((object) $paramResult, 'accessToken') ? '' : $paramResult->{'accessToken'});
            $logHeaders = apache_request_headers();
            
            try {
                // Send parameter cliendId, Email and environment to function checkConfigB2B for check validation.
                $fieldlist = array(
                    'email',
                    'clientId',
                    'environment'
                );
                ValidationManager::validationConfigB2B(ManagepackageAPIBuilder::$serviceConfig, $fieldlist);
                
                if (file_exists(ManagepackageAPIBuilder::$serviceConfig->getLiveKeyPath())) {
                    $liveKey = file_get_contents(ManagepackageAPIBuilder::$serviceConfig->getLiveKeyPath());
                } else {
                    throw new ManagepackageException('422', '90002', '[Hummus] LiveKey path file not found');
                }
                
                $encodeEmail = ServiceAuthenSecurity::encodeEmail(ManagepackageAPIBuilder::$serviceConfig->getClientId(), ManagepackageAPIBuilder::$serviceConfig->getEmail());
                
                if ($liveKey == $encodeEmail) {
                    $fieldlist = array(
                        'accessToken',
                        'msisdn',
                        'accessNum'
                    );
                    
                    if(!isset ($paramResult->{'accessToken'})){$paramResult->{'accessToken'} = null;}
                    if(!isset ($paramResult->{'msisdn'})){$paramResult->{'msisdn'} = null;}
                    if(!isset ($paramResult->{'accessNum'})){$paramResult->{'accessNum'} = null;}
                    ValidationManager::validateRequestMissing($paramResult, $fieldlist);
                   
                    $this->applyPackageParameters = (object) [];
                    $this->applyPackageParameters->{'msisdn'} = $applyPackageParameters->getMsisdn();
                    $this->applyPackageParameters->{'accessNum'} = $applyPackageParameters->getAccessNum();
                    $this->applyPackageParameters->{'accessToken'} = $applyPackageParameters->getAccessToken();
                    
                    $managepackageAPIBackendManager = self::getManagepackageAPIInstance();
                    $reserveVolumeResponse = $managepackageAPIBackendManager->applyPackage($this->applyPackageParameters);
                    
                    return $reserveVolumeResponse;
                } else {
                    throw new ManagepackageException('422', '90002', '[Hummus] LiveKey not match in Livekey file');
                }
            } catch (ServerAuthenException $e) {
                $this->errorResponse = (object) [];
                $this->errorResponse->{'resultCode'} = $e->getErrorCode();
                $this->errorResponse->{'developerMessage'} = $e->getDeveloperMessage();
                $this->errorResponse->{'userMessage'} = $e->getUserMessage();
                $this->errorResponse->{'moreInfo'} = $e->getMoreInfo();
                
                $chargeReservationResponse = new ApplyPackageResponse($this->errorResponse);
                return $chargeReservationResponse;
            } catch (ManagepackageException $e) {
                $this->errorResponse = (object) [];
                $this->errorResponse->{'resultCode'} = $e->getErrorCode();
                $this->errorResponse->{'developerMessage'} = $e->getDeveloperMessage();
                $this->errorResponse->{'userMessage'} = $e->getUserMessage();
                $this->errorResponse->{'moreInfo'} = $e->getMoreInfo();
                
                $chargeReservationResponse = new ApplyPackageResponse($this->errorResponse);
                return $chargeReservationResponse;
            }
        }
    }
}
?>