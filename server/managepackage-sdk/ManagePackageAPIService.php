<?php
namespace _manage_package_sdk
{

    include_once (dirname(__DIR__) . '/basic-authen-sdk/model/ServerConfigParameters.php');
    include_once __DIR__ . '/builder/ManagepackageAPIBuilder.php';
    
    use _server_sdk\model\ServerConfigParameters;
    use _manage_package_sdk\builder\ManagepackageAPIBuilder;
    
    class ManagePackageAPIService
    {

        private $managePackageAPIBuilder;

        public function __construct(ServerConfigParameters $serverConfigParameters)
        {
            $this->managePackageAPIBuilder = new ManagepackageAPIBuilder();
            $this->managePackageAPIBuilder->buildManagepackage($serverConfigParameters);
        }

        // Return result to partner.
        public function buildManagePackageAPI()
        {
            return $this->managePackageAPIBuilder->getResult();
        }
    }
}
?>