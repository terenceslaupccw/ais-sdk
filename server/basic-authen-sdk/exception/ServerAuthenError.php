<?php 
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ServerAuthenError.php
 *
 */

namespace _server_sdk\exception{
	
	class ServerAuthenError{
		private $developerMessage;
		private $userMessage;
		private $errorCode;
		private $moreInfo;
		private $status;
		
		public function __construct($status,$developerMessage,$userMessage,$errorCode,$moreInfo){
			$this->setStatus($status);
			$this->setDeveloperMessage($developerMessage);
			$this->setUserMessage($userMessage);
			$this->setErrorCode($errorCode);
			$this->setMoreInfo($moreInfo);
		}
		
		public function printData(){
			header("HTTP/1.0 ".$this->getStatus());
			$data = array(
					'developerMessage' => $this->getDeveloperMessage()
					,'userMessage' => $this->getUserMessage()
					,'errorCode' => $this->getErrorCode()
					,'moreInfo' => $this->getMoreInfo()
			);
			return $data;
		}
		
		
		public function getDeveloperMessage() {
			return $this->developerMessage;
		}
		public function setDeveloperMessage($developerMessage) {
			$this->developerMessage = $developerMessage;
			return $this;
		}
		public function getUserMessage() {
			return $this->userMessage;
		}
		public function setUserMessage($userMessage) {
			$this->userMessage = $userMessage;
			return $this;
		}
		public function getErrorCode() {
			return $this->errorCode;
		}
		public function setErrorCode($errorCode) {
			$this->errorCode = $errorCode;
			return $this;
		}
		public function getMoreInfo() {
			return $this->moreInfo;
		}
		public function setMoreInfo($moreInfo) {
			$this->moreInfo = $moreInfo;
			return $this;
		}
		public function getStatus() {
			return $this->status;
		}
		public function setStatus($status) {
			$this->status = $status;
			return $this;
		}	
	}
}
?>