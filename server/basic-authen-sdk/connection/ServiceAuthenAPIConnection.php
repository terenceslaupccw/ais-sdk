<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 25/08/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : HummusCurl.php
 *
 */

namespace _server_sdk\connection{
	class ServiceAuthenAPIConnection{
		public function curl($data,$url,$method,$headerArr,$timeout){
			$hummusCurl = new ServiceAuthenAPIConnection();
			$hummusCurl->init();
			$hummusCurl->setopt(CURLOPT_RETURNTRANSFER, true);
			//Set header.
			$hummusCurl->setopt(CURLOPT_HTTPHEADER, $headerArr);
			$hummusCurl->setopt(CURLOPT_HEADER, true);
			
			//Set method.
			$hummusCurl->setopt(CURLOPT_CUSTOMREQUEST, $method);
			if($method == "POST"){
				$hummusCurl->setopt(CURLOPT_POST, 1);
				$hummusCurl->setopt(CURLOPT_POSTFIELDS, $data);
			}
			elseif($method == "PUT"){
				$hummusCurl->setopt(CURLOPT_CUSTOMREQUEST, 'PUT');
				$hummusCurl->setopt(CURLOPT_POSTFIELDS,$data);
				$hummusCurl->setopt(CURLOPT_RETURNTRANSFER, true);
			}
			elseif($method == "DELETE"){
				$hummusCurl->setopt(CURLOPT_POST, 0);
				// 				$data .= '?'.http_build_query($data);
				$hummusCurl->setopt(CURLOPT_POSTFIELDS, $data);
			}
			
			//Set URL.		
			$hummusCurl->setopt(CURLOPT_URL, $url);
			$hummusCurl->setopt(CURLOPT_NOSIGNAL, 1);
			//Set Timeout.
			$hummusCurl->setopt(CURLOPT_TIMEOUT_MS, $timeout);
			
			$hummusCurl->setopt(CURLOPT_SSL_VERIFYHOST, 0);
			$hummusCurl->setopt(CURLOPT_SSL_VERIFYPEER, FALSE);
			
			$output = $hummusCurl->exec();
			$headerSize = $hummusCurl->getinfo(CURLINFO_HEADER_SIZE);
			$http_code = $hummusCurl->getinfo(CURLINFO_HTTP_CODE);
			$errno = $hummusCurl->errno();
			$error = $hummusCurl->error();
			$hummusCurl->close();
			//Response from pantry.
			$header = substr($output, 0, $headerSize);
			$body = substr($output, $headerSize);
			
			$curl = array(
					'header'=>$header
					,'body'=>$body
					,'errno'=>$errno
					,'error'=>$error
					,'http_code'=>$http_code
			);
			return $curl;
		}
		
		public function init(){
			$this->curl = curl_init();
		}
		
		public function setopt($key, $value){
			curl_setopt($this->curl, $key, $value);
		}
		
		public function setopt_array(array $options){
			curl_setopt_array($this->curl, $options);
		}
		
		public function exec(){
			return curl_exec($this->curl);
		}
		
		public function errno(){
			return curl_errno($this->curl);
		}
		
		public function error(){
			return curl_error($this->curl);
		}
		
		public function getinfo($type){
			return curl_getinfo($this->curl, $type);
		}
		
		public function version(){
			return curl_version();
		}
		
		public function close(){
			curl_close($this->curl);
		}
		
	}
}