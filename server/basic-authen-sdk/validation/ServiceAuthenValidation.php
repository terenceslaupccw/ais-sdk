<?php
namespace _server_sdk\validation{
	use _server_sdk\exception\ServerAuthenException;
	class ServiceAuthenValidation{
		public static function validateRequestBody($req, $fieldlist){
			$isError = 0;
			$errorParam = '';
			foreach($fieldlist as $key){
				if(!property_exists($req, $key) || (empty($req->{$key}) || $req->{$key} == " " || $req->{$key} == null)){
					$isError = 1;
					$errorParam .= ','.$key;
				}
			}
			if($isError){
				$errorParam = substr($errorParam,1);
				throw new ServerAuthenException('422', '90003', '[Hummus] {'.$errorParam.'} Missing');
			}
		}
		
		//Check mandatory value clientId, email and secret.
		public static function checkConfigB2B($clientId,$email,$secret='',$status='n'){
			if(empty($clientId)){
				throw new ServerAuthenException('422', '90001','[Hummus] clientId Missing');
			}
			if(empty($email)){
				throw new ServerAuthenException('422', '90001','[Hummus] email Missing');
			}
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				throw new ServerAuthenException('422', '90002','[Hummus] E-Mail Format Incorrect');
			}
			if($status == 'y'){
				if(empty($secret)){
					throw new ServerAuthenException('422', '90001','[Hummus] secret Missing');
				}
			}
		}
		
		//Check mandatory value clientId, email and secret concatString.
		public static function validationConfigB2B($inputData, $fieldlistString){
		    $keyReturn = '' ;
		    $configB2B = array('email' => $inputData->getEmail(),
		        'clientId' => $inputData->getClientId(),
		        'secret' => $inputData->getSecret(),
		        'environment' => $inputData->getEnvironment(),
		    );
		    $encodeConfig = json_encode($configB2B);
		    $data = json_decode($encodeConfig);
		    if(count((array)$data) > 0){
		        foreach($fieldlistString as $key){
		            if(!property_exists($data, $key) ||  $data->{$key} == ' ' || $data->{$key} == ''){
		                $keyReturn = $keyReturn.$key.',';
		            }
		        }
		    }else{
		        throw new ServerAuthenException('422', '90005', $data);
		    }
		    //CREATE BY LEK
		    if($keyReturn != ''){
		        $count = strlen($keyReturn);
		        $return= substr($keyReturn,0,$count-1);
		        throw new ServerAuthenException('422', '90001','[Hummus]('.$return.')'.' Missing');
		    }else if(!filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
		        throw new ServerAuthenException('422', '90002','[Hummus]E-Mail Format Incorrect');
		    }else{
		        ServiceAuthenValidation::checkValidateEnvironment($data->environment);
		    }
		}
		
		//Check value environment from partner.
		public static function checkValidateEnvironment($environment){
			if($environment == ''){
				throw new ServerAuthenException('422', '90003','[Hummus]Environment Missing');
			}elseif(is_numeric($environment)){
				if($environment < 1 ||  $environment > 5){
					throw new ServerAuthenException('422', '90004','[Hummus]Environment Invalid ('.$environment.')');
				}
			}else{
				throw new ServerAuthenException('422', '90004','[Hummus]Environment Invalid Number');
			}
		}
		
		//check validate body response array
		public static function validationResponseArray($data, $body, $fieldlistArray){
			if(!is_array($data)){
				throw new ServerAuthenException('422', '90005',$body);
			}
		}
		
		//check validate response Boolean
		public static function validationResponseBoolean($data, $body, $fieldlistBoolean){
			if(!is_bool($data)){
				if(strtolower($data) != "true" && strtolower($data) != "false"){
					throw new ServerAuthenException('422', '90005',$body);
				}
			}
		}
		
		//Check validation of header response from API backend.
		public static function validationResponseHeader($data,$fieldlist, $fieldlistArray='', $fieldlistBoolean=''){
		    $keyReturn = '' ;
		    if(count($data) > 0){
		        foreach ($fieldlist as $key){
		            if(empty($data[$key])){
		                $keyReturn = $keyReturn.$key.',';
		            }
		        }
		        if(!empty($fieldlistArray)){
		            foreach ($fieldlistArray as $key){
		                self::validationResponseArray($data[$key],'');
		            }
		        }
		        if(!empty($fieldlistBoolean)){
		            foreach ($fieldlistBoolean as $key){
		                self::validationResponseBoolean($data[$key],'');
		            }
		        }
		        
		    }else{
		        $mandStr = '';
		        foreach ($fieldlist as $key){
		            $mandStr = $mandStr.','.$key;
		        }
		        $return= substr($mandStr,1);
		        // 		        print_r($return);
		        throw new ServerAuthenException('422', '90005','('.$return.')'.' Missing');
		    }
		    //CREATE BY LEK
		    if($keyReturn != ''){
		        $count = strlen($keyReturn);
		        // 			    $text= str_replace(' ', '_',substr($keyReturn,$count-2,$count));
		        $return= substr($keyReturn,0,$count-1);
		        throw new ServerAuthenException('422', '90005','('.$return.')'.' Missing');
		    }
		}
		
		//Check validation of header response from API backend.
		public static function validationResponseHeaderHTTPError ($data,$fieldlist, $fieldlistArray='', $fieldlistBoolean=''){
		    $keyReturn = '' ;
		    if(count($data) > 0){
		        foreach ($fieldlist as $key){
		            if(empty($data[$key])){
		                $keyReturn = $keyReturn.$key.',';
		            }
		        }
		        if(!empty($fieldlistArray)){
		            foreach ($fieldlistArray as $key){
		                self::validationResponseArray($data[$key],'');
		            }
		        }
		        if(!empty($fieldlistBoolean)){
		            foreach ($fieldlistBoolean as $key){
		                self::validationResponseBoolean($data[$key],'');
		            }
		        }
		        
		    }else{
		        if($keyReturn != ''){
		            $count = strlen($keyReturn);
		            // 			    $text= str_replace(' ', '_',substr($keyReturn,$count-2,$count));
		            $return= substr($keyReturn,0,$count-1);
		            throw new ServerAuthenException('422', '90005','('.$return.')'.' Missing'.' and HTTP status code was not 200 and 201');
		        }
		    }
		    //CREATE BY LEK
		    if($keyReturn != ''){
		        $count = strlen($keyReturn);
		        // 			    $text= str_replace(' ', '_',substr($keyReturn,$count-2,$count));
		        $return= substr($keyReturn,0,$count-1);
		        throw new ServerAuthenException('422', '90005','('.$return.')'.' Missing'.' and HTTP status code was not 200 and 201');
		    }
		}
		
		
// 		Check validation of body response from API backend.
// 		public static function validationResponseBody($data, $fieldlistString, $fieldlistArray='', $fieldlistBoolean=''){
// 			if(count((array)$data) > 0){
// 				foreach($fieldlistString as $key){
// 					if(!property_exists($data, $key) ||  $data->{$key} == ' ' || $data->{$key} == ''){
// 						throw new ServerAuthenException('422', '90005', $data);
// 					}
// 				}
// 				if(!empty($fieldlistArray)){
// 					foreach ($fieldlistArray as $key){
// 						self::validationResponseArray($data->{$key}, $data, $key);
// 					}
// 				}
// 				if(!empty($fieldlistBoolean)){
// 					foreach ($fieldlistBoolean as $key){
// 						self::validationResponseBoolean($data->{$key}, $data, $key);
// 					}
// 				}
// 			}else{
// 				throw new ServerAuthenException('422', '90005', $data);
// 			}
// 		}
		
		//Check validation of body response from API backend.
		public static function validationResponseBody($data, $fieldlistString, $fieldlistArray='', $fieldlistBoolean=''){
		    $keyReturn = '' ;
		    if(count((array)$data) > 0){
		        foreach($fieldlistString as $key){
		            if(!property_exists($data, $key) ||  $data->{$key} == ' ' || $data->{$key} == ''){
		                $keyReturn = $keyReturn.$key.',';
// 		                throw new ServerAuthenException('422', '90005', $data);
		            }
		        }
		        if(!empty($fieldlistArray)){
		            foreach ($fieldlistArray as $key){
		                self::validationResponseArray($data->{$key}, $data, $key);
		            }
		        }
		        if(!empty($fieldlistBoolean)){
		            foreach ($fieldlistBoolean as $key){
		                self::validationResponseBoolean($data->{$key}, $data, $key);
		            }
		        }
		    }else{
		        $mandStr = '';
		        foreach ($fieldlistString as $key){
		            $mandStr = $mandStr.','.$key;
		        }
		        $return= substr($mandStr,1);
		        // 		        print_r($return);
		        throw new ServerAuthenException('422', '90005','('.$return.')'.' Missing');
		    }
		    //CREATE BY LEK
		    if($keyReturn != ''){
		        $count = strlen($keyReturn);
		        // 			    $text= str_replace(' ', '_',substr($keyReturn,$count-2,$count));
		        $return= substr($keyReturn,0,$count-1);
		        throw new ServerAuthenException('422', '90005','('.$return.')'.' Missing');
		    }
		}
		
		//Check validation of body response from API backend.
		public static function validationResponseBodyHTTPError($data, $fieldlistString, $fieldlistArray='', $fieldlistBoolean=''){
		    $keyReturn = '' ;
		    if(count((array)$data) > 0){
		        foreach($fieldlistString as $key){
		            if(!property_exists($data, $key) ||  $data->{$key} == ' ' || $data->{$key} == ''){
		                $keyReturn = $keyReturn.$key.',';
		                // 		                throw new ServerAuthenException('422', '90005', $data);
		            }
		        }
		        if(!empty($fieldlistArray)){
		            foreach ($fieldlistArray as $key){
		                self::validationResponseArray($data->{$key}, $data, $key);
		            }
		        }
		        
		        if(!empty($fieldlistBoolean)){
		            foreach ($fieldlistBoolean as $key){
		                self::validationResponseBoolean($data->{$key}, $data, $key);
		            }
		        }
		    }else{
		        $mandStr = '';
		        foreach ($fieldlistString as $key){
		            $mandStr = $mandStr.','.$key;
		        }
		        $return= substr($mandStr,1);
		        // 		        print_r($return);
		        throw new ServerAuthenException('422', '90005','('.$return.')'.' Missing'.' and HTTP status code was not 200 and 201');
		    }
		    //CREATE BY LEK
		    if($keyReturn != ''){
		        $count = strlen($keyReturn);
		        // 			    $text= str_replace(' ', '_',substr($keyReturn,$count-2,$count));
		        $return= substr($keyReturn,0,$count-1);
		        throw new ServerAuthenException('422', '90005','('.$return.')'.' Missing'.' and HTTP status code was not 200 and 201');
		    }
		}
		
		//Check validation of body response from API backend.
		public static function validationResponseAPIBody($data, $fieldlistString){
			if(count((array)$data) > 0){
				$isError = 0;
				$errorParam = '';
				foreach($fieldlistString as $key){
					if(!property_exists($data, $key) || $data->{$key} === ''){
						$isError = 1;
						$errorParam .= ','.$key;
					}
				}
				if($isError){
					$errorParam = substr($errorParam,1);
					throw new ServerAuthenException('422', '90005', "(".$errorParam.") Missing");
				}
			}else{
				throw new ServerAuthenException('422', '90005', "(".json_encode($fieldlistString).") Missing");
			}
		}
		
		//Check validation of jsonformat
		public static function validateJson($string)
		{
		    // decode the JSON data
		    $result = json_decode($string,TRUE);
		    
		    // switch and check possible JSON errors
		    switch (json_last_error()) {
		        case JSON_ERROR_NONE:
		            $error = ''; // JSON is valid // No error has occurred
		            break;
		        case JSON_ERROR_DEPTH:
		            $error = 'The maximum stack depth has been exceeded.';
		            break;
		        case JSON_ERROR_STATE_MISMATCH:
		            $error = 'Invalid or malformed JSON.';
		            break;
		        case JSON_ERROR_CTRL_CHAR:
		            $error = 'Control character error, possibly incorrectly encoded.';
		            break;
		        case JSON_ERROR_SYNTAX:
		            $error = 'Syntax error, malformed JSON.';
		            break;
		            // PHP >= 5.3.3
		        case JSON_ERROR_UTF8:
		            $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
		            break;
		            // PHP >= 5.5.0
		        case JSON_ERROR_RECURSION:
		            $error = 'One or more recursive references in the value to be encoded.';
		            break;
		            // PHP >= 5.5.0
		        case JSON_ERROR_INF_OR_NAN:
		            $error = 'One or more NAN or INF values in the value to be encoded.';
		            break;
		        case JSON_ERROR_UNSUPPORTED_TYPE:
		            $error = 'A value of a type that cannot be encoded was given.';
		            break;
		        default:
		            $error = 'Unknown JSON error occured.';
		            break;
		    }
		    
		    if ($error !== '') {
		        // throw the Exception or exit // or whatever :)
		        throw new ServerAuthenException('422', '90011','Response Not JSON format');
		    }
		    
		    // everything is OK
		    return $result;
		}
		
	}
}