<?php
namespace _server_sdk\validation{
	use _server_sdk\exception\ServerAuthenException;

	class ClientAuthenValidation{
		//Check mandatory email values.
		public static function checkConfigB2C($email,$secret){
		    $keyReturn = '';
			if(empty($email)){
			    $keyReturn = $keyReturn.'email'.', ';
			}
			if(empty($secret)){
			    $keyReturn = $keyReturn.'secret'.', ';
			}
			if($keyReturn != ''){
			    $count = strlen($keyReturn);
			    $return= substr($keyReturn,0,$count-2);
			    throw new ServerAuthenException('422', '90001','[Hummus] ('.$return.')'.' Missing');
			}else if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
			    throw new ServerAuthenException('422', '90002', '[Hummus] E-Mail Format Incorrect');
			}
			
		}
		
		//check environment.
		public static function checkEnvironmentB2C($environment){
			if($environment <> "production" and $environment <> "partner_iot" and $environment <> "internal_iot" and $environment <> "development"){
			    throw new ServerAuthenException('422', '90004','[Hummus] ptsAppEnvironmentType Invalid ('.$environment.')');
			}
		}
		
		//Set number for environment.
		public static function pstEnvironment($environment){
			switch ($environment) {
				case 'production' :
					$setEnvironment = '1';
					break;
				case 'partner_iot' :
					$setEnvironment = '2';
					break;
				case 'internal_iot' :
					$setEnvironment = '3';
					break;
				case 'development' :
					$setEnvironment = '4';
					break;
				default :
					$setEnvironment = '';
					break;
			}
			return $setEnvironment;
		}
		
	}
}