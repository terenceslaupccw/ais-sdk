<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ServerAuthenService.php
 *
 */

namespace _server_sdk{
	include_once __DIR__.'/model/ClientConfigParameters.php';
	include_once __DIR__.'/model/ServerConfigParameters.php';
	include_once __DIR__.'/common/ClientAuthenUtility.php';
	include_once __DIR__.'/common/ServerAuthenUtility.php';
	include_once __DIR__.'/builder/ClientAuthenBuilder.php';
	include_once __DIR__.'/builder/ServerAuthenBuilder.php';
	
  	
	use _server_sdk\model\ClientConfigParameters;
	use _server_sdk\model\ServerConfigParameters;
	use _server_sdk\common\ClientAuthenUtility;
	use _server_sdk\common\ServerAuthenUtility;
	use _server_sdk\builder\ClientAuthenBuilder;
	use _server_sdk\builder\ServerAuthenBuilder;
	
	
	class ServerAuthenService{
		private $clientAuthenBuilder;
		private $serverAuthenBuilder;
		
		public function __construct(){
		}
		
		//Get config from partner.
		//Send config to method buildAuthen
		//Return response to partner.
		public function buildClientAuthen(ClientConfigParameters $clientConfigParameters){
			$this->clientAuthenBuilder  = new ClientAuthenBuilder();
			$this->clientAuthenBuilder->buildAuthen($clientConfigParameters);
			return $this->clientAuthenBuilder->getResult();
		}
		
		//Get config from partner.
		//Send config to method buildAuthen.
		//Return response to partner.
		public function buildServerAuthen(ServerConfigParameters $serverConfigParameters){
			$this->serverAuthenBuilder = new ServerAuthenBuilder();
			$this->serverAuthenBuilder->buildAuthen($serverConfigParameters);		
			return $this->serverAuthenBuilder->getResult();
		}
	}
}
?>