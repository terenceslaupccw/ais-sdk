<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ClientAuthenBuilder.php
 *
 */
namespace _server_sdk\builder{
	include_once __DIR__.'/AbstractAuthenServiceBuilder.php';
	include_once __DIR__.'/../service/ClientAuthenManagerImpl.php';
	include_once __DIR__.'/../service/mockup/ClientAuthenMockupManagerImpl.php';
	
    use _server_sdk\service\ClientAuthenManagerImpl;
    use _server_sdk\service\mockup\ClientAuthenMockupManagerImpl;
	class ClientAuthenBuilder extends AbstractAuthenServiceBuilder{
		private $clientAuthenManager;
		public static $serviceConfig;
		
		public function __construct(){
		}
		//Check parameter livekeyPath
		//if partner not set this parameter method buildAuthen will generate class ClientAuthenMockupManagerImpl.
		//else buildAuthen will generate class ClientAuthenManagerImpl.
		public function buildAuthen($serviceConfigParameters){
			self::$serviceConfig = $serviceConfigParameters;
			
	
			if(self::$serviceConfig->getLiveKeyPath() != ''){
				$this->clientAuthenManager = new ClientAuthenManagerImpl();
			}else{
				$this->clientAuthenManager = new ClientAuthenMockupManagerImpl();
			}
		}
		//Return response to method buildClientAuthen.
		public function getResult(){
			return $this->clientAuthenManager;
		}
	}
}
?>