<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 2.0.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ServerAuthenBuilder.php
 *
 */

namespace _server_sdk\builder{
	include_once __DIR__.'/AbstractAuthenServiceBuilder.php';
	include_once __DIR__.'/../service/ServerAuthenManagerImpl.php';
	include_once __DIR__.'/../service/mockup/ServerAuthenMockupManagerImpl.php';

    use _server_sdk\service\ServerAuthenManagerImpl;
    use _server_sdk\service\mockup\ServerAuthenMockupManagerImpl;
	
	class ServerAuthenBuilder extends AbstractAuthenServiceBuilder{
		private $serverAuthenManager;
		public static $serviceConfig;
		
		public function __construct(){
		}
		
		//Check parameter livekeyPath 
		//if partner not set this parameter method buildAuthen will generate class ServerAuthenMockupManagerImpl.
		//else buildAuthen will generate class ServerAuthenManagerImpl.
		public function buildAuthen($serviceConfigParameters){
				self::$serviceConfig = $serviceConfigParameters;
				
				
				if(self::$serviceConfig->getLiveKeyPath() != ''){
					$this->serverAuthenManager = new ServerAuthenManagerImpl();
				}else{
					$this->serverAuthenManager = new ServerAuthenMockupManagerImpl();
				}
		}
		//Return response to method buildServerAuthen.
		public function getResult(){
			return $this->serverAuthenManager;
		}
	}
}
?>