<?php

namespace _server_sdk\security{
	include_once __DIR__.'/../exception/ServerAuthenException.php';
	include_once __DIR__.'/../utils/ServerAuthenUtility.php';
	require __DIR__ . '/../vendor/autoload.php';
	
	use _server_sdk\exception\ServerAuthenException;
	use _server_sdk\utils\ServerAuthenUtility;
	use phpseclib\File\X509;
	use phpseclib\Crypt\RSA;
	use phpseclib\Crypt\AES;
	class ServiceAuthenSecurity{
		//Read secret file(certificate file).
		public static function readSecret($file){
			$cert = "";
			if(!file_exists($file)){
				throw new ServerAuthenException('422', '90002','[Hummus] Secret path file not found');
			}
			if($file != "" || !empty($file)){
				$string = file_get_contents($file);
				$x509 = new X509();
				$cert = $x509->loadX509($string);
				return $cert;
			}else{
				throw new ServerAuthenException('422', '90002','[Hummus] Secret is empty');
			}
		}
		
		//Generate signature.
		public static function genSignature($dataSignature,$publicKey){
			$hash = hash("sha256",$dataSignature);
			$hash = self::hex2bin($hash);
			$rsa = new RSA();
			$rsa->loadKey($publicKey);
			$rsa->setEncryptionMode(2);
			
			$ciphertext = $rsa->encrypt($hash);
			
			$signature = base64_encode($ciphertext);

		
			return $signature;
		}
		
		//Generate AccessToken.
		public static function encAccesstoken($clientId,$accesstoken){
			if(!array_key_exists('authenURL', $accesstoken) || !array_key_exists('apiURL', $accesstoken)){
				$accesstoken['authenURL'] = '';
				$accesstoken['apiURL'] = '';
			}
			//Create By Lek
			$isSetXapp = isset($accesstoken['x-app']);
			if(!$isSetXapp){
			    $accesstoken['x-app'] = null;
			    
			}
			$isSetUser = isset($accesstoken['x-user-id']);
			if(!$isSetUser){
			    $accesstoken['x-user-id'] = null;
			    
			}
			//==================
			$accesstokenJson = array(
				"Cookie" =>	$accesstoken['Cookie'],
				"x-access-token" =>	$accesstoken['x-access-token'],
				"x-app" =>	$accesstoken['x-app'],
				"x-session-id" =>	$accesstoken['x-session-id'],
				"x-user-id" =>	$accesstoken['x-user-id'],
				"authenURL" =>	$accesstoken['authenURL'],
				"apiURL" =>	$accesstoken['apiURL']
			);	
			
			$accesstokenJson = json_encode($accesstokenJson);
			$accesstokenJson = ServerAuthenUtility::removeLN($accesstokenJson);
		
			$hashClientId = hash('sha256', $clientId);
			$cipher = new AES();
			$cipher->setKey($hashClientId);
			$cipher->setIV($clientId);
			$encAccesstoken = base64_encode($cipher->encrypt($accesstokenJson));
		
		
			return $encAccesstoken;
		}
		
		//Decode AccesccToken.
		public static function decAccesstoken($clientId,$accesstoken){
			$base64Acc = base64_decode($accesstoken);
			$hashClientId = hash('sha256', $clientId);
			$cipher = new AES();
			$cipher->setKey($hashClientId);
			$cipher->setIV($clientId);
			$decAES = $cipher->decrypt($base64Acc);
			
			$header = json_decode($decAES,TRUE);
			if(!is_array($header)){throw new ServerAuthenException('422', '90010','[Hummus] AccessToken Incorrect');}
			return $header;
		}
		
		//encode data from string to binary.
		public static function hex2bin($hexdata) {
			$bindata = "";
			for ($i = 0; $i < strlen($hexdata); $i += 2) {
				$bindata .= chr(hexdec(substr($hexdata, $i, 2)));
			}
			return $bindata;
		}
		
		//Combine clientId and email then encode to base64.
		public static function encodeEmail($clientID,$email){
			$newEmail = "";
			$totalStr = strlen($clientID);
			$diffStr = floor($totalStr / 2);
		
			$clientPath1 = substr($clientID,0,$diffStr);
			$clientPath2 = substr($clientID,$diffStr);
		
			list($emailPath1,$emailPath2) = explode("@",$email);
			$newEmail = $emailPath1."@".$clientPath1.$emailPath2.$clientPath2;
			if(strlen($clientPath1) < strlen($clientPath2)){
			}elseif(strlen($clientPath1) > strlen($clientPath2)){
			}else{
			}
			$encodeEmail = base64_encode(ServiceAuthenSecurity::hex2bin(hash('sha256', $newEmail)));
			return $encodeEmail;
		}
	}
}
?>