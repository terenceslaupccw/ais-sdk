<?php

namespace _server_sdk\utils{
	include_once __DIR__.'/../security/ServiceAuthenSecurity.php';
	
    use _server_sdk\utils\common\CommonAuthenUtility;
	
	
	class ServerAuthenUtility extends CommonAuthenUtility{	
				
		public static function createHeaderB2B($headerResponse, $authenURL, $apiURL){
			$apiResponse = array();
			$header = array();
			$cookie = array();
			$strCookie = "";
			//header
			preg_match_all('/(?<param>x.+): (?<value>.+)/', $headerResponse, $header);
			$countHeader = count($header['param']);
			for($i=1;$i<=$countHeader;$i++){
				$apiResponse[trim($header['param'][$i-1])] = self::removeLN($header['value'][$i-1]);
			}
			//cookie
			preg_match_all('/Set-Cookie:(?<cookie>.+)$/im', $headerResponse, $cookie);
			$countCookie = count($cookie['cookie']);
			
			for($i=0;$i<=$countCookie-1;$i++){
				$cookcook = $cookie['cookie'][$i];
				$cook = trim($cookcook);
				$strCookie .= ';'.$cook;
				
			}
			
			if($countCookie == 0){
			    $apiResponse['Cookie'] = null;
			}else{
			    $apiResponse['Cookie'] = trim(self::removeLN(substr($strCookie,1)));
			}
			$apiResponse['authenURL'] = $authenURL;
			$apiResponse['apiURL'] = $apiURL;
			
			return $apiResponse;
		}
		
	}
}
?>