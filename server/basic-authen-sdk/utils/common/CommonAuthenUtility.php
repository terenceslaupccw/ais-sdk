<?php
namespace _server_sdk\utils\common{
	use _server_sdk\exception\ServerAuthenException;
	
	
	class CommonAuthenUtility{	
		//Replace 0 in phone number to 66
		public static function callingCode($param){
			if($param[0] == '0'){
				$msisdn = "66".substr($param,1);
			}elseif($param[0].$param[1] == "66"){
				$msisdn = $param;
			}else{
				throw new ServerAuthenException ( '422', '90004', '' );
			}
			return $msisdn;
		}
		
		//Replace " " become "".
		public static function removeLN($str){
			$replace = str_replace(chr(13), "",$str);
			return $replace;
		}
		
	}
}
?>