<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
  * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 24/08/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ClientAuthenUtility.php
 *
 */

namespace _server_sdk\utils{
	include_once __DIR__.'/common/CommonAuthenUtility.php';
	use _server_sdk\utils\common\CommonAuthenUtility;
	use _server_sdk\exception\ServerAuthenException;
	
	class ClientAuthenUtility extends CommonAuthenUtility{
		public static function checkConfigB2C($email,$secret){
			if(empty($email) || empty($secret)){
				throw new ServerAuthenException('422', '90001','');
			}
		}
		
		public static function checkEnvironmentB2C($environment){
			$environment = strtolower($environment);
			if($environment <> "production" and $environment <> "partner_iot" and $environment <> "internal_iot" and $environment <> "development"){
				throw new ServerAuthenException('422', '90004','');
			}
		}
		
		public static function pstEnvironment($environment){
			switch ($environment) {
				case 'production' :
					$setEnvironment = '1';
					break;
				case 'partner_iot' :
					$setEnvironment = '2';
					break;
				case 'internal_iot' :
					$setEnvironment = '3';
					break;
				case 'development' :
					$setEnvironment = '4';
					break;
				default :
					$setEnvironment = '';
					break;
			}
			return $setEnvironment;
		}
		
		public static function checkLiveKeyB2C($xApp,$platform,$liveKeyPath){
		    if(strtolower($platform) <> "ios" and strtolower($platform) <> "android" and strtolower($platform) <> "browser"){throw new ServerAuthenException('422','90004','[Hummus] Platform Invalid ('.$platform.')');}
			if($liveKeyPath <> ""){
			    list($pantrySideId,$pantryId,$partnerId,$appKeyName) = explode(";",$xApp);
			    list($appKey,$appValue) = explode("=",$appKeyName);
			    list($appName,$appPlatform,$appVersion) = explode("|",$appValue);
			    
			    $filesFolder = scandir($liveKeyPath);
			    foreach($filesFolder as $fileName){
			        if(preg_match('/'.strtolower($appName).'/',strtolower($fileName)) && preg_match('/'.strtolower($platform).'/',strtolower($fileName)) && preg_match('/'.strtolower($appVersion).'/',strtolower($fileName))){
			            $liveKeyName = $fileName;
			        }
			    }
			}
			if(!isset ($liveKeyName)){$liveKeyName = null;}
			if($liveKeyName != ""){
			    $liveKey = file_get_contents($liveKeyPath.$liveKeyName);
			    if(empty($liveKey)){
			        throw new ServerAuthenException('422','90002','[Hummus] LiveKey path file not found');
			    }else{
			        return $liveKey;
			    }
			}else{
			    throw new ServerAuthenException('422','90002','[Hummus] LiveKey path file not found');
			}
			
		}
		
		public static function errorResponsePantryB2C($data){
		    $decodeBodyBase64 = base64_decode($data['body']);
		    $body = json_decode($decodeBodyBase64);
			$header = array();
			preg_match_all('/(?<param>.+): (?<value>.+)/', $data['header'], $header);
			$countHeader = count($header['param']);
			for($i=1;$i<=$countHeader;$i++){
				if(strtolower($header['param'][$i-1]) == "x-app"){
					header('x-app'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
				}
				elseif(strtolower($header['param'][$i-1]) == "x-session-id"){
					header('x-session-id'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
				}elseif(strtolower($header['param'][$i-1]) == "set-cookie"){
				    header('Set-Cookie'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
				}elseif(strtolower($header['param'][$i-1]) == "x-user-id"){
				    header('x-user-id'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
				}elseif(strtolower($header['param'][$i-1]) == "x-access-token"){
				    header('x-access-token'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
				}elseif(strtolower($header['param'][$i-1]) == "x-private-id"){
				    header('x-private-id'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
				}elseif(strtolower($header['param'][$i-1]) == "x-sdk-category"){
				    header('x-sdk-category'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
				}elseif(strtolower($header['param'][$i-1]) == "x-sdk-name"){
				    header('x-sdk-name'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
				}
			}
			
			
			if ((json_last_error() == JSON_ERROR_NONE) and (count($body) > 0)) {
				$returnData = (object)[];
				$returnData->{'developerMessage'} = $body->{'developerMessage'};
				$returnData->{'resultCode'} = $body->{'resultCode'};
				if(isset($body->{'userMessage'})){
					$returnData->{'userMessage'} = $body->{'userMessage'};
				}else{
					$returnData->{'userMessage'} = null;
				}
				if(isset($body->{'moreInfo'})){
				$returnData->{'moreInfo'} = $body->{'moreInfo'};
				}else{
				$returnData->{'moreInfo'} = null;
				}
				return $returnData;
			}else{
				throw new ServerAuthenException('422', '90011','');
			}
		}
		
		public static function validationResponseHeader($data,$fieldlist){
		    if(count($data) > 0){
		        foreach ($fieldlist as $key){
		            if(is_array($data[$key])){
		                
		                if(count($data[$key]) <= 0){
		                    throw new ServerAuthenException('422', '90005','');
		                }
		            }else{
		                if($data[$key] == ''){
		                    throw new ServerAuthenException('422', '90005','');
		                }
		            }
		        }
		    }else{
		        throw new ServerAuthenException('422', '90005','');
		    }
		}
		
		public static function checkRestHeader(){
			$restHeader = self::get_rest_header();
				$header = array();
				if(!isset ($restHeader['x-session-id'])){$restHeader['x-session-id'] = null;}
				if(!isset ($restHeader['x-app'])){$restHeader['x-app'] = null;}
				if(!isset ($restHeader['x-user-id'])){$restHeader['x-user-id'] = null;}
				if(!isset ($restHeader['Cookie'])){$restHeader['Cookie'] = null;}
				if(!isset ($restHeader['x-access-token'])){$restHeader['x-access-token'] = null;}
				if(!isset ($restHeader['x-private-id'])){$restHeader['x-private-id'] = null;}
					$header['x-session-id'] = $restHeader['x-session-id'];
					$header['x-app'] = $restHeader['x-app'];
					$header['x-user-id'] = $restHeader['x-user-id'];
					$header['Cookie'] = $restHeader['Cookie'];
					if (array_key_exists("x-access-token",$restHeader)) $header['x-access-token'] = $restHeader['x-access-token'];
					if (array_key_exists("x-private-id",$restHeader)) $header['x-private-id'] = $restHeader['x-private-id'];
			return $header;
		}
		
		public static function get_rest_header(){
			if(function_exists('apache_request_headers')) {
				$headers = apache_request_headers();
			}elseif(function_exists('getallheaders')){
				$headers = getallheaders();
			}else{	
				$headers = array();
				$rx_http = '/\AHTTP_/';
				foreach($_SERVER as $key => $val) {
					if( preg_match($rx_http, $key) ) {
						$arh_key = strtolower(preg_replace($rx_http, '', $key));
						$rx_matches = array();
						$rx_matches = explode('_', $arh_key);
						if( count($rx_matches) > 1 and strlen($arh_key) > 2 ) {
							foreach($rx_matches as $ak_key => $ak_val){
								$rx_matches[$ak_key] = $ak_val;
							}
							$arh_key = implode('-', $rx_matches);
						}else{
							$arh_key = ucfirst($arh_key);
						}
						$headers[$arh_key] = $val;
					}
				}
			}
			return $headers;
		}
		
		public static function getBodyParameter($body){
			$_body = json_decode($body);
			if ((json_last_error() == JSON_ERROR_NONE) && (count((array)$_body)>0)) {
				return $_body;
			}else{
				throw new ServerAuthenException('422','90011','');
			}
		}
		
		public static function validationResponseBody($data, $fieldlistString, $fieldlistArray='', $fieldlistBoolean=''){
		    $keyReturn = '' ;
		    if(count((array)$data) > 0){
		        foreach($fieldlistString as $key){
		            if(!property_exists($data, $key) ||  is_null($data->{$key}) ||$data->{$key} == ''){
		                if(is_string($data->{$key}) && $data->{$key} == ''){
		                    $keyReturn = $keyReturn.$key.', ';
		                }else if(is_null($data->{$key})){
		                    $keyReturn = $keyReturn.$key.', ';
		                }
		            }
		        }
		        if(!empty($fieldlistArray)){
		            foreach ($fieldlistArray as $key){
		                self::validationResponseArray($data->{$key}, $data, $key);
		            }
		        }
		        if(!empty($fieldlistBoolean)){
		            foreach ($fieldlistBoolean as $key){
		                self::validationResponseBoolean($data->{$key}, $data, $key);
		            }
		        }
		    }else{
		        $mandStr = '';
		        foreach ($fieldlistString as $key){
		            $mandStr = $mandStr.', '.$key;
		        }
		        $return= substr($mandStr,2);
		        throw new ServerAuthenException('422', '90005','('.$return.')'.' Missing');
		    }
		    if($keyReturn != ''){
		        $count = strlen($keyReturn);
		        $return= substr($keyReturn,0,$count-2);
		        throw new ServerAuthenException('422', '90005','('.$return.')'.' Missing');
		    }
		}
		
		public static function validationRequestBody($data, $fieldlistString, $fieldlistArray='', $fieldlistBoolean=''){
		    $keyReturn = '' ;
		    if(count((array)$data) > 0){
		        foreach($fieldlistString as $key){
		            if(!property_exists($data, $key) ||  $data->{$key} == ' ' || $data->{$key} == ''){
		                $keyReturn = $keyReturn.$key.', ';
		            }
		        }
		        if(!empty($fieldlistArray)){
		            foreach ($fieldlistArray as $key){
		                self::validationResponseArray($data->{$key}, $data, $key);
		            }
		        }
		        if(!empty($fieldlistBoolean)){
		            foreach ($fieldlistBoolean as $key){
		                self::validationResponseBoolean($data->{$key}, $data, $key);
		            }
		        }
		    }else{
		        $mandStr = '';
		        foreach ($fieldlistString as $key){
		            $mandStr = $mandStr.', '.$key;
		        }
		        $return= substr($mandStr,1);
		        throw new ServerAuthenException('422', '90003','[Hummus] ('.$return.')'.' Missing');
		    }
		    if($keyReturn != ''){
		        $count = strlen($keyReturn);
		        $return= substr($keyReturn,0,$count-2);
		        throw new ServerAuthenException('422', '90003','[Hummus] ('.$return.')'.' Missing');
		    }
		}
		
	}
}
?>