<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @link : https://devportal.ais.co.th/
 * @filename : CommonConfigParameters.php
 *
 */
namespace _server_sdk\model\common{
	
	class CommonConfigParameters{
		protected $secret;
		protected $liveKeyPath;
		protected $email;
	
		public function setSecret($secret){ $this->secret = $secret;}
		public function getSecret(){ return $this->secret;}
		
		public function setLiveKeyPath($liveKeyPath){ $this->liveKeyPath = $liveKeyPath;}
		public function getLiveKeyPath(){ return $this->liveKeyPath;}
	
		public function setEmail($email){ $this->email = $email;}
		public function getEmail(){ return $this->email;}
		
	}
}
?>