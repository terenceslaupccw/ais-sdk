<?php
namespace _server_sdk\model\common{
	
	class CommonAuthenResponse{
		public $resultCode;
		public $developerMessage;
		public $userMessage = '';
		public $moreInfo = '';
	}
}
?>