<?php
namespace _server_sdk\model{
	include_once __DIR__.'/common/CommonAuthenResponse.php';
	use _server_sdk\model\common\CommonAuthenResponse;	

	class LoginResponse extends CommonAuthenResponse{
		public $accessToken = '';
		public $expireIn = '';
		
		public function __construct($param){
			$this->resultCode = $param->{'resultCode'};
			$this->developerMessage = (!property_exists((object) $param,'developerMessage'))? '':$param->{'developerMessage'};
			$this->userMessage = (!property_exists((object) $param,'userMessage'))? '':$param->{'userMessage'};
			$this->moreInfo = (!property_exists((object) $param,'moreInfo'))? '':$param->{'moreInfo'};
			$this->accessToken = (!property_exists((object) $param,'accessToken'))?'':$param->{'accessToken'};
			$this->expireIn = (!property_exists((object) $param,'expireIn'))?'':$param->{'expireIn'};
			return $this;
		}
	}
}
?>