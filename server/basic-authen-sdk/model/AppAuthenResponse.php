<?php
namespace _server_sdk\model{

	class AppAuthenResponse{
		public $accessToken = '';
		public $expireIn = '';
		
		public $idType = '';
		public $idValue = '';
		public $ptsListOfAPI = '';
		public $ptsAppId = '';
		public $ptsAppEnvironmentType = '';
		
		public function __construct($param){
			$this->resultCode = $param->{'resultCode'};
			$this->developerMessage = $param->{'developerMessage'};
			$this->userMessage = (!property_exists((object) $param,'userMessage'))? null : $param->{'userMessage'};
			$this->moreInfo = (!property_exists((object) $param,'moreInfo'))? null:$param->{'moreInfo'};
			
			$this->accessToken = (!property_exists((object) $param,'accessToken'))? null :$param->{'accessToken'};
			$this->expireIn = (!property_exists((object) $param,'expireIn'))? null :$param->{'expireIn'};
			$this->idType = (!property_exists((object) $param,'idType'))? null: $param->{'idType'};
			$this->idValue = (!property_exists((object) $param,'idValue'))? null:$param->{'idValue'};
			$this->gupAuthenLevel = (!property_exists((object) $param,'gupAuthenLevel'))? null :$param->{'gupAuthenLevel'};
			$this->ptsListOfAPI = (!property_exists((object) $param,'ptsListOfAPI'))? null :$param->{'ptsListOfAPI'};
			$this->gupRegistrationLevel = (!property_exists((object) $param,'gupRegistrationLevel'))? null :$param->{'gupRegistrationLevel'};
			$this->openIdFlag = (!property_exists((object) $param,'openIdFlag'))? null:$param->{'openIdFlag'};
			$this->ptsAppId = (!property_exists((object) $param,'ptsAppId'))? null:$param->{'ptsAppId'};
			$this->ptsAppEnvironmentType = (!property_exists((object) $param,'ptsAppEnvironmentType'))? null:$param->{'ptsAppEnvironmentType'};
			
// 			$this->privateId = (!property_exists((object) $param,'privateId'))? '' :$param->{'privateId'};
// 			$this->ptsWindowTime = (!property_exists((object) $param,'ptsWindowTime'))? '' :$param->{'ptsWindowTime'};
// 			$this->ol4 = (!property_exists((object) $param,'ol4'))? '' :$param->{'ol4'};
// 			$this->partnerSession = (!property_exists((object) $param,'partnerSession'))? '':$param->{'partnerSession'};
// 			$this->turnServer = (!property_exists((object) $param,'turnServer'))? '':$param->{'turnServer'};
// 			$this->stunServer = (!property_exists((object) $param,'stunServer'))? '':$param->{'stunServer'};
// 			$this->pwrtcAddress = (!property_exists((object) $param,'pwrtcAddress'))? '':$param->{'pwrtcAddress'};
			
			return $this;
		}
	}
}
?>