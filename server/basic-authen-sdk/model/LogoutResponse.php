<?php
namespace _server_sdk\model{
	include_once __DIR__.'/common/CommonAuthenResponse.php';
	use _server_sdk\model\common\CommonAuthenResponse;	

	class LogoutResponse extends CommonAuthenResponse{
				
		public function __construct($param){
			$this->resultCode = $param->{'resultCode'};
			$this->developerMessage = (!property_exists((object) $param,'developerMessage'))? '':$param->{'developerMessage'};
			$this->userMessage = (!property_exists((object) $param,'userMessage'))? '':$param->{'userMessage'};
			$this->moreInfo = (!property_exists((object) $param,'moreInfo'))? '':$param->{'moreInfo'};
			return $this;
		}
		
	}
}
?>