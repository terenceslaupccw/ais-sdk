<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ClientConfigParameters.php
 *
 */

namespace _server_sdk\model{
		include_once __DIR__.'/common/CommonConfigParameters.php';
		use _server_sdk\model\common\CommonConfigParameters;
	
	class ClientConfigParameters extends CommonConfigParameters{
		public function toArray() {
			$vars = get_object_vars($this);
			$array = array ();
			foreach ($vars as $key => $value ) {
				$array[$key] = $value;
			}
			return $array;
		}
		public function toJsonString(){
			$data = array();
			$jsonString = "";
			$data = $this->toArray();
			$jsonString = json_encode($data);
			return $jsonString;
		}
	}
}
?>