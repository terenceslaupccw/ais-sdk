<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
  * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 24/08/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ClientAuthenUtility.php
 *
 */

namespace _server_sdk\common{
	include_once __DIR__.'/ServerAuthenUtility.php';
    use _server_sdk\exception\ServerAuthenException;
	
	
	class ClientAuthenUtility extends ServerAuthenUtility{
		//Check mandatory email and secret values.
		public static function checkConfigB2C($email){
			if(empty($email)){
				throw new ServerAuthenException('422', '90001','');
			}
		}
		//Check mandatory clientId, authCode, platform, redirectURL and ptsAppEnvironmentType.
		public static function checkRequestPartner($clientId,$authCode,$platform,$redirectURL,$ptsAppEnvironmentType){
			if(empty($clientId) || empty($authCode) || empty($platform) || empty($redirectURL) || empty($ptsAppEnvironmentType)){
				throw new ServerAuthenException('422', '90003','');
			}
		}
		//Response error form pantry to partner.
		public static function returnErrorB2C($errorStatus, $errorDeveloperMessage,$errorUserMessage,$errorCode,$errorMoreInfo) {
			$resData = array (
					'developerMessage' => $errorDeveloperMessage,
					'userMessage' => $errorUserMessage,
					'resultCode' => $errorCode,
					'moreInfo' => $errorMoreInfo
			);
			$logResponseHeaders = headers_list();
			if(count($logResponseHeaders) > 1){
			}else{
				$reqHeaders = self::get_rest_header();
				foreach($reqHeaders as $headerKey => $headerVal){
					if($headerKey == "Cookie"){
						if($reqHeaders[$headerKey] <> ""){
							$cookie = explode(";",$reqHeaders[$headerKey]);
							foreach($cookie as $cookieData){
								list($cookieName,$cookieValue) = explode("=",$cookieData);
								if(self::removeLN($cookieValue)<>""){
									setcookie(trim($cookieName),self::removeLN($cookieValue));
								}else{
									setcookie(trim($cookieName));
								}
							}
						}else{
							header("Set-Cookie: ");
						}
					}else{
						header(trim($headerKey).': '.self::removeLN($headerVal));
					}
				}
		
				$logResponseHeaders = headers_list();
			}
			return json_encode($resData);
		}
		//check environment.
		public static function checkEnvironmentB2C($environment){
			if($environment <> "production" and $environment <> "partner_iot" and $environment <> "internal_iot" and $environment <> "development"){
				throw new ServerAuthenException('422', '90004','');
			}
		}
		//Set number for environment.
		public static function pstEnvironment($environment){
			switch ($environment) {
				case 'production' :
					$setEnvironment = '1';
					break;
				case 'partner_iot' :
					$setEnvironment = '2';
					break;
				case 'internal_iot' :
					$setEnvironment = '3';
					break;
				case 'development' :
					$setEnvironment = '4';
					break;
				default :
					$setEnvironment = '';
					break;
			}
			return $setEnvironment;
		}
		//Search file Livekey in directory.
		public static function checkLiveKeyB2C($xApp,$platform,$liveKeyPath){
			if(strtolower($platform) <> "ios" and strtolower($platform) <> "android"){throw new ServerAuthenException('422','90004','');}
			if($liveKeyPath <> ""){
				list($pantrySideId,$pantryId,$partnerId,$appKeyName) = explode(";",$xApp);
				list($appKey,$appValue) = explode("=",$appKeyName);
				list($appName,$appPlatform,$appVersion) = explode("|",$appValue);
				
				$filesFolder = scandir($liveKeyPath);
				foreach($filesFolder as $fileName){
					if(preg_match('/'.strtolower($appName).'/',strtolower($fileName)) && preg_match('/'.strtolower($platform).'/',strtolower($fileName)) && preg_match('/'.strtolower($appVersion).'/',strtolower($fileName))){
						$liveKeyName = $fileName;
					}
				}
			}
			if($liveKeyName != ""){
				$liveKey = file_get_contents($liveKeyPath.$liveKeyName);
				if(empty($liveKey)){
					throw new ServerAuthenException('422','90002','');
				}else{
					return $liveKey;
				}
			}else{
				throw new ServerAuthenException('422','90002','');
			}
		}
		//Forward error form API backend.
		public function errorResponsePantryB2C($data){
			$body = base64_decode($data['body']);
			$body = json_decode($body);
			//log
			if($data["body"] <> ""){
			}
	
			$header = array();
			preg_match_all('/(?<param>x.+): (?<value>.+)/', $data['header'], $header);
			$finData = array();
			$countHeader = count($header['param']);
			for($i=1;$i<=$countHeader;$i++){
				$finData['header'][trim($header['param'][$i-1])] = self::removeLN($header['value'][$i-1]);
				header(trim($header['param'][$i-1]).': '.self::removeLN($header['value'][$i-1]));
			}
			$cookie = array();
			$strCookie = "";
			preg_match_all('/Set-Cookie:(?<cookie>.+)$/im', $data['header'], $cookie);
			$countCookie = count($cookie['cookie']);
			for($i=1;$i<=$countCookie;$i++){
				$cookcook = explode(";", $cookie['cookie'][$i-1]);
				$cook = $cookcook[0];
				$var_cook = explode("=", $cook);
				if($var_cook[1] != ""){
					$strCookie .= ";".trim($var_cook[0])."=".self::removeLN($var_cook[1]);
				}
				if(self::removeLN($var_cook[1]) <> "" and trim($var_cook[0]) <> ""){
					setcookie(trim($var_cook[0]),self::removeLN($var_cook[1]));
				}elseif(self::removeLN($var_cook[1]) == "" and trim($var_cook[0]) <> ""){
					setcookie(trim($var_cook[0]));
				}
			}
			if($strCookie == "" and $countCookie>0){
				header("Set-Cookie: ");
			}
	
			if ((json_last_error() == JSON_ERROR_NONE) and (count($body) > 0)) {
				$returnData = (object)[];
				$returnData->{'developerMessage'} = $body->{'developerMessage'};
				$returnData->{'resultCode'} = $body->{'resultCode'};
				if(isset($body->{'userMessage'})){
					$returnData->{'userMessage'} = $body->{'userMessage'};
				}else{
					$returnData->{'userMessage'} = '';
				}
				if(isset($body->{'moreInfo'})){
				$returnData->{'moreInfo'} = $body->{'moreInfo'};
				}else{
				$returnData->{'moreInfo'} = '';
				}
				return json_encode($returnData);
			}else{
				throw new ServerAuthenException('422', '90011','');
			}
		}
		//Return header.
		public static function checkRestHeader(){
			$restHeader = self::get_rest_header();
			$pieces =array();
				if(array_key_exists('x-session-id', $restHeader)){
					if($restHeader['x-session-id'] == '') array_push($pieces, 'x-session-id');
				}else{
					array_push($pieces, 'x-session-id');
				}
				if(array_key_exists('x-app', $restHeader)){
					if($restHeader['x-app'] == '') array_push($pieces, 'x-app');
				}else{
					array_push($pieces, 'x-app');
				}
				if(array_key_exists('x-user-id', $restHeader)){
					if($restHeader['x-user-id'] == '') array_push($pieces, 'x-user-id');
				}else{
					array_push($pieces, 'x-user-id');
				}
				if(array_key_exists('Cookie', $restHeader)){
					if($restHeader['Cookie'] == '') array_push($pieces, 'Cookie');
				}else{
					array_push($pieces, 'Cookie');
				}

			$piecesCount = count($pieces);
			if($piecesCount!=0){
				$param = implode(', ', $pieces);
				throw new ServerAuthenException('422', '90005', $param);
			}else{
				$header = array();
					$header['x-session-id'] = $restHeader['x-session-id'];
					$header['x-app'] = $restHeader['x-app'];
					$header['x-user-id'] = $restHeader['x-user-id'];
					$header['Cookie'] = $restHeader['Cookie'];
					if (array_key_exists("x-access-token",$restHeader)) $header['x-access-token'] = $restHeader['x-access-token'];
					if (array_key_exists("x-private-id",$restHeader)) $header['x-private-id'] = $restHeader['x-private-id'];
			}
			return $header;
		}
		
		//Get header.
		public static function get_rest_header(){
			if(function_exists('apache_request_headers')) {
				$headers = apache_request_headers();
			}elseif(function_exists('getallheaders')){
				$headers = getallheaders();
			}else{	
				$headers = array();
				$rx_http = '/\AHTTP_/';
				foreach($_SERVER as $key => $val) {
					if( preg_match($rx_http, $key) ) {
						$arh_key = strtolower(preg_replace($rx_http, '', $key));
						$rx_matches = array();
						// do some nasty string manipulations to restore the original letter case
						// this should work in most cases
						$rx_matches = explode('_', $arh_key);
						if( count($rx_matches) > 1 and strlen($arh_key) > 2 ) {
							foreach($rx_matches as $ak_key => $ak_val){
								$rx_matches[$ak_key] = $ak_val;
							}
							$arh_key = implode('-', $rx_matches);
						}else{
							$arh_key = ucfirst($arh_key);
						}
						$headers[$arh_key] = $val;
					}
				}
			}
			return $headers;
		}
		
		//Get body from partner.
		public function getBodyParameter($body){
			$_body = json_decode($body);
			if ((json_last_error() == JSON_ERROR_NONE) and (count($_body)>0)) {
				return $_body;
			}else{
				
				throw new ServerAuthenException('422','90011','');
			}
		}
	}
}
?>