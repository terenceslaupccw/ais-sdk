<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 25/08/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : Config.php
 *
 */

namespace _server_sdk\common{
	class Configuration{
	    private $initConfig = "ew0KICAgICJwdHNTREsiOiJiYXNpY0F1dGhlbiIsDQogICAgInB0c0FwcEVudmlyb25tZW50VHlwZSI6ew0KICAgICAgICAiQjJDIjp7DQogICAgICAgICAgICAiMSI6Imh0dHBzOi8vYXBpY20uYWlzLmNvLnRoOjE1MzAwIiwNCiAgICAgICAgICAgICIyIjoiaHR0cHM6Ly9hcGlwZy5haXMuY28udGg6MTUzMDAiLA0KICAgICAgICAgICAgIjMiOiJodHRwczovLzEwLjEzNy4xNi41NzoxNTMwMCIsDQogICAgICAgICAgICAiNCI6Imh0dHA6Ly8xMC4xMDQuMjQwLjE3Mjo2MTAzIg0KICAgICAgICB9LA0KICAgICAgICAiQjJCIjp7DQogICAgICAgICAgICAiMSI6Imh0dHBzOi8vYXBpY20uYWlzLmNvLnRoOjE1NDAwIiwNCiAgICAgICAgICAgICIyIjoiaHR0cHM6Ly9hcGkuYWlzLmNvLnRoOjE0NTAwIiwNCiAgICAgICAgICAgICIzIjoiaHR0cHM6Ly9hcGlwZy5haXMuY28udGg6MTU0MDAiLA0KICAgICAgICAgICAgIjQiOiJodHRwczovL2FwaXBnLmFpcy5jby50aDoxNTQwMCIsDQogICAgICAgICAgICAiNSI6Imh0dHA6Ly8xMC4xMDQuMjQwLjE3Mjo2MTAzIg0KICAgICAgICB9DQogICAgfSwNCiAgICAiYmFzaWNBdXRoZW4iOnsNCiAgICAgICAgImF1dGhlbl91cmwiOiIvb2J0YWluQXV0aG9yaXR5TGlzdCIsDQogICAgICAgICJsb2dpbl91cmwiOiIvdjEvbG9naW5CeUIyQi5qc29uIiwNCiAgICAgICAgImtlZXBhbGl2ZV91cmwiOiIvdjEva2VlcEFsaXZlLmpzb24iLA0KICAgICAgICAibG9nb3V0X3VybCI6Ii92MS9sb2dvdXQuanNvbiINCiAgICB9DQp9";
		private $methodType;
		private $ptsAppEnvironmentType;
		private $authenType;
		
		//Set parameter PtsAppEnvironmentType and methodType.
		public function __construct($environment='', $methodType,$authenType="B2C") {
			$this->setPtsAppEnvironmentType($environment);
			$this->setMethodType($methodType);
			$this->setAuthenType($authenType);
		}
		//Decode initConfig and call method __parseURL for get URL from initConfig.
		public function initConfiguration(){
			$config = $this->__decodeBase64($this->initConfig);
			return $this->__parseURL($config,$this->getPtsAppEnvironmentType(),$this->getMethodType());
		}
		//Decode base64 format.
		private function __decodeBase64($param){
			return base64_decode($param);
		}
		//Decode json format.
		private function __jsonDecode($param){
			return json_decode($param,true);
		}
		//Get url from $config.
		private function __parseURL($config,$environment,$methodType){
			
			$parseURL = $this->__jsonDecode($config);

			if($this->getAuthenType() == "B2B"){
				$url = array(
					"host" => $parseURL['ptsAppEnvironmentType'][$this->getAuthenType()][$environment],
					"path" => $parseURL[$parseURL['ptsSDK']][$methodType]
				);
				if($environment == "development") $url["path"] = "";
				
			}else{
				$url = array(
					"host" => $parseURL['ptsAppEnvironmentType'][$this->getAuthenType()][$environment],
					"path" => $parseURL[$parseURL['ptsSDK']][$methodType]
				);				
				if($environment == "development") $url["path"] = "";
				
			}							
			return $url;
		}
			
		private function setPtsAppEnvironmentType($param){$this->ptsAppEnvironmentType = $param;}
		private function getPtsAppEnvironmentType(){return $this->ptsAppEnvironmentType;}
		
		private function setMethodType($param){$this->methodType = $param;}
		private function getMethodType(){return $this->methodType;}
		
		private function setAuthenType($param){$this->authenType = $param;}
		private function getAuthenType(){return $this->authenType;}
	}
}
?>