<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 24/08/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ServerAuthenUtility.php
 *
 */

namespace _server_sdk\common{
    include_once __DIR__.'/CommonAuthen.php';
    use _server_sdk\exception\ServerAuthenException;
    
    
    class ServerAuthenUtility extends CommonAuthen{
        //Check mandatory value clientId, email and secret.
        public static function checkConfigB2B($clientId,$email,$secret,$typeCheck="N"){
            if(empty($clientId) || empty($email)){
                throw new ServerAuthenException('422', '90001','');
            }
            if($typeCheck=="Y"){
                if(empty($secret)){
                    throw new ServerAuthenException('422', '90001','');
                }
            }
        }
        //Decode accessToken for get header.
        public static function checkData($clientId,$accessToken){
            // Check AccessToken
            $header = self::decAccesstoken($accessToken,$clientId);
            
            
            // check data for accesstoken array
            if(!is_array($header)){
                throw new ServerAuthenException('422', '90004','');
            }
            return $header;
        }
        //Replace 0 in phone number to 66
        public static function callingCode($param){
            if($param[0] == '0'){
                $msisdn = "66".substr($param,1);
            }elseif($param[0].$param[1] == "66"){
                $msisdn = $param;
            }else{
                throw new ServerAuthenException ( '422', '90004', '' );
            }
            return $msisdn;
        }
        
        //Forward error from API backend.
        // 		public static function returnError($errorStatus,$errorDeveloperMessage,$errorUserMessage,$errorCode,$errorMoreInfo) {
        // 			$resData = array (
        // 					'resultCode' => $errorCode,
        // 					'developerMessage' => $errorDeveloperMessage,
        // 					'userMessage' => $errorUserMessage,
        // 					'moreInfo' => $errorMoreInfo
        // 			);
        // 			return $resData;
        // 		}
        
        //Return error from API backend.
        public static function errorResponsePantry($data){
            $body = json_decode($data['body'],TRUE);
            //log
            
            // Check JSON FORMAT
            if ((json_last_error() == JSON_ERROR_NONE) and (count($body) > 0)) {
                $returnData = array();
                $returnData['developerMessage'] = $body['developerMessage'];
                $returnData['resultCode'] = $body['resultCode'];
                if (array_key_exists("userMessage",$body)){
                    $returnData['userMessage'] = $body['userMessage'];
                }else{
                    $returnData['userMessage'] = null;
                }
                if (array_key_exists("moreInfo",$body)){
                    $returnData['moreInfo'] = $body['moreInfo'];
                }else{
                    $returnData['moreInfo'] = null;
                }
                return $returnData;
            }else{
                throw new ServerAuthenException('422', '90005',$data['body']);
            }
        }
        //Check value environment from partner.
        public static function checkEnvironmentB2B($environment){
            if($environment == ''){
                throw new ServerAuthenException('422', '90003','');
            }elseif(is_numeric($environment)){
                if($environment >= 1 and $environment <= 5){
                    switch ($environment) {
                        case '1' :
                            $setEnvironment = 'production';
                            break;
                        case '2' :
                            $setEnvironment = 'pre_prod';
                            break;
                        case '3' :
                            $setEnvironment = 'partner_iot';
                            break;
                        case '4' :
                            $setEnvironment = 'internal_iot';
                            break;
                        case '5' :
                            $setEnvironment = 'development';
                            break;
                        default :
                            throw new ServerAuthenException('422', '90004','');
                            break;
                    }
                    return $setEnvironment;
                }else{
                    throw new ServerAuthenException('422', '90004','');
                }
            }else{
                throw new ServerAuthenException('422', '90004','');
            }
        }
        
        //Check value environment from partner.
        public static function checkValidateEnvironment($environment){
            if($environment == ''){
                throw new ServerAuthenException('422', '90003','');
            }elseif(is_numeric($environment)){
                if($environment < 1 and $environment > 4){
                    throw new ServerAuthenException('422', '90004','');
                }
            }else{
                throw new ServerAuthenException('422', '90004','');
            }
        }
        
    }
}
?>