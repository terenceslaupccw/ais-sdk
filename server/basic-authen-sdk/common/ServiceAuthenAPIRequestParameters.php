<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 25/08/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ServiceAuthenAPIRequestParameters.php
 *
 */

namespace _server_sdk\common{
	
	class ServiceAuthenAPIRequestParameters{
		private $initConfig;
		private $initPath;
		private $initBody;
		private $initHeader;
		private $initTimeout;
		private $initMethod;
		private $initAuthenURL;
		private $initApiURL;
		
		public function setInitConfig($param){ $this->initConfig = $param;}
		public function getInitConfig(){ return $this->initConfig;}
		
		public function setInitPath($param){ $this->initPath = $param;}
		public function getInitPath(){ return $this->initPath;}
		
		public function setInitBody($param){$this->initBody = $param;}
		public function getInitBody(){ return $this->initBody;}
		
		public function setInitHeader($param){ $this->initHeader = $param;}
		public function getInitHeader(){ return $this->initHeader;}
		
		public function setInitTimeout($param){ $this->initTimeout = $param;}
		public function getInitTimeout(){ return $this->initTimeout;}
		
		public function setInitMethod($param){ $this->initMethod = $param;}
		public function getInitMethod(){ return $this->initMethod;}
		
		public function setInitAuthenURL($param){ $this->initAuthenURL = $param;}
		public function getInitAuthenURL(){ return $this->initAuthenURL;}
		
		public function setInitApiURL($param){ $this->initApiURL = $param;}
		public function getInitApiURL(){ return $this->initApiURL;}
	}
}
?>