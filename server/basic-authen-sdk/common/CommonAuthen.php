<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
  * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 25/08/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : CommonAuthen.php
 *
 */

namespace _server_sdk\common{
	include_once __DIR__.'/../exception/ServerAuthenException.php';
	
	use _server_sdk\exception\ServerAuthenException;
    use phpseclib\Crypt\AES;
    use phpseclib\Crypt\RSA;
	
	class CommonAuthen{
		//Generate signature.
		public static function genSignature($dataSignature,$publicKey){
			$hash = hash("sha256",$dataSignature);
			$hash = self::hex2bin($hash);
		
			$rsa = new RSA();
			$rsa->loadKey($publicKey);
			$rsa->setEncryptionMode(ENCRYPTION_PKCS1);
			$ciphertext = $rsa->encrypt($hash);
			$signature = base64_encode($ciphertext);
		
		
			return $signature;
		}
		//Generate AccessToken.
		public static function encAccesstoken($finData,$clientId){
			$accesstoken = array(
					'Cookie'			=>	$finData['header']['Cookie'],
					'x-session-id'		=>	$finData['header']['x-session-id'],
					'x-app'				=>	$finData['header']['x-app'],
					'x-user-id'			=>	$finData['header']['x-user-id'],
					'x-access-token'	=>	$finData['header']['x-access-token'],
					'authenURL'			=>	$finData['header']['authenURL'],
					'apiURL'			=>	$finData['header']['apiURL']
			);
			$accesstoken = json_encode($accesstoken);
			$accesstoken = self::removeLN($accesstoken);
		
			$hashClientId = hash('sha256', $clientId);
			$cipher = new AES();
			$cipher->setKey($hashClientId);
			$cipher->setIV($clientId);
			$encAccesstoken = base64_encode($cipher->encrypt($accesstoken));
		
		
			return $encAccesstoken;
		}
		//encode data from string to binary.
		public static function hex2bin($hexdata) {
			$bindata = "";
			for ($i = 0; $i < strlen($hexdata); $i += 2) {
				$bindata .= chr(hexdec(substr($hexdata, $i, 2)));
			}
			return $bindata;
		}
		//Replace " " become "".
		public static function removeLN($str){
			$replace = str_replace(chr(13), "",$str);
			return $replace;
		}
		//Decode AccesccToken.
		public static function decAccesstoken($accesstoken,$clientId){
			$base64Acc = base64_decode($accesstoken);
			$hashClientId = hash('sha256', $clientId);
			$cipher = new AES();
			$cipher->setKey($hashClientId);
			$cipher->setIV($clientId);
			$decAES =$cipher->decrypt($base64Acc);
			return json_decode($decAES,TRUE);
		}
		//Check validation of header response from API backend. 
		public static function validationResponseHeader($data,$fieldlist){
			if(count($data) > 0){
				foreach ($fieldlist as $key){
					if(is_array($data[$key])){
		
						if(count($data[$key]) <= 0){
							throw new ServerAuthenException('422', '90005','');
						}
					}else{
						if($data[$key] == ''){
							throw new ServerAuthenException('422', '90005','');
						}
					}
				}//foreach
			}else{
				throw new ServerAuthenException('422', '90005','');
			}
		}
		//Check validation of body response from API backend.
		public static function validationResponseBody($data,$fieldlist){
			if(count($data) > 0){
				foreach ($fieldlist as $key){
					if(is_array($data[$key])){
						if(count($data[$key]) <= 0){
							throw new ServerAuthenException('422', '90005',json_encode($data));
						}
					}else{
						if($key == 'openIdFlag' or $key == 'isTravellerSim'){
							if(json_encode($data[$key]) == '' || json_encode($data[$key]) == '""' || json_encode($data[$key]) == 'null'){
								throw new ServerAuthenException('422', '90005',json_encode($data));
							}
						}else{
							if($data[$key] == ''){
								throw new ServerAuthenException('422', '90005',json_encode($data));
							}
						}
					}
				}//foreach
			}else{
				throw new ServerAuthenException('422', '90005',json_encode($data));
			}
		}
		//Combine clientId and email then encode to base64.  
		public static function encodeEmail($clientID,$email){
			$newEmail = "";
		
			$totalStr = strlen($clientID);
			$diffStr = floor($totalStr / 2);
		
			$clientPath1 = substr($clientID,0,$diffStr);
			$clientPath2 = substr($clientID,$diffStr);
		
			list($emailPath1,$emailPath2) = explode("@",$email);
			$newEmail = $emailPath1."@".$clientPath1.$emailPath2.$clientPath2;
		
			if(strlen($clientPath1) < strlen($clientPath2)){
			}elseif(strlen($clientPath1) > strlen($clientPath2)){
			}else{
			}
			$encodeEmail = base64_encode(self::hex2bin(hash('sha256', $newEmail)));
			return $encodeEmail;
		}
		
		public static function writeLog($str) {
			$logDate = date("Ymd");
			$file_name = "D:/Hummus-Log-PHP/Hummus-B2B-Log\Log_".$logDate.".txt";
			if(file_exists($file_name)){
				$objFopen = fopen($file_name, 'a') or die("couldn't open file <i>$file_name</i>");
				rewind($objFopen);
				fwrite( $objFopen, $str . "\r\n" );
			}else{
				$objFopen = fopen($file_name, 'w');
				rewind($objFopen);
				fwrite($objFopen, $str . "\r\n" );
			}
			fclose($objFopen);
		}
	}
}