<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @modify by : Krisana A.
 * @modify date : 01/06/2016
 * @link : https://devportal.ais.co.th/
 * @filename : HummusSecret.php
 *
 */
namespace _server_sdk\common{
	include_once __DIR__.'/../exception/ServerAuthenException.php';
	include_once __DIR__.'/File/X509.php';
	
	use _server_sdk\exception\ServerAuthenException;
	class HummusSecret{
		public function __construct(){}
		//Read secret file(certificate file).
		public function readSeret($file){
			$cert = "";
			if(file_exists($file) and $file != ""){
				$string = $this->file_get_contents($file);
				$cert = $this->readCRT($string);
			}else{throw new ServerAuthenException('422', '90002','');}
			return $cert;
		}
		//Get value in certificate file.
		private function file_get_contents($input){
			return file_get_contents($input);
		}
		//Read certificate file.
		public function readCRT($string){
			//Start function X509
			$x509 = new \File_X509();
			$cert = $x509->loadX509($string);
			return $cert;
		}
	}
}
?>