<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 25/08/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ServerAuthenManagerImpl.php
 *
 */

namespace _server_sdk\service{
	include_once __DIR__.'/ServerAuthenManager.php';
	include_once __DIR__.'/../utils/ServerAuthenUtility.php';
	include_once __DIR__.'/../builder/ServerAuthenBuilder.php';
	include_once __DIR__.'/../security/ServiceAuthenSecurity.php';
	include_once __DIR__.'/../exception/ServerAuthenException.php';
	include_once __DIR__.'/api/ServerAuthenAPIManagerImpl.php';
	
	include_once __DIR__.'/../validation/ServiceAuthenValidation.php';
	include_once __DIR__.'/../model/LoginResponse.php';
	include_once __DIR__.'/../model/KeepAliveResponse.php';
	include_once __DIR__.'/../model/LogoutResponse.php';
	
	use _server_sdk\service\ServerAuthenManager;
	use _server_sdk\utils\ServerAuthenUtility;
	use _server_sdk\builder\ServerAuthenBuilder;
	use _server_sdk\security\ServiceAuthenSecurity;
	use _server_sdk\exception\ServerAuthenException;
	use _server_sdk\service\api\ServerAuthenAPIManagerImpl;
	
	use _server_sdk\validation\ServiceAuthenValidation;
	use _server_sdk\model\LoginResponse;
	use _server_sdk\model\KeepAliveResponse;
	use _server_sdk\model\LogoutResponse;
	
	
	class ServerAuthenManagerImpl implements ServerAuthenManager{
		private $serviceAPIParameters;
		private $loginResponse;
		private $keepAliveResponse;
		private $logoutResponse;
		private $errorResponse;
		private static $serverAuthenAPIManagerImpl;
		
		//Singleton.
		public static function getServerAuthenAPIInstance(){
			if(!isset(self::$serverAuthenAPIManagerImpl)){
				self::$serverAuthenAPIManagerImpl = new ServerAuthenAPIManagerImpl();
			}
			return self::$serverAuthenAPIManagerImpl;
		}

		public function login(){
			try{
				//Send parameter cliendId, Email and Secret to function checkConfigB2B for check validation.
				//ServiceAuthenValidation::checkConfigB2B(ServerAuthenBuilder::$serviceConfig->getClientId(),ServerAuthenBuilder::$serviceConfig->getEmail(),ServerAuthenBuilder::$serviceConfig->getSecret(),'y');
				
				//Send parameter Environment check validation.
				//ServiceAuthenValidation::checkValidateEnvironment(ServerAuthenBuilder::$serviceConfig->getEnvironment());
				
			    $fieldlist = array('email','clientId','secret','environment');
			    ServiceAuthenValidation::validationConfigB2B(ServerAuthenBuilder::$serviceConfig,$fieldlist);
			    
				//Sent parameter ClientId and Email to method encodeEmail for compare with liveKey's value.
				$encodeEmail = ServiceAuthenSecurity::encodeEmail(ServerAuthenBuilder::$serviceConfig->getClientId(),ServerAuthenBuilder::$serviceConfig->getEmail());
				
				//Check liveKey file and value in liveKey file.
				if(file_exists(ServerAuthenBuilder::$serviceConfig->getLiveKeyPath())){
					$liveKey = file_get_contents(ServerAuthenBuilder::$serviceConfig->getLiveKeyPath());
					if(empty($liveKey)){
						throw new ServerAuthenException('422', '90002','[Hummus] LiveKey path file not found');
					}
				}else{
					throw new ServerAuthenException('422', '90002','[Hummus] LiveKey path file not found');
				}
				
				//Compare value liveKey and encodeEmail.
				if($liveKey == $encodeEmail){
					$_secret = ServiceAuthenSecurity::readSecret(ServerAuthenBuilder::$serviceConfig->getSecret());
					//Get publicKey in secret file.
					$publicKey = $_secret['tbsCertificate']['subjectPublicKeyInfo']['subjectPublicKey'];
					if($publicKey == ''){
						throw new ServerAuthenException('422', '90002','[Hummus] Secret path file not found');
					}
					//Set timeStamp.
					$milliseconds = round(microtime(true) * 1000);
					$hostname = "";
					//Get server's name.
					if (function_exists('php_uname')) {
						$hostname = php_uname('n');
					}else{
						$hostname = gethostname();
					}
					//Generate signature.
					$stringSignature = ServerAuthenBuilder::$serviceConfig->getClientId().$milliseconds.$hostname;
					$signature = ServiceAuthenSecurity::genSignature($stringSignature,$publicKey);
					
					$this->serviceAPIParameters = (object)[];
					$this->serviceAPIParameters->timeStamp = $milliseconds;
					$this->serviceAPIParameters->serverId = $hostname;
					$this->serviceAPIParameters->signature = $signature;
					
					//Call singerton and send parameter to method loginByB2B.
					$serverAuthenAPIManagerImpl = self::getServerAuthenAPIInstance();
					$loginResponse = $serverAuthenAPIManagerImpl->loginByB2B($this->serviceAPIParameters);

					$this->loginResponse = new LoginResponse($loginResponse);
					return $this->loginResponse;
					
				}else{
					throw new ServerAuthenException('422', '90002','[Hummus] LiveKey not match in Livekey file');
				}
			}catch (ServerAuthenException $e){
				$this->errorResponse = (object)[];
				$this->errorResponse->resultCode = $e->getErrorCode();
				$this->errorResponse->developerMessage = $e->getDeveloperMessage();
				$this->errorResponse->userMessage = $e->getUserMessage();
				$this->errorResponse->moreInfo = $e->getMoreInfo();
				//Response error form pantry to partner.
				$this->loginResponse = new LoginResponse($this->errorResponse);
				return $this->loginResponse;
			}
		} 
		
		public function keepAlive($paramResult){
			try {
				
				//Send parameter cliendId, Email and Secret to function checkConfigB2B for check validation.
				//ServiceAuthenValidation::checkConfigB2B(ServerAuthenBuilder::$serviceConfig->getClientId(),ServerAuthenBuilder::$serviceConfig->getEmail());
				
				//Send parameter Environment check validation.
				//ServiceAuthenValidation::checkValidateEnvironment(ServerAuthenBuilder::$serviceConfig->getEnvironment());
				
			    $fieldlist = array('email','clientId','secret','environment');
			    ServiceAuthenValidation::validationConfigB2B(ServerAuthenBuilder::$serviceConfig,$fieldlist);
			    
				//Check liveKey file and value in liveKey file.
				if(file_exists(ServerAuthenBuilder::$serviceConfig->getLiveKeyPath())){
					$liveKey = file_get_contents(ServerAuthenBuilder::$serviceConfig->getLiveKeyPath());
					if(empty($liveKey)){ 
						throw new ServerAuthenException('422', '90002','[Hummus] LiveKey path file not found');
					}
				}else{
					throw new ServerAuthenException('422', '90002','[Hummus] LiveKey path file not found');
				}
				
				//Sent parameter ClientId and Email to method encodeEmail for compare with liveKey's value.
				$encodeEmail = ServiceAuthenSecurity::encodeEmail(ServerAuthenBuilder::$serviceConfig->getClientId(),ServerAuthenBuilder::$serviceConfig->getEmail());
				
				//Compare value liveKey and encodeEmail
				if($liveKey == $encodeEmail){
					//Validate accessToken.
					if(!property_exists($paramResult,'accessToken') || empty($paramResult->{'accessToken'})){
						throw new ServerAuthenException('422', '90003',"[Hummus] {'accessToken'} Missing");
					}
					$this->serviceAPIParameters = (object)[];
					$this->serviceAPIParameters->accessToken = $paramResult->{'accessToken'};
					
					$decrpt = ServiceAuthenSecurity::decAccesstoken(ServerAuthenBuilder::$serviceConfig->getClientId(),$paramResult->{'accessToken'});
					$decrptA = json_encode($decrpt);
					//Call singerton and send parameter to method keepAlive.
					$serverAuthenAPIManagerImpl = self::getServerAuthenAPIInstance();
					$keepAliveResponse = $serverAuthenAPIManagerImpl->keepAlive($this->serviceAPIParameters);
					
					$this->keepAliveResponse = new KeepAliveResponse($keepAliveResponse);
					
					return $this->keepAliveResponse;
				}else{
					throw new ServerAuthenException('422', '90002','[Hummus] LiveKey not match in Livekey file');
				}
			}catch (ServerAuthenException $e){
				//Response error form pantry to partner.
				$this->errorResponse = (object)[];
				$this->errorResponse->resultCode = $e->getErrorCode();
				$this->errorResponse->developerMessage = $e->getDeveloperMessage();
				$this->errorResponse->userMessage = $e->getUserMessage();
				$this->errorResponse->moreInfo = $e->getMoreInfo();
				//Response error form pantry to partner.
				$this->keepAliveResponse = new KeepAliveResponse($this->errorResponse);
				return $this->keepAliveResponse;
			}
		}
		
		public function logout($paramResult){
			try {
				//Send parameter cliendId, Email and Secret to function checkConfigB2B for check validation.
				//ServiceAuthenValidation::checkConfigB2B(ServerAuthenBuilder::$serviceConfig->getClientId(),ServerAuthenBuilder::$serviceConfig->getEmail(),'','n');
				
				//Send parameter Environment check validation.
				//ServiceAuthenValidation::checkValidateEnvironment(ServerAuthenBuilder::$serviceConfig->getEnvironment());

			    $fieldlist = array('email','clientId','secret','environment');
			    ServiceAuthenValidation::validationConfigB2B(ServerAuthenBuilder::$serviceConfig,$fieldlist);
			    
				//Check liveKey file and value in liveKey file.
				if(file_exists(ServerAuthenBuilder::$serviceConfig->getLiveKeyPath())){
					$liveKey = file_get_contents(ServerAuthenBuilder::$serviceConfig->getLiveKeyPath());
					if(empty($liveKey)){ 
						throw new ServerAuthenException('422', '90002','[Hummus] LiveKey path file not found');
					}
				}else{
					throw new ServerAuthenException('422', '90002','[Hummus] LiveKey path file not found');
				}
				//Sent parameter ClientId and Email to method encodeEmail for compare with liveKey's value.
				$encodeEmail = ServiceAuthenSecurity::encodeEmail(ServerAuthenBuilder::$serviceConfig->getClientId(),ServerAuthenBuilder::$serviceConfig->getEmail());
				//Compare value liveKey and encodeEmail
				if($liveKey == $encodeEmail){
					if(!property_exists($paramResult,'accessToken') || empty($paramResult->{'accessToken'})){
						throw new ServerAuthenException('422', '90003',"[Hummus] {'accessToken'} Missing");
					}
						
					$this->serviceAPIParameters = (object)[];
					$this->serviceAPIParameters->accessToken = $paramResult->{'accessToken'};
					$decrpt = ServiceAuthenSecurity::decAccesstoken(ServerAuthenBuilder::$serviceConfig->getClientId(),$paramResult->{'accessToken'});
					$decrptA = json_encode($decrpt);
					//Call singerton and send parameter to method logout.
					$serverAuthenAPIManagerImpl = self::getServerAuthenAPIInstance();
					$logoutResponse = $serverAuthenAPIManagerImpl->logout($this->serviceAPIParameters);
	
					$this->logoutResponse = new LogoutResponse($logoutResponse);
					
					return $this->logoutResponse;
				}else{
					throw new ServerAuthenException('422', '90002','[Hummus] LiveKey not match in Livekey file');
				}
			}catch (ServerAuthenException $e){
				//Response error form pantry to partner.
				$this->errorResponse = (object)[];
				$this->errorResponse->resultCode = $e->getErrorCode();
				$this->errorResponse->developerMessage = $e->getDeveloperMessage();
				$this->errorResponse->userMessage = $e->getUserMessage();
				$this->errorResponse->moreInfo = $e->getMoreInfo();
				//Response error form pantry to partner.
				$this->logoutResponse = new LogoutResponse($this->errorResponse);
				
				return $this->logoutResponse;
			}
		}
	}
}
?>