<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 25/08/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ServerAuthenManager.php
 *
 */

namespace _server_sdk\service{
	
	interface ServerAuthenManager{
		public function login();
		public function keepAlive($paramResult);
		public function logout($paramResult);
	}
}
?>