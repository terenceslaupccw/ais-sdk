<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ServerAuthenParameters.php
 *
 */

namespace _server_sdk\service\param{
	include_once __DIR__.'/CommonParameters.php';
	use _server_sdk\service\param\CommonParameters;	

	class ServerAuthenParameters extends CommonParameters{
		private $timeStamp;
		private $serverId;
		private $acessToken;
		
		private $deviceId;
		private $channel;
		private $appName;
		private $commandId;
		private $authenType;
		private $defaultTemplate;
		private $lang;
		private $anPublicId;
		private $privateId;
		private $ptsAppEnvironmentType;
		
		public function setTimeStamp($param) { $this->timeStamp = $param;}
		public function getTimeStamp(){ return $this->timeStamp;}
		public function setServerId($param) { $this->serverId = $param;}
		public function getServerId(){ return $this->serverId;}
		public function setAccessToken($param){ $this->acessToken = $param;}
		public function getAccessToken(){ return $this->acessToken;}
		
		public function setDeviceId($param){ $this->deviceId = $param;}
		public function getDeviceId(){ return  $this->deviceId;}
		public function setChannel($param){ $this->channel = $param;}
		public function getChannel(){ return  $this->channel;}
		public function setAppName($param){ $this->appName = $param;}
		public function getAppName(){ return  $this->appName;}
		public function setCommandId($param){ $this->commandId = $param;}
		public function getCommandId(){ return  $this->commandId;}
		public function setAuthenType($param){ $this->authenType = $param;}
		public function getAuthenType(){ return  $this->authenType;}
		public function setDefaultTemplate($param){ $this->defaultTemplate = $param;}
		public function getDefaultTemplate(){ return  $this->defaultTemplate;}
		public function setLang($param){ $this->lang = $param;}
		public function getLang(){ return  $this->lang;}
		public function setAnPublicId($param){ $this->anPublicId = $param;}
		public function getAnPublicId(){ return  $this->anPublicId;}
		public function setPrivateId($param){ $this->privateId = $param;}
		public function getPrivateId(){ return  $this->privateId;}
		public function setPtsAppEnvironmentType($param){ $this->ptsAppEnvironmentType = $param;}
		public function getPtsAppEnvironmentType(){ return  $this->ptsAppEnvironmentType;}
		
	
	}
}
?>