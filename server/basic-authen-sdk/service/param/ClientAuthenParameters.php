<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ClientAuthenParameters.php
 *
 */

namespace _server_sdk\service\param{
	include_once __DIR__.'/CommonParameters.php';
	use _server_sdk\service\param\CommonParameters;
	
	class ClientAuthenParameters extends CommonParameters{
		private $x_requester;
		private $grantType;
		private $timeStamp;
		
		private $authCode;
		private $platform;
		private $redirectURL;
		private $commandId;
		private $ptsAppEnvironmentType;
		
		public function getXRequester() {return $this->x_requester;}
		public function setXRequester($param) {$this->x_requester = $param;}
		public function getGrantType() {return $this->grantType;}
		public function setGrantType($grantType) {$this->grantType = $grantType;}
		public function getTimeStamp(){return $this->timeStamp;}
		public function setTimeStamp($param) {$this->timeStamp = $param;}
		
		public function getAuthCode() {return $this->authCode;}
		public function setAuthCode($param) {$this->authCode = $param;}
		public function getPlatform(){return $this->platform;}
		public function setPlatform($param){$this->platform = $param; }
		public function getRedirectURL() {return $this->redirectURL;}
		public function setRedirectURL($param) {$this->redirectURL = $param;}
		public function getCommandId(){return $this->commandId;}
		public function setCommandId($param){$this->commandId = $param; }
		public function getPtsAppEnvironmentType(){return $this->ptsAppEnvironmentType;}
		public function setPtsAppEnvironmentType($param){$this->ptsAppEnvironmentType = $param;}
	}
}
?>