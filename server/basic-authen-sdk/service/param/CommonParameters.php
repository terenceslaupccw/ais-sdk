<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @link : https://devportal.ais.co.th/
 * @filename : CommonParameters.php
 *
 */

namespace _server_sdk\service\param{
	
	class CommonParameters{
		protected $clientId;
		protected $email;
		protected $secret;
		protected $liveKeyPath;
		protected $environment;
		
		protected $publicKey;
		protected $signature;
		
		protected $cookie;
		protected $x_session_id;
		protected $x_app;
		protected $x_user_id;
		protected $x_access_token;
		protected $x_private_id;
		
		protected $authenURL;
		protected $apiURL;
		
		public function getClientId(){ return $this->clientId;}
		public function setClientId($param) { $this->clientId = $param;}
		public function getEmail(){ return $this->email;}
		public function setEmail($param) { $this->email = $param;}
		public function getSecret(){ return $this->secret;}
		public function setSecret($param) { $this->secret = $param;}
		public function getLiveKeyPath(){ return $this->liveKeyPath;}
		public function setLiveKeyPath($param) { $this->liveKeyPath = $param;}
		public function setEnvironment($param){ $this->environment = $param;}
		public function getEnvironment(){ return $this->environment;}
		
		public function setSignature($param) { $this->signature = $param;}
		public function getSignature(){ return $this->signature;}
		public function setPublicKey($param) {
			$param = str_replace('-----BEGIN PUBLIC KEY-----', '', $param);
			$param = trim(str_replace('-----END PUBLIC KEY-----', '', $param));
			$param = str_replace("\n", '', $param);
			$this->publicKey = $param;
		}
		public function getPublicKey(){ return $this->publicKey;}
		
		public function getCookie(){return $this->cookie;}
		public function setCookie($param) {$this->cookie = $param;}
		public function getXSessionId(){return $this->x_session_id;}
		public function setXSessionId($param) {$this->x_session_id = $param;}
		public function getXApp(){return $this->x_app;}
		public function setXApp($param) {$this->x_app = $param;return $this;}
		public function getXUserId(){return $this->x_user_id;}
		public function setXUserId($param) {$this->x_user_id = $param;return $this;}
		public function getXAccessToken(){return $this->x_access_token;}
		public function setXAccessToken($param){$this->x_access_token = $param;}
		public function getXPrivateId(){return $this->x_private_id;}
		public function setXPrivateId($param){$this->x_private_id = $param;}

		public function setAuthenURL($param) { $this->authenURL = $param;}
		public function getAuthenURL(){ return $this->authenURL;}
		public function setApiURL($param) { $this->apiURL = $param;}
		public function getApiURL(){ return $this->apiURL;}
	}
}