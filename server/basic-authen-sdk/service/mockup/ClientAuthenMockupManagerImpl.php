<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 24/08/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ClientAuthenMockupManagerImpl.php
 *
 */
namespace _server_sdk\service\mockup{
	include_once __DIR__.'/../ClientAuthenManager.php';
	include_once __DIR__.'/../../utils/ClientAuthenUtility.php';
	include_once __DIR__.'/../../builder/ClientAuthenBuilder.php';
	include_once __DIR__.'/../../exception/ServerAuthenException.php';
	include_once __DIR__.'/../../validation/ClientAuthenValidation.php';
	include_once __DIR__.'/../../model/AppAuthenResponse.php';

	use _server_sdk\utils\ClientAuthenUtility;
	use _server_sdk\builder\ClientAuthenBuilder;
	use _server_sdk\service\ClientAuthenManager;
	use _server_sdk\exception\ServerAuthenException;
	use _server_sdk\validation\ClientAuthenValidation;
	use _server_sdk\model\AppAuthenResponse;
	class ClientAuthenMockupManagerImpl implements ClientAuthenManager{
		private $clientAuthenParameters;
		private $apiResponse;
		private $apiResponseBody;
		public function appAuthen($paramResult){
			
			
		
			try{
				//Check config Email.
				ClientAuthenValidation::checkConfigB2C(ClientAuthenBuilder::$serviceConfig->getEmail(),ClientAuthenBuilder::$serviceConfig->getSecret());
				//Get header.
				$requestHeader = ClientAuthenUtility::checkRestHeader();
				//Get boby from partner set.
				$requestBody = ClientAuthenUtility::getBodyParameter($paramResult["requestParameter"]);
				//Check Header
				$requestHeaderVal = (object)[];
				$requestHeaderVal = json_decode(json_encode($requestHeader));
				$fieldValidateDataHeader = array('Cookie','x-session-id','x-app','x-user-id');
				ClientAuthenUtility::validationRequestBody($requestHeaderVal,$fieldValidateDataHeader);
				//Check Body
				$fieldValidateData = array('clientId','authCode','platform','redirectURL','ptsAppEnvironmentType');
				ClientAuthenUtility::validationRequestBody($requestBody,$fieldValidateData);
				
				//Check mandatory parameter EnvironmentType.
				ClientAuthenValidation::checkEnvironmentB2C($requestBody->{'ptsAppEnvironmentType'});
				
				
				$mockupAuthList = '{
				"developerMessage":"Success (Mockup)",
				"userMessage" : "Success (Mockup)",
				"resultCode":"20000",
				"moreInfo":"http://smaf.apigw.ais/errors/20000",
				"accessToken": "eifu11EKDFGT1313",
				"expireIn":"600",
				"idType":"E.164",
				"idValue": 	"66811234567",
                "gupAuthenLevel": 	"password",
                "gupRegistrationLevel": 	"password",
				"ptsListOfAPI": [
					{
						"loanPotential|C":"password",
						"R":"no",
					 	"U":"password",
					 	"D":"0","L":"token|no"
					},
					{
						"Push Notification|C":"password",
					 	"R":"no",
					 	"U":"password",
					 	"D":"no",
					 	"L":"token|yes"
					}
				],
				"openIdFlag":"true",
				"ptsAppId":	"400200020001000",
				"ptsAppEnvironmentType": "production"
				}';
				
				$jsonBodyMockup = json_decode($mockupAuthList);
				//header
				header('x-session-id: ojgojfo;djsajfdjs');
				header('x-user-id: Y9sa2@nvlsowhkd');
				header('x-private-id: asf1234sdff1d2g3g');
				header('x-access-token: TXjsooq##@i0df');
				header('x-app: CWT1;101;20001;Cookierun|Android|1.0.0');
				//Set-Cookie
				setcookie("site","CWT1");
				setcookie("node","101");
				
				$this->apiResponseBody = new AppAuthenResponse($jsonBodyMockup);
				
// 				echo json_encode($this->apiResponseBody);
				return  $this->apiResponseBody;
				
			}catch (ServerAuthenException $e){
// 				$logResponseHeaders = apache_request_headers();
// 				foreach($logResponseHeaders as $key => $val){
// 						header($key.': '.$val);
// 				}
				//Response error form pantry to partner.
				$errorResponse = (object)[];
				$errorResponse->resultCode = $e->getErrorCode();
				$errorResponse->developerMessage = $e->getDeveloperMessage();
				$errorResponse->userMessage = $e->getUserMessage();
				$errorResponse->moreInfo = $e->getMoreInfo();
				$this->errorResponse = new AppAuthenResponse($errorResponse);
				
				
				return $this->errorResponse;
// 				echo json_encode($this->errorResponse);
			}
		}
		
	}
}
?>