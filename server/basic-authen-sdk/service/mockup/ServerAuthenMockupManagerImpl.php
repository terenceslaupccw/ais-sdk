<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 24/08/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ServerAuthenMockupManagerImpl.php
 *
 */

namespace _server_sdk\service\mockup{
	include_once __DIR__.'/../ServerAuthenManager.php';
	include_once __DIR__.'/../../utils/ServerAuthenUtility.php';
	include_once __DIR__.'/../../builder/ServerAuthenBuilder.php';
	include_once __DIR__.'/../../exception/ServerAuthenException.php';
	include_once __DIR__.'/../../validation/ServiceAuthenValidation.php';
	
	include_once __DIR__.'/../../model/LoginResponse.php';
	include_once __DIR__.'/../../model/KeepAliveResponse.php';
	include_once __DIR__.'/../../model/LogoutResponse.php';
	
	use _server_sdk\utils\ServerAuthenUtility;
	use _server_sdk\service\ServerAuthenManager;
	use _server_sdk\builder\ServerAuthenBuilder;
	use _server_sdk\exception\ServerAuthenException;
	use _server_sdk\validation\ServiceAuthenValidation;
	
	use _server_sdk\model\LoginResponse;
	use _server_sdk\model\KeepAliveResponse;
	use _server_sdk\model\LogoutResponse;
	class ServerAuthenMockupManagerImpl implements ServerAuthenManager{
		private $serverAuthenParameters;
		private $apiResponse = array();
		private $loginResponse;
		private $keepAliveResponse;
		private $logoutResponse;
		private $configJson;
		public function login(){
			
			try{
// 				ServiceAuthenValidation::checkConfigB2B(ServerAuthenBuilder::$serviceConfig->getClientId(),ServerAuthenBuilder::$serviceConfig->getEmail(),ServerAuthenBuilder::$serviceConfig->getSecret(),'y');
			    $fieldlist = array('email','clientId','secret','environment');
			    ServiceAuthenValidation::validationConfigB2B(ServerAuthenBuilder::$serviceConfig,$fieldlist);
				
// 				ServiceAuthenValidation::checkValidateEnvironment(ServerAuthenBuilder::$serviceConfig->getEnvironment());

				//check Validations Header
				
				//check Validations Body
				
				$this->serverAuthenParameters = (object)[];
				$accessToken = base64_encode("kDHNvwqJoht60LruyNVSowRYOGs=");
				$this->serverAuthenParameters->{'developerMessage'}	= "Success (Mockup)";
				$this->serverAuthenParameters->{'userMessage'}	=	"Success (Mockup)";
				$this->serverAuthenParameters->{'resultCode'}   =	"20000";
				$this->serverAuthenParameters->{'moreInfo'}		=	"";
				$this->serverAuthenParameters->{'accessToken'}	=	$accessToken;
				$this->serverAuthenParameters->{'expireIn'}		=	"30";
				
				return $this->serverAuthenParameters;
					
			}catch (ServerAuthenException $e){
				$this->errorResponse = (object)[];
				$this->errorResponse->resultCode = $e->getErrorCode();
				$this->errorResponse->developerMessage = $e->getDeveloperMessage();
				$this->errorResponse->userMessage = $e->getUserMessage();
				$this->errorResponse->moreInfo = $e->getMoreInfo();
				//Response error form pantry to partner.
				$this->loginResponse = new LoginResponse($this->errorResponse);
				return $this->loginResponse;
			}
		}
		
		public function keepAlive($paramResult){
			try {
				//Send parameter cliendId, Email and Secret to function checkConfigB2B for check validation.
				//ServiceAuthenValidation::checkConfigB2B(ServerAuthenBuilder::$serviceConfig->getClientId(),ServerAuthenBuilder::$serviceConfig->getEmail());
				
				//Send parameter Environment check validation.
				//ServiceAuthenValidation::checkValidateEnvironment(ServerAuthenBuilder::$serviceConfig->getEnvironment());
				
			    $fieldlist = array('email','clientId','secret','environment');
			    ServiceAuthenValidation::validationConfigB2B(ServerAuthenBuilder::$serviceConfig,$fieldlist);
			    
				//Validate accessToken.
				if(!property_exists($paramResult,'accessToken') || empty($paramResult->{'accessToken'})){
				    throw new ServerAuthenException('422', '90003',"[Hummus] {'accessToken'} Missing");
				}
				
				//check Validations Header
				
				//check Validations Body
				
				$this->serverAuthenParameters = (object)[];
				$accessToken = base64_encode("kDHNvwqJoht60LruyNVSowRYOGs=");
				$this->serverAuthenParameters->{'developerMessage'}	= "Success (Mockup)";
				$this->serverAuthenParameters->{'userMessage'}	=	"Success (Mockup)";
				$this->serverAuthenParameters->{'resultCode'}   =	"20000";
				$this->serverAuthenParameters->{'moreInfo'}		=	"";
				$this->serverAuthenParameters->{'accessToken'}	=	$accessToken;
				$this->serverAuthenParameters->{'expireIn'}		=	"30";
				
				return $this->serverAuthenParameters;
			} catch (ServerAuthenException $e) {
				//Response error form pantry to partner.
				$this->errorResponse = (object)[];
				$this->errorResponse->resultCode = $e->getErrorCode();
				$this->errorResponse->developerMessage = $e->getDeveloperMessage();
				$this->errorResponse->userMessage = $e->getUserMessage();
				$this->errorResponse->moreInfo = $e->getMoreInfo();
				//Response error form pantry to partner.
				$this->keepAliveResponse = new KeepAliveResponse($this->errorResponse);
				
				return $this->keepAliveResponse;
			}
		}
		
		public function logout($paramResult){
			try {
				//Send parameter cliendId, Email and Secret to function checkConfigB2B for check validation.
				//ServiceAuthenValidation::checkConfigB2B(ServerAuthenBuilder::$serviceConfig->getClientId(),ServerAuthenBuilder::$serviceConfig->getEmail());
				
				//Send parameter Environment check validation.
				//ServiceAuthenValidation::checkValidateEnvironment(ServerAuthenBuilder::$serviceConfig->getEnvironment());

			    $fieldlist = array('email','clientId','secret','environment');
			    ServiceAuthenValidation::validationConfigB2B(ServerAuthenBuilder::$serviceConfig,$fieldlist);
			    
				//if(!property_exists($paramResult,'accessToken') || isset($paramResult->{'accessToken'})){
				//	throw new ServerAuthenException('422', '90003','');
				//}
				//Validate accessToken.
				if(!property_exists($paramResult,'accessToken') || empty($paramResult->{'accessToken'})){
				    throw new ServerAuthenException('422', '90003',"[Hummus] {'accessToken'} Missing");
				}
				
				//check Validations Header
				
				//check Validations Body
				
				$logoutResponse = array(
				        'developerMessage'	=>	"Success (Mockup)",
				        'userMessage'		=>	"Success (Mockup)",
						'resultCode'		=>	"20000",
						'moreInfo'			=>	""
				);
			    $logoutResponseLog = null;
				$logoutResponseLog = (object)[];
				$logoutResponseLog->{'developerMessage'}	= "Success (Mockup)";
				$logoutResponseLog->{'userMessage'}	=	"Success (Mockup)";
				$logoutResponseLog->{'resultCode'}   =	"20000";
				$logoutResponseLog->{'moreInfo'}		=	"";
				
				
				
				return $logoutResponse;
			} catch (ServerAuthenException $e) {
				//Response error form pantry to partner.
				$this->errorResponse = (object)[];
				$this->errorResponse->resultCode = $e->getErrorCode();
				$this->errorResponse->developerMessage = $e->getDeveloperMessage();
				$this->errorResponse->userMessage = $e->getUserMessage();
				$this->errorResponse->moreInfo = $e->getMoreInfo();
				//Response error form pantry to partner.
				$this->logoutResponse = new LogoutResponse($this->errorResponse);
				return $this->logoutResponse;
			}
		}
	}
}
?>