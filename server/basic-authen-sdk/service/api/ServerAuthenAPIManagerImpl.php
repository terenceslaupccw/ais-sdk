<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 2.0.0
 * @author : Watcharachai T.
 * @date : 25/08/2016
 * @modify by : Watcharachai T.
 * @modify date : 05/10/2017
 * @link : https://devportal.ais.co.th/
 * @filename : ServerAuthenAPIManagerImpl.php
 *
 */
namespace _server_sdk\service\api{
    set_time_limit(0);
    include_once __DIR__.'/../../connection/ServiceAuthenAPIConnection.php';
    include_once __DIR__.'/../../utils/ServerAuthenUtility.php';
    include_once __DIR__.'/../../builder/ServerAuthenBuilder.php';
    include_once __DIR__.'/../../exception/ServerAuthenException.php';
    include_once __DIR__.'/../../common/ServiceAuthenAPIRequestParameters.php';
    include_once __DIR__.'/ServerAuthenAPIManager.php';
    
    include_once __DIR__.'/../param/ServerAuthenParameters.php';
    include_once __DIR__.'/../../security/ServiceAuthenSecurity.php';
    include_once __DIR__.'/../../common/config.php';
    
    include_once __DIR__.'/../../validation/ServiceAuthenValidation.php';
    
    use _server_sdk\connection\ServiceAuthenAPIConnection;
    use _server_sdk\utils\ServerAuthenUtility;
    use _server_sdk\builder\ServerAuthenBuilder;
    use _server_sdk\exception\ServerAuthenException;
    use _server_sdk\service\api\ServerAuthenAPIManager;
    use _server_sdk\common\ServiceAuthenAPIRequestParameters;
    
    use _server_sdk\service\param\ServerAuthenParameters;
    use _server_sdk\security\ServiceAuthenSecurity;
    use _server_sdk\common\Configuration;
    
    use _server_sdk\validation\ServiceAuthenValidation;
    
    class ServerAuthenAPIManagerImpl implements ServerAuthenAPIManager{
        private $bodyResponse;
        private $headerResponse;
        private $authenURL;
        private $apiURL;
        private $flag;
        private $count;
        private $serverAuthenParameters;
        private $apiResponseBody;
        //Method loginByB2B will send object data to fuction curl for request to pantry.
        public function loginByB2B($serverAuthenParameters){
//             $array = array(
//                 "foo" => "bar",
//                 "bar" => "foo", );
//             $isTouch = isset($array['xx']);
//             if(!$isTouch){
//                 print_r('xx');
//             }
            $this->bodyResponse;
            $this->headerResponse;
            $this->authenURL = "";
            $this->apiURL = "";
            $this->flag = false;
            $this->count = 0;
            $this->apiResponseBody;
            $apiResponse = (object)[];
            include __DIR__.'/../../basic-authen-config.php';
            
            //Get URL
            $configuration = new Configuration(ServerAuthenBuilder::$serviceConfig->getEnvironment(),'login_url','B2B');
            $iniConfig = $configuration->initConfiguration();
            
            //Body request for pantry.
            $requestBody = array(
                'clientId' 	=>	ServerAuthenBuilder::$serviceConfig->getClientId(),
                'timeStamp'	=>	$serverAuthenParameters->timeStamp,
                'serverId' 	=>	$serverAuthenParameters->serverId,
                'signature' =>	$serverAuthenParameters->signature
            );
            
            $initBody = json_encode($requestBody);
            $initTimeout = 10000;
            $initHeader = array(
                'Content-Type: application/json; charset=utf-8',
                'Accept: application/json',
                'x-sdk-category: Hummus|PHP',
                'x-sdk-name: B2B Basic Authen|2.0.1'
            );
            
            $serviceAuthenAPIRequestParameters = new ServiceAuthenAPIRequestParameters();
            // 				$serviceAuthenAPIRequestParameters->setInitConfig('https://iot-sacf.ais.co.th:14400'.$iniConfig['path']);
            $serviceAuthenAPIRequestParameters->setInitConfig($iniConfig['host'].$iniConfig['path']);
            $serviceAuthenAPIRequestParameters->setInitPath($iniConfig['path']);
            $serviceAuthenAPIRequestParameters->setInitMethod('POST');
            $serviceAuthenAPIRequestParameters->setInitTimeout($initTimeout);
            $serviceAuthenAPIRequestParameters->setInitHeader($initHeader);
            $serviceAuthenAPIRequestParameters->setInitBody($initBody);
            $headerReq = json_encode($initHeader);
            
            //////////////////////////// Curl PHP /////////////////////////////////////
            $hummusCurl = new ServiceAuthenAPIConnection();
            while($this->flag === false && $this->count <= $retry){
                $responseCurl = $hummusCurl->curl(
                    $serviceAuthenAPIRequestParameters->getInitBody(),
                    $serviceAuthenAPIRequestParameters->getInitConfig(),
                    $serviceAuthenAPIRequestParameters->getInitMethod(),
                    $serviceAuthenAPIRequestParameters->getInitHeader(),
                    $serviceAuthenAPIRequestParameters->getInitTimeout()
                    );
                
                
                
                
                // Connection Error 90007
                if($responseCurl['errno'] == '52' || $responseCurl['errno'] == '56' ||$responseCurl['errno'] == '7'|| $responseCurl['errno'] == '6'){ throw new ServerAuthenException('422', '90007','[Hummus] Can\'t Connect Backend');}
                if($responseCurl['errno'] == '28'){throw new ServerAuthenException('422', '90008','[Hummus] Timeout SDK { 30 Second }');}
                
                $bodyLog = $responseCurl['body'];
                $isSetHTTP = isset($bodyLog);
                if(!$isSetHTTP){
                    $bodyLog = null;
                }
                // 				$responseCurl['body'] = '()';
                // 				print_r($responseCurl['body']);
                $body = ServiceAuthenValidation::validateJson($responseCurl['body']);
                $isSet = isset($body['resultCode']);
                if(!$isSet){
                    $body['resultCode'] = null;
                }
                if($body['resultCode'] == "20000"){
                    // 				    $this->flag = true;
                    break;
                }else{
                    if($body['resultCode'] >= "40302" && $body['resultCode'] <= "40304"){
                        $isSetcorrectAuthen = isset($body['correctAuthenURL']);
                        $isSetcorrectAPI = isset($body['correctAPIURL']);
                        if(!$isSetcorrectAuthen){
                            $body['correctAuthenURL'] = null;
                        }
                        if(!$isSetcorrectAPI){
                            $body['correctAPIURL'] = null;
                        }
                        if($body['correctAuthenURL'] == "" || $body['correctAPIURL'] == ""){
                            // 				            $this->flag = true;
                            break;
                        }else{
                            $this->authenURL = $body['correctAuthenURL'];
                            $this->apiURL = $body['correctAPIURL'];
                            $this->count += 1;
                            $serviceAuthenAPIRequestParameters->setInitConfig($this->authenURL.$serviceAuthenAPIRequestParameters->getInitPath());
                            sleep($delay);
                        }
                    }else{
                        $this->count += 1;
                        sleep($delay);
                    }
                    
                }
            }//loop while
            
            $responseCurl["authenURL"] = $this->authenURL;
            $responseCurl["apiURL"] = $this->apiURL;
            if($responseCurl['http_code'] == '200' || $responseCurl['http_code'] == '201'){
                //header
                $this->headerResponse = ServerAuthenUtility::createHeaderB2B($responseCurl['header'], $responseCurl["authenURL"], $responseCurl["apiURL"]);
                $this->bodyResponse = json_decode($responseCurl['body']);
                $isSetCheckResult = isset($this->bodyResponse->{'resultCode'});
                if(!$isSetCheckResult){
                    $this->bodyResponse->{'resultCode'} = null;
                }
                if($this->bodyResponse->{'resultCode'} == "20000"){
                    //check Validations Header
                    $fieldlist = array('x-session-id','x-access-token');
                    ServiceAuthenValidation::validationResponseHeader($this->headerResponse,$fieldlist);
                    
                    //check Validations Body
                    $fieldlist = array('developerMessage','resultCode','accessToken','expireIn','ptsAppEnvironmentType','ptsListOfAPI');
                    ServiceAuthenValidation::validationResponseBody($this->bodyResponse,$fieldlist);
                    
                    $serverAuthenException = new ServerAuthenException('200', '20000',"");
                    $isSetUserMessage = isset( $this->bodyResponse->{'userMessage'});
                    if(!$isSetUserMessage){
                        $this->bodyResponse->{'userMessage'} = "";
                    }
                    
                   
                    
                    $apiResponse->{'resultCode'} = $serverAuthenException->getErrorCode();
                    $apiResponse->{'developerMessage'} = $this->bodyResponse->{'developerMessage'};
                    $apiResponse->{'userMessage'} = $this->bodyResponse->{'userMessage'};
                    $apiResponse->{'moreInfo'} = (!property_exists((object) $this->bodyResponse,'moreInfo'))?'':$this->bodyResponse->{'moreInfo'};
                    //Eccrype accessToken
                    $apiResponse->{'accessToken'} = ServiceAuthenSecurity::encAccesstoken(ServerAuthenBuilder::$serviceConfig->getClientId(),$this->headerResponse);
                    $apiResponse->{'expireIn'} = $this->bodyResponse->{'expireIn'};
                }else{
                    //check Validations Header
                    
                    $fieldlist = array('developerMessage','resultCode');
                    ServiceAuthenValidation::validationResponseBody($this->bodyResponse,$fieldlist);
                    
                    $apiResponse->{'resultCode'} = $this->bodyResponse->{'resultCode'};
                    $apiResponse->{'developerMessage'} = $this->bodyResponse->{'developerMessage'};
                    $apiResponse->{'userMessage'} = $this->bodyResponse->{'developerMessage'};
                    $apiResponse->{'moreInfo'} = (!property_exists((object) $this->bodyResponse,'moreInfo'))?'':$this->bodyResponse->{'moreInfo'};
                }
                
                return $apiResponse;
            }else{
                $this->headerResponse = ServerAuthenUtility::createHeaderB2B($responseCurl['header'], $responseCurl["authenURL"], $responseCurl["apiURL"]);
                $this->bodyResponse = json_decode($responseCurl['body']);
                //check Validations Header
                
                
                $fieldlist = array('developerMessage','resultCode');
                ServiceAuthenValidation::validationResponseBodyHTTPError($this->bodyResponse,$fieldlist);
                
                $this->bodyResponse = json_decode($responseCurl['body']);
                $apiResponse->{'resultCode'} = $this->bodyResponse->{'resultCode'};
                $apiResponse->{'developerMessage'} = $this->bodyResponse->{'developerMessage'}.' and HTTP status code was not 200 and 201';
                $apiResponse->{'userMessage'} = $this->bodyResponse->{'developerMessage'};
                $apiResponse->{'moreInfo'} = (!property_exists((object) $this->bodyResponse,'moreInfo'))?'':$this->bodyResponse->{'moreInfo'};
                return $apiResponse;
            }
        }
        
        //Method keepAlive will send object data to fuction curl for request to pantry.
        public function keepAlive($serverAuthenParameters){
            $this->bodyResponse;
            $this->headerResponse;
            $this->authenURL = "";
            $this->apiURL = "";
            $this->flag = false;
            $this->count = 0;
            $this->apiResponseBody;
            $apiResponse = (object)[];
            include __DIR__.'/../../basic-authen-config.php';
            
            //Check value of accessToken and decode AccessToken for get header.
            $header = ServiceAuthenSecurity::decAccesstoken(ServerAuthenBuilder::$serviceConfig->getClientId(),$serverAuthenParameters->accessToken);
            
            //Get URL
            $authenURL = "";
            $apiURL = "";
            $configuration = new Configuration(ServerAuthenBuilder::$serviceConfig->getEnvironment(),'keepalive_url','B2B');
            $iniConfig = $configuration->initConfiguration();
            
            if($header['authenURL'] != null && $header['apiURL'] != null){
                $iniConfig["host"] = $header['authenURL'];
                $this->authenURL = $header['authenURL'];
                $this->apiURL = $header['apiURL'];
            }
            
            //Body request for pantry.
            $requestBody = array(
                'clientId'		=>	ServerAuthenBuilder::$serviceConfig->getClientId(),
                'accessToken'	=>	$header['x-access-token']
            );
            $initBody = json_encode($requestBody);
            $initTimeout = 10000;
            $initHeader = array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Cookie: '.ServerAuthenUtility::removeLN($header['Cookie']),
                'x-session-id: '.ServerAuthenUtility::removeLN($header['x-session-id']),
                'x-app: '.ServerAuthenUtility::removeLN($header['x-app']),
                'x-access-token: '.ServerAuthenUtility::removeLN($header['x-access-token']),
                'x-user-id: '.ServerAuthenUtility::removeLN($header['x-user-id']),
                'x-sdk-category: Hummus|PHP',
                'x-sdk-name: B2B Basic Authen|2.0.1'
                
            );
            //Set value config, method POST, timeOut, header and body for pantry.
            $serviceAuthenAPIRequestParameters = new ServiceAuthenAPIRequestParameters();
            
            $serviceAuthenAPIRequestParameters->setInitConfig($iniConfig['host'].$iniConfig['path']);
            $serviceAuthenAPIRequestParameters->setInitPath($iniConfig['path']);
            $serviceAuthenAPIRequestParameters->setInitAuthenURL($authenURL);
            $serviceAuthenAPIRequestParameters->setInitApiURL($apiURL);
            $serviceAuthenAPIRequestParameters->setInitMethod('POST');
            $serviceAuthenAPIRequestParameters->setInitTimeout($initTimeout);
            $serviceAuthenAPIRequestParameters->setInitHeader($initHeader);
            $serviceAuthenAPIRequestParameters->setInitBody($initBody);
            
            
            $hummusCurl = new ServiceAuthenAPIConnection();
            while($this->flag === false && $this->count <= $retry){
                $responseCurl = $hummusCurl->curl(
                    $serviceAuthenAPIRequestParameters->getInitBody(),
                    $serviceAuthenAPIRequestParameters->getInitConfig(),
                    $serviceAuthenAPIRequestParameters->getInitMethod(),
                    $serviceAuthenAPIRequestParameters->getInitHeader(),
                    $serviceAuthenAPIRequestParameters->getInitTimeout()
                    );
                
                
                $bodyLog = $responseCurl['body'];
                $isSetHTTP = isset($bodyLog);
                if(!$isSetHTTP){
                    $bodyLog = null;
                }
                
                $body = ServiceAuthenValidation::validateJson($responseCurl['body']);
                $isSet = isset($body['resultCode']);
                if(!$isSet){
                    $body['resultCode'] = null;
                }
                if($body['resultCode'] == "20000"){
                    // 					$this->flag = true;
                    break;
                }else{
                    if($body['resultCode'] >= "40302" && $body['resultCode'] <= "40304"){
                        $isSetcorrectAuthen = isset($body['correctAuthenURL']);
                        $isSetcorrectAPI = isset($body['correctAPIURL']);
                        if(!$isSetcorrectAuthen){
                            $body['correctAuthenURL'] = null;
                        }
                        if(!$isSetcorrectAPI){
                            $body['correctAPIURL'] = null;
                        }
                        if($body['correctAuthenURL'] == "" || $body['correctAPIURL'] == ""){
                            // 					      $this->flag = true;
                            break;
                        }else{
                            $this->authenURL = $body['correctAuthenURL'];
                            $this->apiURL = $body['correctAPIURL'];
                            $this->count += 1;
                            $serviceAuthenAPIRequestParameters->setInitConfig($this->authenURL.$serviceAuthenAPIRequestParameters->getInitPath());
                            sleep($delay);
                        }
                    }else{
                        // 						if(($serviceAuthenAPIRequestParameters->getInitAuthenURL() != "" && $body['correctAuthenURL'] == "") && ($serviceAuthenAPIRequestParameters->getInitApiURL() != "" && $body['correctAPIURL'] == "")){
                        // 							$this->authenURL = $serviceAuthenAPIRequestParameters->getInitAuthenURL();
                        // 							$this->apiURL = $serviceAuthenAPIRequestParameters->getInitApiURL();
                        // 						}
                        $this->count += 1;
                        sleep($delay);
                    }
                    
                }
            }
            
            $responseCurl["authenURL"] = $this->authenURL;
            $responseCurl["apiURL"] = $this->apiURL;
            if($responseCurl['http_code'] == '200' || $responseCurl['http_code'] == '201'){
                //header
                $this->headerResponse = ServerAuthenUtility::createHeaderB2B($responseCurl['header'], $responseCurl["authenURL"], $responseCurl["apiURL"]);
                //body
                $this->bodyResponse = json_decode($responseCurl['body']);
                // 				//add parameter url in header
                // 				$this->headerResponse['authenURL'] = $responseCurl['authenURL'];
                // 				$this->headerResponse['apiURL'] = $responseCurl['apiURL'];
                $isSetCheckResult = isset($this->bodyResponse->{'resultCode'});
                if(!$isSetCheckResult){
                    $this->bodyResponse->{'resultCode'} = null;
                }
                if($this->bodyResponse->{'resultCode'} == "20000"){
                    //check Validations Header
                    $fieldlist = array('x-session-id','x-access-token');
                    ServiceAuthenValidation::validationResponseHeader($this->headerResponse,$fieldlist);
                    
                    //check Validations Body
                    $fieldlist = array('developerMessage','resultCode','accessToken','expireIn','ptsListOfAPI');
                    // 						$fieldlistArray = array('ptsListOfAPI');
                    ServiceAuthenValidation::validationResponseBody($this->bodyResponse,$fieldlist);
                    
                    $serverAuthenException = new ServerAuthenException('200', '20000',"");
                    
                    $isSetUserMessage = isset( $this->bodyResponse->{'userMessage'});
                    if(!$isSetUserMessage){
                        $this->bodyResponse->{'userMessage'} = "";
                    }
                   
                    $apiResponse->{'resultCode'} = $serverAuthenException->getErrorCode();
                    $apiResponse->{'developerMessage'} = $this->bodyResponse->{'developerMessage'};
                    $apiResponse->{'userMessage'} = $this->bodyResponse->{'userMessage'};
                    $apiResponse->{'moreInfo'} = (!property_exists((object) $this->bodyResponse,'moreInfo'))?'':$this->bodyResponse->{'moreInfo'};
                    $apiResponse->{'accessToken'} = ServiceAuthenSecurity::encAccesstoken(ServerAuthenBuilder::$serviceConfig->getClientId(),$this->headerResponse);
                    $apiResponse->{'expireIn'} = $this->bodyResponse->{'expireIn'};
                }else{
                    //check Validations Header
                    
                    $fieldlist = array('developerMessage','resultCode');
                    ServiceAuthenValidation::validationResponseBody($this->bodyResponse,$fieldlist);
                    
                    $apiResponse->{'resultCode'} = $this->bodyResponse->{'resultCode'};
                    $apiResponse->{'developerMessage'} = $this->bodyResponse->{'developerMessage'};
                    $apiResponse->{'userMessage'} = $this->bodyResponse->{'developerMessage'};
                    $apiResponse->{'moreInfo'} = (!property_exists((object) $this->bodyResponse,'moreInfo'))?'':$this->bodyResponse->{'moreInfo'};
                }
                
                return $apiResponse;
            }else{
                //header
                $this->headerResponse = ServerAuthenUtility::createHeaderB2B($responseCurl['header'], $responseCurl["authenURL"], $responseCurl["apiURL"]);
                //body
                $this->bodyResponse = json_decode($responseCurl['body']);
                //check Validations Header
                
                $fieldlist = array('developerMessage','resultCode');
                ServiceAuthenValidation::validationResponseBodyHTTPError($this->bodyResponse,$fieldlist);
                
                $apiResponse->{'resultCode'} = $this->bodyResponse->{'resultCode'};
                $apiResponse->{'developerMessage'} = $this->bodyResponse->{'developerMessage'}.' and HTTP status code was not 200 and 201';
                $apiResponse->{'userMessage'} = $this->bodyResponse->{'developerMessage'};
                $apiResponse->{'moreInfo'} = (!property_exists((object) $this->bodyResponse,'moreInfo'))?'':$this->bodyResponse->{'moreInfo'};
                return $apiResponse;
            }
        }
        
        //Method logout will send object data to fuction curl for request to pantry.
        public function logout($serverAuthenParameters){
            $this->bodyResponse;
            $this->headerResponse;
            $this->authenURL = "";
            $this->apiURL = "";
            $this->apiResponseBody;
            $apiResponse = (object)[];
            
            $header = ServiceAuthenSecurity::decAccesstoken(ServerAuthenBuilder::$serviceConfig->getClientId(),$serverAuthenParameters->accessToken);
            
            //Get URL
            $authenURL = "";
            $apiURL = "";
            $configuration = new Configuration(ServerAuthenBuilder::$serviceConfig->getEnvironment(),'logout_url','B2B');
            $iniConfig = $configuration->initConfiguration();
            
            if($header['authenURL'] != null && $header['apiURL'] != null){
                if($header["authenURL"] != ""){
                    $iniConfig["host"] = $header['authenURL'];
                    $authenURL = $header['authenURL'];
                    $apiURL = $header['apiURL'];
                }
            }
            
            
            //Body request for pantry.
            $requestBody = array(
                'clientId'		=>	ServerAuthenBuilder::$serviceConfig->getClientId(),
                'accessToken'	=>	$header['x-access-token']
            );
            $initBody = json_encode($requestBody);
            $initTimeout = 10000;
            //Header request for pantry.
            $initHeader = array(
                'Content-Type: application/json',
                'Accept: application/json',
                // 					'Cookie: '.ServerAuthenUtility::removeLN($header['Cookie']),
                'x-session-id: '.ServerAuthenUtility::removeLN($header['x-session-id']),
                // 					'x-app: '.ServerAuthenUtility::removeLN($header['x-app']),
                // 					'x-access-token: '.ServerAuthenUtility::removeLN($header['x-access-token']),
                // 			        'x-user-id: '.ServerAuthenUtility::removeLN($header['x-user-id']),
                'x-sdk-category: Hummus|PHP',
                'x-sdk-name: B2B Basic Authen|2.0.1'
            );
            //Set value config, method POST, timeOut, header and body for pantry.
            $serviceAuthenAPIRequestParameters = new ServiceAuthenAPIRequestParameters();
            
            $serviceAuthenAPIRequestParameters->setInitConfig($iniConfig['host'].$iniConfig['path']);
            $serviceAuthenAPIRequestParameters->setInitPath($iniConfig['path']);
            $serviceAuthenAPIRequestParameters->setInitAuthenURL($authenURL);
            $serviceAuthenAPIRequestParameters->setInitApiURL($apiURL);
            $serviceAuthenAPIRequestParameters->setInitMethod('POST');
            $serviceAuthenAPIRequestParameters->setInitTimeout($initTimeout);
            $serviceAuthenAPIRequestParameters->setInitHeader($initHeader);
            $serviceAuthenAPIRequestParameters->setInitBody($initBody);
            
            
            $hummusCurl = new ServiceAuthenAPIConnection();
            $responseCurl = $hummusCurl->curl(
                $serviceAuthenAPIRequestParameters->getInitBody(),
                $serviceAuthenAPIRequestParameters->getInitConfig(),
                $serviceAuthenAPIRequestParameters->getInitMethod(),
                $serviceAuthenAPIRequestParameters->getInitHeader(),
                $serviceAuthenAPIRequestParameters->getInitTimeout()
                );
            // Connection Error 90007
            if($responseCurl['errno'] == '52' || $responseCurl['errno'] == '56' ||$responseCurl['errno'] == '7'|| $responseCurl['errno'] == '6'){throw new ServerAuthenException('422', '90007','[Hummus] Can\'t Connect Backend');}
            if($responseCurl['errno'] == '28'){throw new ServerAuthenException('422', '90008',"[Hummus] Timeout SDK { 10 Second }");}
//             $body = json_decode($responseCurl['body'],TRUE);
            $bodyLog = $responseCurl['body'];
            $isSetHTTP = isset($bodyLog);
            if(!$isSetHTTP){
                $bodyLog = null;
            }
            
            $body = ServiceAuthenValidation::validateJson($responseCurl['body']);
            if(json_last_error() != JSON_ERROR_NONE && $responseCurl['body'] == null){
                throw new ServerAuthenException('422', '90011','[Hummus] Unknown format');
            }
            
            $responseCurl["authenURL"] = $serviceAuthenAPIRequestParameters->getInitAuthenURL().$serviceAuthenAPIRequestParameters->getInitPath();
            $responseCurl["apiURL"] = $serviceAuthenAPIRequestParameters->getInitApiURL().$serviceAuthenAPIRequestParameters->getInitPath();
            
            if($responseCurl['http_code'] == '200' || $responseCurl['http_code'] == '201'){
                
                //header
                $this->headerResponse = ServerAuthenUtility::createHeaderB2B($responseCurl['header'], $responseCurl["authenURL"], $responseCurl["apiURL"]);
                //body
                $this->bodyResponse = json_decode($responseCurl['body']);
                $isSetCheckResult = isset($this->bodyResponse->{'resultCode'});
                if(!$isSetCheckResult){
                    $this->bodyResponse->{'resultCode'} = null;
                }
                //add parameter url in header
                $this->headerResponse['authenURL'] = $responseCurl['authenURL'];
                $this->headerResponse['apiURL'] = $responseCurl['apiURL'];
                
                $accessToken = "";
                // 				ServiceAuthenValidation::validateJson($responseCurl['body']);
                if(json_last_error() == JSON_ERROR_NONE && $responseCurl['body'] != null){
                    if($this->bodyResponse->{'resultCode'} == "20000"){
                        //check Validations Header
                        $fieldlist = array('x-session-id');
                        ServiceAuthenValidation::validationResponseHeader($this->headerResponse,$fieldlist);
                        
                        //check Validations Body
                        $fieldlist = array('developerMessage','resultCode');
                        ServiceAuthenValidation::validationResponseBody($this->bodyResponse,$fieldlist);
                        
                        $serverAuthenException = new ServerAuthenException('200', '20000',"");
                        
                        $isSetUserMessage = isset( $this->bodyResponse->{'userMessage'});
                        if(!$isSetUserMessage){
                            $this->bodyResponse->{'userMessage'} = "";
                        }
                        
                        
                        $apiResponse->{'resultCode'} = $serverAuthenException->getErrorCode();
                        $apiResponse->{'developerMessage'} = $this->bodyResponse->{'developerMessage'};
                        $apiResponse->{'userMessage'} = $this->bodyResponse->{'userMessage'};
                        $apiResponse->{'moreInfo'} = (!property_exists((object) $this->bodyResponse,'moreInfo'))?'':$this->bodyResponse->{'moreInfo'};
                        
                    }else{
                        //check Validations Header
                        
                        //check Validations Body
                        $fieldlist = array('developerMessage','resultCode');
                        ServiceAuthenValidation::validationResponseBody($this->bodyResponse,$fieldlist);
                        
                        $apiResponse->{'resultCode'} = $this->bodyResponse->{'resultCode'};
                        $apiResponse->{'developerMessage'} = $this->bodyResponse->{'developerMessage'};
                        $apiResponse->{'userMessage'} = $this->bodyResponse->{'developerMessage'};
                        $apiResponse->{'moreInfo'} = (!property_exists((object) $this->bodyResponse,'moreInfo'))?'':$this->bodyResponse->{'moreInfo'};
                    }
                    return $apiResponse;
                }else{
                    throw new ServerAuthenException('422', '90011','Response Not JSON format');
                }
            }else{
                
                //header
                $this->headerResponse = ServerAuthenUtility::createHeaderB2B($responseCurl['header'], $responseCurl["authenURL"], $responseCurl["apiURL"]);
                //body
                $this->bodyResponse = json_decode($responseCurl['body']);
                
                //check Validations Header
                // 				    $fieldlist = array('Cookie','x-session-id','x-app','x-user-id','x-access-token');
                // 				    ServiceAuthenValidation::validationResponseHeaderHTTPError($this->headerResponse,$fieldlist);
                
                //check Validations Body
                $fieldlist = array('developerMessage','resultCode');
                ServiceAuthenValidation::validationResponseBodyHTTPError($this->bodyResponse,$fieldlist);
                $apiResponse->{'resultCode'} = $this->bodyResponse->{'resultCode'};
                $apiResponse->{'developerMessage'} = $this->bodyResponse->{'developerMessage'}.' and HTTP status code was not 200 and 201';
                $apiResponse->{'userMessage'} = $this->bodyResponse->{'developerMessage'};
                $apiResponse->{'moreInfo'} = (!property_exists((object) $this->bodyResponse,'moreInfo'))?'':$this->bodyResponse->{'moreInfo'};
                return $apiResponse;
            }
        }//log out
    }
}
?>