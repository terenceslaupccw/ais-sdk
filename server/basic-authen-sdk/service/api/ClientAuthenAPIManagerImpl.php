<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ClientAuthenAPIManagerImpl.php
 *
 */
namespace _server_sdk\service\api{
	include_once __DIR__.'/../../connection/ServiceAuthenAPIConnection.php';
	include_once __DIR__.'/../../utils/ClientAuthenUtility.php';// Cut no use
	include_once __DIR__.'/../../builder/ClientAuthenBuilder.php';
	include_once __DIR__.'/../../exception/ServerAuthenException.php';
	include_once __DIR__.'/../../common/ServiceAuthenAPIRequestParameters.php';
	include_once __DIR__.'/ClientAuthenAPIManager.php';
	
	include_once __DIR__.'/../param/ClientAuthenParameters.php';
	include_once __DIR__.'/../../connection/ServiceAuthenAPIConnection.php';
	include_once __DIR__.'/../../common/config.php';
	
	include_once __DIR__.'/../../validation/ServiceAuthenValidation.php';
	
	use _server_sdk\connection\ServiceAuthenAPIConnection;
	use _server_sdk\utils\ClientAuthenUtility;///////////////////////////////
	use _server_sdk\builder\ClientAuthenBuilder;
	use _server_sdk\exception\ServerAuthenException;
	use _server_sdk\common\ServiceAuthenAPIRequestParameters;
	use _server_sdk\service\api\ClientAuthenAPIManager;
	
	use _server_sdk\service\param\ClientAuthenParameters;
	use _server_sdk\common\Configuration;
	
	use _server_sdk\validation\ServiceAuthenValidation;
	
	class ClientAuthenAPIManagerImpl implements ClientAuthenAPIManager{
	    private $apiResponse;
		private $apiBodyResponse;
		private $mockData;
		//Method obtainAuthorityList will send object data to fuction curl for request to pantry.
		//When recieve response from pantry method obtainAuthorityList will send response to method appAuthenProcess.
		public function obtainAuthorityList($clientAuthenParameters,$requestHeader){	
			$this->apiResponse = array();
			$this->apiBodyResponse;
			
			$environment = ClientAuthenUtility::pstEnvironment($clientAuthenParameters->{'ptsAppEnvironmentType'});
			
			//Get URL
			$configuration = new Configuration($environment,'authen_url','B2C');
			$iniConfig = $configuration->initConfiguration();
			//Body request for pantry.
			$requestBody = array(
					'authCode'		=>	$clientAuthenParameters->{'authCode'},
					'clientId'		=>	$clientAuthenParameters->{'clientId'},
					'grantType'		=>	$clientAuthenParameters->{'grantType'},
					'redirectURL'	=>	$clientAuthenParameters->{'redirectURL'},
					'signature'		=>	$clientAuthenParameters->{'signature'},
					'timeStamp'		=>	$clientAuthenParameters->{'timeStamp'},
					'commandId'		=>	$clientAuthenParameters->{'commandId'},
			);
			$initBody = json_encode($requestBody);
			$initBodyBase64 = base64_encode($initBody);
			$initTimeout = 20000;
			//Header request for pantry.
			$initHeader = array(
			    'Content-Type: application/json; charset=utf-8',
			    'Cookie: '.$requestHeader["Cookie"],
			    'x-session-id: '.$requestHeader["x-session-id"],
			    'x-app: '.$requestHeader["x-app"],
			    'x-user-id: '.$requestHeader["x-user-id"],
			    'x-requester: '."Hummus",
			    'x-access-token: '.$requestHeader["x-access-token"],
			    'x-private-id: '.$requestHeader["x-private-id"],
			    'x-sdk-category: '.'Hummus|php',
			    'x-sdk-name: '.'B2C Basic Authen|1.0.0'
			);
			
			
			
			//Set value config, method POST, timeOut, header and body for pantry.
			$serviceAuthenAPIRequestParameters = new ServiceAuthenAPIRequestParameters();
				
			$serviceAuthenAPIRequestParameters->setInitConfig($iniConfig['host'].$iniConfig['path']);
			$serviceAuthenAPIRequestParameters->setInitMethod('POST');
			$serviceAuthenAPIRequestParameters->setInitTimeout($initTimeout);
			$serviceAuthenAPIRequestParameters->setInitHeader($initHeader);
			$serviceAuthenAPIRequestParameters->setInitBody($initBodyBase64);
				
			foreach($initHeader as $key => $val){
			}
			
			$hummusCurl = new ServiceAuthenAPIConnection();
			$responseCurl = $hummusCurl->curl(
							$serviceAuthenAPIRequestParameters->getInitBody(), 
							$serviceAuthenAPIRequestParameters->getInitConfig(), 
							$serviceAuthenAPIRequestParameters->getInitMethod(), 
							$serviceAuthenAPIRequestParameters->getInitHeader(), 
							$serviceAuthenAPIRequestParameters->getInitTimeout()
						);
			
			
			
			
			//Check Connection Error
			if($responseCurl['errno']=='52'||$responseCurl['errno']=='56'||$responseCurl['errno']=='7' || $responseCurl['errno'] == '6'){throw new ServerAuthenException('422', '90007',"[Hummus] Can't Connect Backend");}
			if($responseCurl['errno']=='28'){throw new ServerAuthenException('422', '90008',"[Hummus] Timeout SDK { 20 Second }");}
			
			if($responseCurl['http_code']=='200'||$responseCurl['http_code']=='201'){
			    //header
			    $header = array();
			    preg_match_all('/(?<param>.+): (?<value>.+)/', $responseCurl['header'], $header);
			    $countHeader = count($header['param']);
			    for($i=1;$i<=$countHeader;$i++){
			        $this->apiResponse['header'][trim($header['param'][$i-1])] = ClientAuthenUtility::removeLN($header['value'][$i-1]);
			        // Set Header
			        if(strtolower($header['param'][$i-1]) == "x-app"){
			            header('x-app'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
			        }
			        elseif(strtolower($header['param'][$i-1]) == "x-session-id"){
			            header('x-session-id'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
			        }elseif(strtolower($header['param'][$i-1]) == "set-cookie"){
			            header('Set-Cookie'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
			        }elseif(strtolower($header['param'][$i-1]) == "x-user-id"){
			            header('x-user-id'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
			        }elseif(strtolower($header['param'][$i-1]) == "x-access-token"){
			            header('x-access-token'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
			        }elseif(strtolower($header['param'][$i-1]) == "x-private-id"){
			            header('x-private-id'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
			        }elseif(strtolower($header['param'][$i-1]) == "x-sdk-category"){
			            header('x-sdk-category'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
			        }elseif(strtolower($header['param'][$i-1]) == "x-sdk-name"){
			            header('x-sdk-name'.': '.ClientAuthenUtility::removeLN($header['value'][$i-1]));
			        }
			    }
			    //Set Defult Header
			    if(!isset ($this->apiResponse['header']['x-app'])){$this->apiResponse['header']['x-app'] = null;}
			    if(!isset ($this->apiResponse['header']['x-session-id'])){$this->apiResponse['header']['x-session-id'] = null;}
			    if(!isset ($this->apiResponse['header']['Set-Cookie'])){$this->apiResponse['header']['Set-Cookie'] = null;}
			    if(!isset ($this->apiResponse['header']['x-user-id'])){$this->apiResponse['header']['x-user-id'] = null;}
			    if(!isset ($this->apiResponse['header']['x-access-token'])){$this->apiResponse['header']['x-access-token'] = null;}
			    if(!isset ($this->apiResponse['header']['x-private-id'])){$this->apiResponse['header']['x-private-id'] = null;}
			    if(!isset ($this->apiResponse['header']['x-sdk-category'])){$this->apiResponse['header']['x-sdk-category'] = null;}
			    if(!isset ($this->apiResponse['header']['x-sdk-name'])){$this->apiResponse['header']['x-sdk-name'] = null;}
			    
			    //json decode body
			    $decodeBodyBase64 = base64_decode($responseCurl['body'], true);
			    if($decodeBodyBase64 === false){throw new ServerAuthenException('422', '90011','Not Decode Base64 format');}
			    if(json_decode($decodeBodyBase64) == null){throw new ServerAuthenException('422', '90011','Response Not JSON format');}
			    
			    $this->apiResponse['body'] = json_decode($decodeBodyBase64);
			    $this->apiBodyResponse = $this->apiResponse['body'];
			    //Set Defult Body
			    if(!isset ($this->apiBodyResponse->{'resultCode'})){$this->apiBodyResponse->{'resultCode'} = null;}
			    if(!isset ($this->apiBodyResponse->{'developerMessage'})){$this->apiBodyResponse->{'developerMessage'} = null;}
			    if(!isset ($this->apiBodyResponse->{'accessToken'})){$this->apiBodyResponse->{'accessToken'} = null;}
			    if(!isset ($this->apiBodyResponse->{'expireIn'})){$this->apiBodyResponse->{'expireIn'} = null;}
			    if(!isset ($this->apiBodyResponse->{'gupAuthenLevel'})){$this->apiBodyResponse->{'gupAuthenLevel'} = null;}
			    if(!isset ($this->apiBodyResponse->{'ptsListOfAPI'})){$this->apiBodyResponse->{'ptsListOfAPI'} = null;}
			    if(!isset ($this->apiBodyResponse->{'gupRegistrationLevel'})){$this->apiBodyResponse->{'gupRegistrationLevel'} = null;}
			    if(!isset ($this->apiBodyResponse->{'openIdFlag'})){$this->apiBodyResponse->{'openIdFlag'} = null;}
			    if(!isset ($this->apiBodyResponse->{'ptsAppId'})){$this->apiBodyResponse->{'ptsAppId'} = null;}
			    if(!isset ($this->apiBodyResponse->{'ptsAppEnvironmentType'})){$this->apiBodyResponse->{'ptsAppEnvironmentType'} = null;}
				$fieldlist = array();
				$fieldValidateArray = array();
				$fieldValidateBoolean = array();
				if($this->apiBodyResponse->{"resultCode"} == "20000"){
				    
				    $apiResponseHeaderVal = (object)[];
				    $apiResponseHeaderVal = json_decode(json_encode($this->apiResponse['header']));
				    
				    
				    //check Validations Header
				    $fieldlist = array('Set-Cookie','x-session-id','x-app','x-user-id','x-access-token','x-private-id');
				    ClientAuthenUtility::validationResponseBody($apiResponseHeaderVal,$fieldlist);
					//check Validations Body
				    $fieldValidateData = array('resultCode','developerMessage','accessToken','expireIn','gupAuthenLevel', 'ptsListOfAPI','gupRegistrationLevel','openIdFlag','ptsAppId','ptsAppEnvironmentType');
					//sent parameter in this function 1.body 2.validate string 3.validate array and 4.validate boolean 
					ClientAuthenUtility::validationResponseBody($this->apiBodyResponse, $fieldValidateData);
				}else{
				    //check Validations Header
					//check Validations Body
					$fieldValidateData = array('developerMessage','resultCode');
					ClientAuthenUtility::validationResponseBody($this->apiBodyResponse,$fieldValidateData);
				}
				return $this->apiBodyResponse;
			}else{
			    $decodeBodyBase64 = base64_decode($responseCurl['body']);
			    if($decodeBodyBase64 === false){throw new ServerAuthenException('422', '90011','Not Decode Base64 format');}
			    if(json_decode($decodeBodyBase64) == null){throw new ServerAuthenException('422', '90011','Response Not JSON format');}
			    $this->apiBodyResponse = json_decode($decodeBodyBase64);
			    $fieldValidateData = array('developerMessage','resultCode');
			    ClientAuthenUtility::validationResponseBody($this->apiBodyResponse,$fieldValidateData);
			    $returnData = ClientAuthenUtility::errorResponsePantryB2C($responseCurl);
			    
				return $returnData;
			}
		
		}	
		
	}
}
?>