<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 17/03/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ClientAuthenAPIManager.php
 *
 */
namespace _server_sdk\service\API{
// 	use _server_sdk\common\ServiceAuthenAPIRequestParameters;
	
	interface ClientAuthenAPIManager{
	    public function obtainAuthorityList($clientAuthenParameters,$requestHeader);
	}
}
?>