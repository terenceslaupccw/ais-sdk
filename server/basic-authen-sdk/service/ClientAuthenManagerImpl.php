<?php
/*
 *
 * The server sdk (Hummus) was basic authentication on server.
 * Our provided 2 parts were server authentication and client authentication
 *
 * @version : 1.2.0
 * @author : Watcharachai T.
 * @date : 24/08/2016
 * @link : https://devportal.ais.co.th/
 * @filename : ClientAuthenManagerImpl.php
 *
 */

namespace _server_sdk\service{
	include_once __DIR__.'/ClientAuthenManager.php';
	include_once __DIR__.'/../utils/ClientAuthenUtility.php';
	include_once __DIR__.'/../builder/ClientAuthenBuilder.php';
	include_once __DIR__.'/../security/ServiceAuthenSecurity.php';
	include_once __DIR__.'/../exception/ServerAuthenException.php';
	include_once __DIR__.'/api/ClientAuthenAPIManagerImpl.php';
	include_once __DIR__.'/param/ClientAuthenParameters.php';
	include_once __DIR__.'/../validation/ClientAuthenValidation.php';
	include_once __DIR__.'/../model/AppAuthenResponse.php';
	
	use _server_sdk\service\param\ClientAuthenParameters;
	use _server_sdk\service\ClientAuthenManager;
	use _server_sdk\utils\ClientAuthenUtility;
	use _server_sdk\builder\ClientAuthenBuilder;
	use _server_sdk\security\ServiceAuthenSecurity;
	use _server_sdk\exception\ServerAuthenException;
	use _server_sdk\service\api\ClientAuthenAPIManagerImpl;
	
	use _server_sdk\validation\ClientAuthenValidation;
	use _server_sdk\model\AppAuthenResponse;

	class ClientAuthenManagerImpl implements ClientAuthenManager{
		private $clientAuthenParameters;
		private static $clientAuthenAPIManagerImpl;
		public $apiResponseBody;
		
		//Singleton
		public static function getServerAuthenAPIInstance(){
			if(!isset(self::$serverAuthenAPIManagerImpl)){
				self::$clientAuthenAPIManagerImpl = new ClientAuthenAPIManagerImpl();
			}
			return self::$clientAuthenAPIManagerImpl;
		}
		//Method appAuthen.
		public function appAuthen($paramResult){
			try{
				//Check config Email.
				ClientAuthenValidation::checkConfigB2C(ClientAuthenBuilder::$serviceConfig->getEmail(),ClientAuthenBuilder::$serviceConfig->getSecret());
				//Get header.
				$requestHeader = ClientAuthenUtility::checkRestHeader(); 
				//Get boby from partner set.
				$requestBody = ClientAuthenUtility::getBodyParameter($paramResult["requestParameter"]);
                //Check Header
				$requestHeaderVal = (object)[];
				$requestHeaderVal = json_decode(json_encode($requestHeader));
				$fieldValidateDataHeader = array('Cookie','x-session-id','x-app','x-user-id');
				ClientAuthenUtility::validationRequestBody($requestHeaderVal,$fieldValidateDataHeader);
				//Check Body
				$fieldValidateData = array('clientId','authCode','platform','redirectURL','ptsAppEnvironmentType');
				ClientAuthenUtility::validationRequestBody($requestBody,$fieldValidateData);
				
				//Check mandatory parameter EnvironmentType.
				ClientAuthenValidation::checkEnvironmentB2C($requestBody->{'ptsAppEnvironmentType'});
				
				//Get value from file liveKey
				$liveKey = ClientAuthenUtility::checkLiveKeyB2C($requestHeader["x-app"],$requestBody->{'platform'},ClientAuthenBuilder::$serviceConfig->getLiveKeyPath());
				//Sent parameter ClientId and Email to method encodeEmail for compare with liveKey's value.
				$encodeEmail = ServiceAuthenSecurity::encodeEmail($requestBody->{'clientId'},ClientAuthenBuilder::$serviceConfig->getEmail());
				//Compare value liveKey and encodeEmail.
				if($encodeEmail == $liveKey){
					$this->clientAuthenParameters = (object)[];
					
					//Get publicKey in secret file.
					$_secret = ServiceAuthenSecurity::readSecret(ClientAuthenBuilder::$serviceConfig->getSecret());
					$publicKey = $_secret['tbsCertificate']['subjectPublicKeyInfo']['subjectPublicKey'];
					
					if($publicKey == ''){
						throw new ServerAuthenException('422', '90002','');
					}else{
						$this->clientAuthenParameters->{'publicKey'} = $publicKey;
					}
					
					//body
					$milliseconds = round(microtime(true) * 1000);
					$this->clientAuthenParameters->{'authCode'} = $requestBody->{'authCode'};
					$this->clientAuthenParameters->{'clientId'} = $requestBody->{'clientId'};
					$this->clientAuthenParameters->{'grantType'} = "authorization_code";
					$this->clientAuthenParameters->{'redirectURL'} = $requestBody->{'redirectURL'};
					$this->clientAuthenParameters->{'timeStamp'} = (string)$milliseconds;
					$this->clientAuthenParameters->{'commandId'} = $requestBody->{'commandId'};
					$this->clientAuthenParameters->{'ptsAppEnvironmentType'} = $requestBody->{'ptsAppEnvironmentType'};
					
					//Generate signature.
					$stringSignature = $this->clientAuthenParameters->{'grantType'}.$this->clientAuthenParameters->{'authCode'}.$this->clientAuthenParameters->{'redirectURL'}.$this->clientAuthenParameters->{'clientId'}.$this->clientAuthenParameters->{'timeStamp'};
					$signature = ServiceAuthenSecurity::genSignature($stringSignature,$this->clientAuthenParameters->{'publicKey'});
					$this->clientAuthenParameters->{'signature'} = $signature;
					
					//Call singerton and send parameter to to method obtainAuthorityList.
					$clientAuthenAPIManagerImpl = self::getServerAuthenAPIInstance();
					$appAuthenResponse = $clientAuthenAPIManagerImpl->obtainAuthorityList($this->clientAuthenParameters,$requestHeader);
					
					if($appAuthenResponse->{'resultCode'} == '20000'){
						$this->apiResponseBody = new AppAuthenResponse($appAuthenResponse);
					}else{
					    $this->apiResponseBody = new AppAuthenResponse($appAuthenResponse);
					
					}

					
// 					print_r(json_encode($this->apiResponseBody));
					return $this->apiResponseBody;
				}else{
					throw new ServerAuthenException('422','90002','[Hummus] LiveKey not match in Livekey file');
				}
			}catch (ServerAuthenException $e){
				$errorResponse = (object)[];
				$errorResponse->resultCode = $e->getErrorCode();
				$errorResponse->developerMessage = $e->getDeveloperMessage();
				$errorResponse->userMessage = $e->getUserMessage();
				$errorResponse->moreInfo = $e->getMoreInfo();
				$this->errorResponse = new AppAuthenResponse($errorResponse);
				
				return $this->errorResponse;
			}
		}
	}
}
?>